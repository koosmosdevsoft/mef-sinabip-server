<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('validaTokenAuth/{token}', 'MenuPrincipalMueblesController@validaTokenAuth');  


Route::get('ModuloMuebles/{id}/{token}', 'MenuPrincipalMueblesController@ListadoMenuPrincipalMuebles');  


/* ACTOS DE ADQUISICION */
Route::post('ListadoActosAdquisicion', 'ActosAdquisicionController@ListadoActosAdquisicion');
Route::post('RegistrarActosAdquisicion', 'ActosAdquisicionController@RegistrarActosAdquisicion');
Route::post('RegistrarActosAdquisicion_Captura_Errores', 'ActosAdquisicionController@RegistrarActosAdquisicion_Captura_Errores');
Route::get('ActualizarActosAdquisicion/{id}', 'ActosAdquisicionController@ActualizarActosAdquisicion');
Route::post('ListadoCatalogoFormaIndividual', 'ActosAdquisicionController@ListadoCatalogoFormaIndividual');
Route::post('ListadoBienesEliminados', 'ActosAdquisicionController@ListadoBienesEliminados');
Route::post('RegistrarBienes', 'ActosAdquisicionController@RegistrarBienes');
Route::post('EditarActoAdquisicion', 'ActosAdquisicionController@EditarActoAdquisicion');
Route::post('AgregarBien_Actos', 'ActosAdquisicionController@AgregarBien_Actos');
Route::post('Eliminar_Actos', 'ActosAdquisicionController@Eliminar_Actos');
Route::post('Datos_Caracteristicas_Bien', 'ActosAdquisicionController@Datos_Caracteristicas_Bien'); 
Route::post('Guardar_Caracteristicas_Bien', 'ActosAdquisicionController@Guardar_Caracteristicas_Bien'); 
// Route::post('SubirZip/{data}' , 'ActosAdquisicionController@SubirZip');
Route::post('Guardar_Actos' , 'ActosAdquisicionController@Guardar_Actos');
Route::post('AdjuntarAltatxt/{data}/{token}','ActosAdquisicionController@AdjuntarAltatxt'); 
Route::post('CargarTXT','ActosAdquisicionController@CargarTXT'); 
Route::post('ValidarTXT','ActosAdquisicionController@ValidarTXT'); 
Route::post('FinalizarTXT','ActosAdquisicionController@FinalizarTXT'); 
Route::post('Observaciones_Validacion','ActosAdquisicionController@Observaciones_Validacion'); 
Route::post('ListadoErroresCargaMasiva','ActosAdquisicionController@ListadoErroresCargaMasiva');
Route::post('Eliminar_Bien_Proceso','ActosAdquisicionController@Eliminar_Bien_Proceso'); 
Route::post('Validar_Caracteritica_Bienes','ActosAdquisicionController@Validar_Caracteritica_Bienes'); 
Route::post('BqserieExiste','ActosAdquisicionController@BqserieExiste'); 


/*RECODIFICACION*/
Route::post('ListadoRecodificacion', 'RecodificacionController@ListadoRecodificacion');
Route::post('ProcesarRecodificacion', 'RecodificacionController@ProcesarRecodificacion');
Route::post('ImprimirResumenRecodificacion', 'RecodificacionController@ImprimirResumenRecodificacion');
Route::get('ImprimirResumenRecodificacion/{id}/{codigo_recodificacion}/{oficio}/{informe}/{fecha}', 'RecodificacionController@ImprimirResumenRecodificacionGet');

/*ELIMINACION*/
Route::post('ListadoEliminacion', 'EliminacionController@ListadoEliminacion');
Route::post('ProcesarEliminacion', 'EliminacionController@ProcesarEliminacion');
Route::post('ImprimirResumenEliminacion', 'EliminacionController@ImprimirResumenEliminacion');
Route::get('ImprimirResumenEliminacion/{id}/{codigo_eliminacion}/{oficio}/{resolucion}/{informe}/{motivos}/{fecha}', 'EliminacionController@ImprimirResumenEliminacionGet');
Route::post('Adjuntareliminartxt/{data}','EliminacionController@Adjuntareliminartxt'); 
Route::post('CargareliminarTXT','EliminacionController@CargareliminarTXT'); 
Route::post('ValidarEliminarTXT','EliminacionController@ValidarEliminarTXT'); 
Route::post('ListadoErroresCargaMasivaEliminar','EliminacionController@ListadoErroresCargaMasivaEliminar');
Route::post('FinalizarEliminacionTXT','EliminacionController@FinalizarEliminacionTXT'); 


/*PERSONAL*/
Route::post('ListadoPersonal', 'PersonalController@ListadoPersonal');
Route::post('ProcesarBajaPersonal', 'PersonalController@ProcesarBajaPersonal');
Route::post('ProcesarEliminarPersonal', 'PersonalController@ProcesarEliminarPersonal');
Route::post('RegistrarPersonal', 'PersonalController@RegistrarPersonal');
Route::post('ObtenerDataPersonal', 'PersonalController@ObtenerDataPersonal');
Route::post('EditarPersonal', 'PersonalController@EditarPersonal');

Route::post('prueba_post', 'PersonalController@prueba_post');

/* ACTOS DE BAJA */
Route::post('ListadoActosBajas','ActosBajasController@ListadoActosBajas');
Route::post('RegistrarActosBajas','ActosBajasController@RegistrarActosBajas');
Route::post('RegistrarActosBajas_Captura_Errores','ActosBajasController@RegistrarActosBajas_Captura_Errores');
Route::get('ActualizarActosBajas/{id}','ActosBajasController@ActualizarActosBajas');
Route::post('ListadoBienesFormaIndividual', 'ActosBajasController@ListadoBienesFormaIndividual');
Route::post('AdjuntarBajatxt/{data}','ActosBajasController@AdjuntarBajatxt');  
Route::post('CargarBajaTXT','ActosBajasController@CargarBajaTXT'); 
Route::post('ValidarBajaTXT','ActosBajasController@ValidarBajaTXT'); 
Route::post('FinalizarBajaTXT','ActosBajasController@FinalizarBajaTXT');  
Route::post('ListadoErroresCargaMasivaBaja','ActosBajasController@ListadoErroresCargaMasivaBaja'); 
Route::post('editarActoBaja', 'ActosBajasController@editarActoBaja'); 
Route::post('Eliminar_ActosBaja', 'ActosBajasController@Eliminar_ActosBaja'); 
Route::post('Agregar_Bienes_al_Detalle', 'ActosBajasController@Agregar_Bienes_al_Detalle');
Route::post('Eliminacion_Detalles_Acto', 'ActosBajasController@Eliminacion_Detalles_Acto');
Route::post('Guardar_ActosBaja' , 'ActosBajasController@Guardar_ActosBaja');
//Route::post('NombArchInffActConc2' , 'ActosBajasController@NombArchInffActConc2');
Route::post('SubirActaInfFinal/{data}' , 'ActosBajasController@SubirActaInfFinal');
Route::post('Consultar_Bienes_Baja_a_Eliminar' , 'ActosBajasController@Consultar_Bienes_Baja_a_Eliminar');
Route::post('SubirFormatoBaja/{data}' , 'ActosBajasController@SubirFormatoBaja');
Route::post('ValidarPDFBajaCatalogo' , 'ActosBajasController@ValidarPDFBajaCatalogo');
Route::get('VerPDFBaja/{archivo}','ActosBajasController@VerPDFBaja');
Route::post('ActualizaPDFBaja' , 'ActosBajasController@ActualizaPDFBaja');





/* ACTOS DE ADMINISTRACION */
Route::post('ListadoActosAdministracion','ActosAdministracionController@ListadoActosAdministracion');
Route::post('editarActoAdministracion', 'ActosAdministracionController@editarActoAdministracion'); 
Route::post('DatosListadosFormaIndividual', 'ActosAdministracionController@DatosListadosFormaIndividual'); 
Route::post('ListadoBienesFormaIndividual_Administracion', 'ActosAdministracionController@ListadoBienesFormaIndividual_Administracion');
Route::post('Agregar_Bienes_al_Detalle_Administracion', 'ActosAdministracionController@Agregar_Bienes_al_Detalle_Administracion');
Route::post('Guardar_ActosAdministracion' , 'ActosAdministracionController@Guardar_ActosAdministracion');
Route::post('Eliminacion_Detalles_Administracion', 'ActosAdministracionController@Eliminacion_Detalles_Administracion'); 
Route::post('Eliminar_ActosAdministracion', 'ActosAdministracionController@Eliminar_ActosAdministracion'); 
Route::post('AdjuntarAdministraciontxt/{data}','ActosAdministracionController@AdjuntarAdministraciontxt'); 
Route::post('CargarAdministracionTXT','ActosAdministracionController@CargarAdministracionTXT'); 
Route::post('ValidarAdministracionTXT','ActosAdministracionController@ValidarAdministracionTXT'); 
Route::post('ListadoErroresCargaMasivaAdministracion','ActosAdministracionController@ListadoErroresCargaMasivaAdministracion');
Route::post('FinalizarAdministracionTXT','ActosAdministracionController@FinalizarAdministracionTXT'); 
Route::post('comprobar_RUC','ActosAdministracionController@comprobar_RUC'); 






/* ACTOS DE DISPOSICION */ 
Route::post('ListadoActosDisposicion','ActosDisposicionController@ListadoActosDisposicion');
Route::post('DatosListadosFormaIndividual_Disposicion', 'ActosDisposicionController@DatosListadosFormaIndividual_Disposicion'); 
Route::post('ListadoBienesFormaIndividual_Disposicion', 'ActosDisposicionController@ListadoBienesFormaIndividual_Disposicion'); 
Route::post('Guardar_ActosDisposicion' , 'ActosDisposicionController@Guardar_ActosDisposicion');
Route::post('editarActoDisposicion', 'ActosDisposicionController@editarActoDisposicion'); 
Route::post('Eliminacion_Detalles_Disposicion', 'ActosDisposicionController@Eliminacion_Detalles_Disposicion'); 
Route::post('Eliminar_ActosDisposicion', 'ActosDisposicionController@Eliminar_ActosDisposicion'); 
Route::post('AdjuntarDisposiciontxt/{data}','ActosDisposicionController@AdjuntarDisposiciontxt'); 
Route::post('CargarDisposicionTXT','ActosDisposicionController@CargarDisposicionTXT'); 
Route::post('ValidarDisposicionTXT','ActosDisposicionController@ValidarDisposicionTXT'); 
Route::post('ListadoErroresCargaMasivaDisposicion','ActosDisposicionController@ListadoErroresCargaMasivaDisposicion');
Route::post('FinalizarDisposicionTXT','ActosDisposicionController@FinalizarDisposicionTXT'); 
Route::post('comprobar_RUC_disposicion','ActosDisposicionController@comprobar_RUC_disposicion'); 








Route::post('RegistrarActosDisposicion_Captura_Errores','ActosDisposicionController@RegistrarActosDisposicion_Captura_Errores');


//Inventario
Route::get('ListadoHistoricoInv/{id}','InventarioMueblesController@ListadoHistoricoInv');
Route::get('Pdf_SustentoInventario/{id}/{anno}','InventarioMueblesController@Pdf_SustentoInventario');
Route::get('ListadoHabilitadoInv/{id}','InventarioMueblesController@ListadoHabilitadoInv');
Route::get('TotalInvActivo/{id}','InventarioMueblesController@TotalInvActivo');
Route::post('ListadoPrediosInvEntidad', 'InventarioMueblesController@ListadoPrediosInvEntidad');
Route::get('HabilitarBotonInv/{id}','InventarioMueblesController@HabilitarBotonInv');
Route::get('ListadoPadronPredios_Id/{predio_id}','InventarioMueblesController@ListadoPadronPredios_Id');
Route::post('RegistroInvSinBienes', 'InventarioMueblesController@RegistroInvSinBienes');
Route::post('ListadoNombreAdjunto', 'InventarioMueblesController@ListadoNombreAdjunto');
Route::post('EliminarAdjunto', 'InventarioMueblesController@EliminarAdjunto');
Route::post('SubirZip/{data}' , 'InventarioMueblesController@SubirZip');
Route::get('Pdf_ObservacionInv/{id}/{predio_id}','InventarioMueblesController@Pdf_ObservacionInv');
Route::post('EnviarAdjunto', 'InventarioMueblesController@EnviarAdjunto');
Route::get('ListadoPreviaInvFinal/{id}/{periodo}','InventarioMueblesController@ListadoPreviaInvFinal');
Route::get('ListadoUsuario_Id/{id}/{cod_inv}','InventarioMueblesController@ListadoUsuario_Id');
Route::get('ListadoPreviaDetalleInvFinal/{id}/{page}/{records}','InventarioMueblesController@ListadoPreviaDetalleInvFinal');
Route::post('RegistrarcabInventario', 'InventarioMueblesController@RegistrarcabInventario');
Route::post('SubirActaInfFinal/{data}' , 'InventarioMueblesController@SubirActaInfFinal');
Route::get('VerPD/{estado}/{archivo}','InventarioMueblesController@VerPD');
Route::post('NombArchInffActConc' , 'InventarioMueblesController@NombArchInffActConc');
Route::get('EliminarArchivo/{estado}/{NomArch}','InventarioMueblesController@EliminarArchivo');
Route::get('NombArchInffActConcUnion/{cod_inventario_ent}' , 'InventarioMueblesController@NombArchInffActConcUnion');
Route::post('FinalizarInvertario' , 'InventarioMueblesController@FinalizarInvertario');
Route::get('NombreEntidad/{id}' , 'InventarioMueblesController@NombreEntidad');
Route::post('EliminarcabInventario', 'InventarioMueblesController@EliminarcabInventario');
Route::post('EstadoFinalInv' , 'InventarioMueblesController@EstadoFinalInv');
Route::post('ValidaResponsablePredio' , 'InventarioMueblesController@ValidaResponsablePredio');
// Route::get('ValidaDepreciacioInv/{id}' , 'InventarioMueblesController@ValidaDepreciacioInv');
Route::get('BuscarEntidad/{id}/{token}' , 'InventarioMueblesController@BuscarEntidad');
Route::post('EliminarArchivoInvPdf' , 'InventarioMueblesController@EliminarArchivoInvPdf');
Route::get('ValidaAsignacionInv/{id}' , 'InventarioMueblesController@ValidaAsignacionInv');


//Asignacion
Route::get('Listadopersonal/{id}/{token}' , 'AsignacionController@Listadopersonal');
Route::get('ListadoTipoBienEntidad/{id}' , 'AsignacionController@ListadoTipoBienEntidad');
Route::post('ListadoAsignarPerson' , 'AsignacionController@ListadoAsignarPerson');
Route::get('ListadoComboAsig' , 'AsignacionController@ListadoComboAsig');
Route::post('ListadoFechaPecosa' , 'AsignacionController@ListadoFechaPecosa');
Route::post('ActualizaPecosa' , 'AsignacionController@ActualizaPecosa');
Route::post('ListadoPersonalAsignar' , 'AsignacionController@ListadoPersonalAsignar');
Route::post('Listadopredarea' , 'AsignacionController@Listadopredarea');
Route::get('PersonalNomb/{id}/{cod_pers}' , 'AsignacionController@PersonalNomb');
Route::post('ResumenPatrimonialAsig' , 'AsignacionController@ResumenPatrimonialAsig');
Route::post('AgregarCabAsignacion' , 'AsignacionController@AgregarCabAsignacion');
Route::post('EliminarCabAsignacion' , 'AsignacionController@EliminarCabAsignacion');
Route::post('AgregarDetAsignacion' , 'AsignacionController@AgregarDetAsignacion');
Route::get('ListaobtTodosCP/{id}' , 'AsignacionController@ListaobtTodosCP');
Route::post('AdjuntarAsignartxt/{data}','AsignacionController@AdjuntarAsignartxt'); 
Route::post('CargarAsigTXT','AsignacionController@CargarAsigTXT'); 
Route::post('ValidarAsigTXT','AsignacionController@ValidarAsigTXT'); 
Route::post('ListadoErroresAsigCargaMasiva','AsignacionController@ListadoErroresAsigCargaMasiva'); 
Route::post('FinalizarAsigTXT','AsignacionController@FinalizarAsigTXT'); 
Route::get('Pdf_ResumenAsignacion/{id}/{nrasig_act}','AsignacionController@Pdf_ResumenAsignacion'); 

//Devolucion
Route::post('ListadoPersonalDevolucion' , 'DevolucionController@ListadoPersonalDevolucion');
Route::post('ListadoDevolucionPerson' , 'DevolucionController@ListadoDevolucionPerson');
Route::get('ListaobtTodosCPDevol/{id}/{idpers}' , 'DevolucionController@ListaobtTodosCPDevol');
Route::get('CantBienesaDevolver/{id}/{idpers}' , 'DevolucionController@CantBienesaDevolver');
Route::post('AgregarCabDevolucion' , 'DevolucionController@AgregarCabDevolucion');
Route::post('EliminarCabDevolucion' , 'DevolucionController@EliminarCabDevolucion');
Route::post('AgregarDetDevolucion' , 'DevolucionController@AgregarDetDevolucion');
Route::get('Pdf_ResumenDevolucion/{id}/{nrdevolu_act}','DevolucionController@Pdf_ResumenDevolucion'); 

//Translado
Route::post('ListadoPersonalTraslado' , 'TransladoController@ListadoPersonalTraslado');
Route::get('CantBienesaTraslado/{id}/{idpers}' , 'TransladoController@CantBienesaTraslado');
Route::post('ListadoTrasladoPerson' , 'TransladoController@ListadoTrasladoPerson');
Route::get('ListadoTipoTraslado' , 'TransladoController@ListadoTipoTraslado');


//Reporte de Actos
Route::post('ListadoActos' , 'ReporteActosController@ListadoActos');
Route::post('ListadoFormaActo' , 'ReporteActosController@ListadoFormaActo');
Route::post('verDetalleActo' , 'ReporteActosController@verDetalleActo');
Route::get('Pdf_Imprimir_Detalle_Acto/{cod_entidad}/{id_acto}/{tipo_acto}','ReporteActosController@Pdf_Imprimir_Detalle_Acto');
Route::get('Pdf_Imprimir_Consolidado_Acto/{cod_entidad}/{tipo_acto}/{forma_adquisicion}/{nro_documento}/{fechaDesde}/{fechaHasta}','ReporteActosController@Pdf_Imprimir_Consolidado_Acto');






Route::post('AgregarCabTraslado' , 'TransladoController@AgregarCabTraslado');
Route::get('ListadoTipoTrasladoxid/{tipo}' , 'TransladoController@ListadoTipoTrasladoxid');
Route::post('EliminarCabTraslado' , 'TransladoController@EliminarCabTraslado');
Route::post('AgregarDetTraslado' , 'TransladoController@AgregarDetTraslado');
Route::get('Pdf_ResumenTraslados/{id}/{nrTrasl_act}','TransladoController@Pdf_ResumenTraslados'); 


//Reporte Situacion Actual
Route::get('ListadoGrupo' , 'ReportesController@ListadoGrupo');
Route::get('ListadoClase/{idclase}' , 'ReportesController@ListadoClase');
Route::get('ListadoEstadoBien' , 'ReportesController@ListadoEstadoBien');
Route::get('ListadoTipoMotivo' , 'ReportesController@ListadoTipoMotivo');
Route::post('ListadoSituacionActual' , 'ReportesController@ListadoSituacionActual');
Route::get('ListadoCaracteristicas/{id}/{codBienPat}' , 'ReportesController@ListadoCaracteristicas');
Route::get('ListadoCuentaContable' , 'ReportesController@ListadoCuentaContable');
Route::get('ListadoUbicacionActual/{id}/{codBienPat}' , 'ReportesController@ListadoUbicacionActual');
Route::get('Pdf_ResumenSituacionActual/{id}/{data}' , 'ReportesController@Pdf_ResumenSituacionActual');
Route::get('downloadExcel/{id}/{data}' , 'ReportesController@downloadExcel');
Route::post('ImportarFormatoTXT','ReportesController@ImportarFormatoTXT'); 
Route::get('ImportarFormatoTXTxGrupo/{id}/{grupo}','ReportesController@ImportarFormatoTXTxGrupo'); 
Route::get('Listar_Bienes_TXT/{id}/{grupo}','ReportesController@Listar_Bienes_TXT'); 
Route::post('ListadoAsignarPersonReporte','ReportesController@ListadoAsignarPersonReporte'); 
Route::post('ListadoAsignarPersonReporteDet','ReportesController@ListadoAsignarPersonReporteDet'); 
Route::get('Pdf_ResumenAsignacionReporte/{id}/{idpersonal}' , 'ReportesController@Pdf_ResumenAsignacionReporte');
Route::get('Pdf_Reportefichatecnica/{id}/{data}/{page}/{records}' , 'ReportesController@Pdf_Reportefichatecnica');
Route::get('FichaTecnicaContar/{id}/{data}' , 'ReportesController@FichaTecnicaContar'); 



//Depreciacion
Route::post('ListadoModificacionValorNeto' , 'DepreciacionController@ListadoModificacionValorNeto');
Route::post('ActualizarDepreciacion' , 'DepreciacionController@ActualizarDepreciacion');
Route::post('ValidaFechaDepreciacion' , 'DepreciacionController@ValidaFechaDepreciacion');
Route::post('ValidaDepreciacionestado' , 'DepreciacionController@ValidaDepreciacionestado');
Route::get('ListaFechaDepreciacionSelec' , 'DepreciacionController@ListaFechaDepreciacionSelec');
Route::post('GuardarTipoDepreciacion' , 'DepreciacionController@GuardarTipoDepreciacion');
Route::post('ListaFechaPecosaActualiza' , 'DepreciacionController@ListaFechaPecosaActualiza');
Route::post('AdjuntarDepreciaciontxt/{data}','DepreciacionController@AdjuntarDepreciaciontxt'); 
Route::post('CargarDepreciacionTXT','DepreciacionController@CargarDepreciacionTXT'); 
Route::post('ValidarDepreciacionTXT','DepreciacionController@ValidarDepreciacionTXT'); 
Route::post('ListadoErroresCargaMasivaDepreciacion','DepreciacionController@ListadoErroresCargaMasivaDepreciacion');
Route::post('FinalizarDepreciacionTXT','DepreciacionController@FinalizarDepreciacionTXT'); 
Route::post('ImportarFormatoExcel','DepreciacionController@ImportarFormatoExcel'); 
Route::get('ImportarFormatoExcelxGrupo/{id}/{grupo}','DepreciacionController@ImportarFormatoExcelxGrupo'); 


//Controllers: Recepcion de Bienes por Acto de Administracion
Route::post('Listado_Recepcion_Bienes' , 'RecepcionBienesActosAdministracion@Listado_Recepcion_Bienes');
Route::post('ListadoTipoAdministracion_Recepcion' , 'RecepcionBienesActosAdministracion@ListadoTipoAdministracion_Recepcion');
Route::post('ListadoTipoBeneficiario', 'RecepcionBienesActosAdministracion@ListadoTipoBeneficiario'); 
Route::post('ListadoCatalogoBienes', 'RecepcionBienesActosAdministracion@ListadoCatalogoBienes');
Route::post('AgregarBien_para_Recepcion', 'RecepcionBienesActosAdministracion@AgregarBien_para_Recepcion');
Route::post('EditarRecepcion', 'RecepcionBienesActosAdministracion@EditarRecepcion');
Route::post('Eliminar_Recepcion', 'RecepcionBienesActosAdministracion@Eliminar_Recepcion');
Route::post('AdjuntarRecepciontxt/{data}','RecepcionBienesActosAdministracion@AdjuntarRecepciontxt'); 
Route::post('CargarRecepcionTXT','RecepcionBienesActosAdministracion@CargarRecepcionTXT'); 
Route::post('ValidarRecepcionTXT','RecepcionBienesActosAdministracion@ValidarRecepcionTXT'); 
Route::post('FinalizarRecepcionTXT','RecepcionBienesActosAdministracion@FinalizarRecepcionTXT'); 
Route::post('ListadoErroresCargaMasivarRecepcion','RecepcionBienesActosAdministracion@ListadoErroresCargaMasivarRecepcion');
Route::post('Validar_Caracteritica_Bienes_Recepcion','RecepcionBienesActosAdministracion@Validar_Caracteritica_Bienes_Recepcion'); 
Route::post('Datos_Caracteristicas_Bien_Recepcion','RecepcionBienesActosAdministracion@Datos_Caracteristicas_Bien_Recepcion'); 
Route::post('Guardar_Actos_Recepcion' , 'RecepcionBienesActosAdministracion@Guardar_Actos_Recepcion');
Route::post('Eliminar_Bien_Recepcion','RecepcionBienesActosAdministracion@Eliminar_Bien_Recepcion'); 





//Controllers: Entrega de Bienes-Recepcion por Acto de Administracion
Route::post('Listado_Entrega_Bienes' , 'EntregaBienesActosAdministracion@Listado_Entrega_Bienes');
Route::post('ListadoTipoAdministracion_Entrega' , 'EntregaBienesActosAdministracion@ListadoTipoAdministracion_Entrega');
Route::post('ListadoTipoBeneficiarioEntrega', 'EntregaBienesActosAdministracion@ListadoTipoBeneficiarioEntrega'); 

Route::post('AgregarBien_para_Entrega', 'EntregaBienesActosAdministracion@AgregarBien_para_Entrega');
Route::post('EditarEntrega', 'EntregaBienesActosAdministracion@EditarEntrega');
Route::post('Eliminar_Entrega', 'EntregaBienesActosAdministracion@Eliminar_Entrega');
Route::post('AdjuntarEntregatxt/{data}','EntregaBienesActosAdministracion@AdjuntarEntregatxt'); 
Route::post('CargarEntregaTXT','EntregaBienesActosAdministracion@CargarEntregaTXT'); 
Route::post('ValidarEntregaTXT','EntregaBienesActosAdministracion@ValidarEntregaTXT'); 
Route::post('FinalizarEntregaTXT','EntregaBienesActosAdministracion@FinalizarEntregaTXT'); 
Route::post('ListadoErroresCargaMasivarEntrega','EntregaBienesActosAdministracion@ListadoErroresCargaMasivarEntrega');
Route::post('ListadoBienesXActoAdministracion','EntregaBienesActosAdministracion@ListadoBienesXActoAdministracion');
Route::post('Agregar_Bienes_al_Detalle_Reingreso', 'EntregaBienesActosAdministracion@Agregar_Bienes_al_Detalle_Reingreso');
Route::post('Guardar_Entrega_Reingreso' , 'EntregaBienesActosAdministracion@Guardar_Entrega_Reingreso');
Route::post('AdjuntarReingresotxt/{data}','EntregaBienesActosAdministracion@AdjuntarReingresotxt'); 
Route::post('ListadoCatalogoBienes_Reingreso', 'EntregaBienesActosAdministracion@ListadoCatalogoBienes_Reingreso');
Route::post('editarReingreso', 'EntregaBienesActosAdministracion@editarReingreso');
Route::post('EliminarItemReingreso', 'EntregaBienesActosAdministracion@EliminarItemReingreso');



/* Controllers: Devolucion de Bienes por Acto de Administracion */
Route::post('ListadoDevolAdministracion' , 'DevolucionBienesActosAdministracion@ListadoDevolAdministracion');
Route::post('editarDevolAdministracion', 'DevolucionBienesActosAdministracion@editarDevolAdministracion'); 
Route::post('ListadoBienesDevolFormaIndividual_Administracion', 'DevolucionBienesActosAdministracion@ListadoBienesDevolFormaIndividual_Administracion');
Route::post('Guardar_DevolAdministracion' , 'DevolucionBienesActosAdministracion@Guardar_DevolAdministracion');
Route::post('Eliminacion_Detalles_DevAdministracion', 'DevolucionBienesActosAdministracion@Eliminacion_Detalles_DevAdministracion'); 
Route::post('Eliminar_ActosDevAdministracion', 'DevolucionBienesActosAdministracion@Eliminar_ActosDevAdministracion'); 
Route::post('Eliminacion_Detalles_DevAdministracion', 'DevolucionBienesActosAdministracion@Eliminacion_Detalles_DevAdministracion'); 
Route::post('Agregar_Bienes_al_Detalle_Dev_Administracion', 'DevolucionBienesActosAdministracion@Agregar_Bienes_al_Detalle_Dev_Administracion');
Route::post('AdjuntarDevAdministraciontxt/{data}','DevolucionBienesActosAdministracion@AdjuntarDevAdministraciontxt'); 
Route::post('CargarDevAdministracionTXT','DevolucionBienesActosAdministracion@CargarDevAdministracionTXT'); 
Route::post('ValidarDevAdministracionTXT','DevolucionBienesActosAdministracion@ValidarDevAdministracionTXT'); 
Route::post('FinalizarDevAdministracionTXT','DevolucionBienesActosAdministracion@FinalizarDevAdministracionTXT'); 
Route::post('ListadoErroresCargaMasivaDevAdministracion','DevolucionBienesActosAdministracion@ListadoErroresCargaMasivaDevAdministracion');

/* Configuracion de carga de documento por acto */
Route::post('ListadoCargaActosDoc' , 'ReporteActosController@ListadoCargaActosDoc');
Route::post('SubirDocActo/{data}' , 'ReporteActosController@SubirDocActo');
Route::get('NombDocActo/{cod_entidad}/{id_acto}/{tipo_acto}' , 'ReporteActosController@NombDocActo');
Route::get('VerPDFDocAct/{archivo}','ReporteActosController@VerPDFDocAct');
Route::get('EliminarArchivoDocActo/{archivo}','ReporteActosController@EliminarArchivoDocActo');

//Route::get('downloadExcel/{id}/{data}' , 'DepreciacionController@downloadExcel');
