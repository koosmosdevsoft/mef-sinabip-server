<?php

namespace sinabipmuebles\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use DB;
use Config;
use Pdf_Resumen_Firma;
use funciones\funciones;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Helpers\JwtAuth;

class AsignacionController extends Controller
{
    public function index()
    {
        return 'FUNCTION INDEX';
    }

    public function Listadopersonal(Request $request,$id,$token){
        // $hash = $request->header('Authorization', null);
        $jwtAuth = new JwtAuth();
		$checktoken = $jwtAuth->checkToken($token); 
        // header("Access-Control-Allow-Origin: *");
        if ($checktoken) {
            $data = DB::select(
                "exec PA_LISTA_PERSONAL_ENTIDAD ?",[$id]
            );
            return response()->success($data);
        }else{
			return response()->success([
				'error' => true,
				'reco' => array(
					"id"=>'No estas autorizado para acceder a esta ruta'
				)
			]);	
		}
    }

    public function ListadoTipoBienEntidad(Request $request,$id){ 
        // header("Access-Control-Allow-Origin: *");
        $data = DB::select(
            "exec PA_LISTA_TIPOBIEN_ENTIDAD ?",[$id]
        );
        return response()->success($data);
    }

    public function ListadoAsignarPerson(Request $request){ 
        // header("Access-Control-Allow-Origin: *");

        $reglas = [
            'id'                => 'int',
            'fecha_adquis'      => 'max:10',
            'nrodocument'       => 'max:10',
            'page'              => 'int',
            'records'           => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
                ]);
        }

        $id                 = $request->id;
        $cod_patrimonial    = $request->cod_patrimonial;
        $denominacion       = $request->denominacion;
        $fecha_adquis       = $request->fecha_adquis;
        $cod_pers           = $request->cod_pers;
        $nrodocument        = $request->nrodocument;
        $nrobien            = $request->nrobien;
        $flagesta           = $request->flagesta;
		$page               = $request->page;
        $records            = $request->records;
        
        $data = DB::select(
            "exec PA_LISTA_ASIGNACION_BIENES ?,?,?,?,?,?,?,?,?,?",
            [$id,$cod_patrimonial,$denominacion,$fecha_adquis,$cod_pers,$nrodocument,$nrobien,$flagesta,$page,$records]
        );
        return response()->success($data);
    }

    public function ListadoComboAsig(Request $request){ 
        // header("Access-Control-Allow-Origin: *");
        $data = DB::select(
            "SELECT DESCRIPCION_ESTADO, ESTADO_PARAMETRO FROM PA_LISTA_COMBO_ASIG"
        );
        return response()->success($data);
    }

    public function ListadoFechaPecosa(Request $request){ 
        // header("Access-Control-Allow-Origin: *");
        $id                 = $request->id;
        $cod_patrimonial    = $request->cod_patrimonial;
        $estado             = $request->estado;
       
        $data = DB::select(
            "exec PA_PROC_PECOSA ?,?,?",[$id,$cod_patrimonial,$estado]
        );
        return response()->success($data);
    }


    public function ActualizaPecosa(Request $request){ 

        $reglas = [
            'id'            => 'int',
            'fecha_pecosa'  => 'max:10' 
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
                ]);
        }

		$id              = $request->id;
		$cod_patrimonial = $request->cod_patrimonial;
		$fecha_pecosa    = $request->fecha_pecosa;
		
		DB::beginTransaction();
        try {
            DB::statement("exec PA_ACTUALIZA_PECOSA ?,?,?",[$id,$cod_patrimonial,date('Y-m-d',strtotime($fecha_pecosa))]);
            DB::commit();
        } catch (\Illuminate\database\QueryException $e) {
            DB::rollBack();
        	return response()->error($e->getMessage());
        }
        return response()->success(true);
    }
    
    public function ListadoPersonalAsignar(Request $request){ 
        // header("Access-Control-Allow-Origin: *");

        $reglas = [
            'id'            => 'int',
            'nrodocument'   => 'max:11',
            'page'          => 'int',
            'records'       => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

        $id                 = $request->id;
        $nrodocument        = $request->nrodocument;
        $appaterpers        = $request->appaterpers;
        $apmaterpers        = $request->apmaterpers;
        $nombre             = $request->nombre;
		$page               = $request->page;
        $records            = $request->records;

        $data = DB::select(
            "exec PA_CONSULTA_PERSONAL ?,?,?,?,?,?,?",
            [$id,$nrodocument,$appaterpers,$apmaterpers,$nombre,$page,$records]);
        return response()->success($data);
    }
    
    
    public function Listadopredarea(Request $request){ 
        // header("Access-Control-Allow-Origin: *");
        $id           = $request->id;
        $id_predio    = $request->id_predio;
        $id_area    = $request->id_area;
        $estado       = $request->estado;

        $data = DB::select(
            "exec PA_LISTA_PRED_AREA ?,?,?,?",[$id,$id_predio,$id_area,$estado]
        );
        return response()->success($data);
    }

    public function PersonalNomb(Request $request,$id,$cod_pers){ 
        // header("Access-Control-Allow-Origin: *");
        $data = DB::select(
            "exec PA_PERSONAL_NOM_COMPLETO ?,?",[$id,$cod_pers]
        );
        return response()->success($data);
    }
    
    public function ResumenPatrimonialAsig(Request $request){ 
        // header("Access-Control-Allow-Origin: *");
        $id                 = $request->id;
        $cod_patrimonial    = $request->cod_patrimonial;
		$page               = $request->page2;
        $records            = $request->records2;

        $data = DB::select(
            "exec PA_RESUMEN_CPATR ?,?,?,?",[$id,$cod_patrimonial,$page,$records]
        );
        return response()->success($data);
    }
       
    public function AgregarCabAsignacion(Request $request){ 
        // header("Access-Control-Allow-Origin: *");

        $reglas = [
            'id'            => 'int',
            'id_predio'     => 'int',
            'id_area'       => 'int',
            'id_personal'   => 'int',
            'id_usuario'    => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
                ]);
        }

        $id           = $request->id;
        $id_predio    = $request->id_predio;
		$id_area      = $request->id_area;
        $id_personal  = $request->id_personal;
        $id_usuario   = $request->id_usuario;

        $data = DB::select(
            "exec PA_INSERT_CAB_ASIG ?,?,?,?,?",[$id,$id_predio,$id_area,$id_personal,$id_usuario]
        );
        return response()->success($data);
    }

    public function EliminarCabAsignacion(Request $request){ 
		$id       = $request->id;
        $num_asig = $request->num_asig;
        
		DB::beginTransaction();
        try {
            DB::statement("exec PA_DELETE_CAB_ASIG ?,?",[$id,$num_asig]);
            DB::commit();
        } catch (\Illuminate\database\QueryException $e) {
            DB::rollBack();
        	return response()->error($e->getMessage());
        }
        return response()->success(true);
    }

    public function AgregarDetAsignacion(Request $request){ 

        $reglas = [
            'id'            => 'int',
            'id_predio'     => 'int',
            'id_area'       => 'int',
            'id_personal'   => 'int',
            'id_usuario'    => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
                ]);
        }

        $id           = $request->id;
        $Listcod_patrimonial = $request->Listcod_patrimonial;
        $num_asig = $request->num_asig;
        $id_predio    = $request->id_predio;
		$id_area      = $request->id_area;
        $id_personal  = $request->id_personal;
        $id_usuario   = $request->id_usuario;
        
		DB::beginTransaction();
        try {
            DB::statement("exec PA_INSERT_DET_ASIG ?,?,?,?,?,?,?",
            [$id,$Listcod_patrimonial,$num_asig,$id_predio,$id_area,$id_personal,$id_usuario]);
            DB::commit();
        } catch (\Illuminate\database\QueryException $e) {
            DB::rollBack();
        	return response()->error($e->getMessage());
        }
        return response()->success(true);
    }

    public function ListaobtTodosCP(Request $request,$id){ 
        // header("Access-Control-Allow-Origin: *");
        $data = DB::select(
            "exec PA_OBTENER_LISTA_CODPATRIMONIAL ?",[$id]
        );
        return response()->success($data);
    }
    
    
    public function AdjuntarAsignartxt(Request $request, $data){
       //header("Access-Control-Allow-Origin: *");
       $data = explode('-',$data);
	   $id_entidad	= $data[0];
	   $usua_creacion = $data[1];
       $tipo_acto = 5;
       $nomb_archivo = '';
       
       //$cod_alta = $cod_alta[0]->CODIGO.'_'; 
       $id_adjuntado =  DB::select( "SELECT TOP 1 ID_ADJUNTADOS_ACTOS_TL FROM TBL_ADJUNTADOS_ACTOS_TL ORDER BY ID_ADJUNTADOS_ACTOS_TL DESC" );
       
       if( empty($id_adjuntado) ){
           $id_adjuntado = 1;
       }else{
           $id_adjuntado = $id_adjuntado[0]->ID_ADJUNTADOS_ACTOS_TL;
           $id_adjuntado++  ;
       }

       if ($request->hasFile('file0')) {  
           $image = $request->file('file0');
           $ARCHIVO_EXTENSION = 'txt'; 
           $NUEVA_CADENA   = substr( md5(microtime()), 1, 4);
           $FECHA_ARCHIVO  = date('Y').''.date('m').''.date('d'); 
           $name = "Asignacion/";
           
           //dd($ARCHIVO_NOMBRE_GENERADO);
           $result = DB::select(
               "exec PA_ADJUNTADO_ACTOS_ADQUISICION_SW ?,?,?,?",[ 
                   $id_entidad,1,"",$usua_creacion
                   ]
           );
           
           $id_adjuntado = $result[0]->ID_ADJUNTADO; 
           $ARCHIVO_NOMBRE_GENERADO ='TXT_'. strval($id_adjuntado).'Asignar_Entidad_'.$id_entidad.'_Serie_'.$FECHA_ARCHIVO.'_'.$NUEVA_CADENA.'.'.$ARCHIVO_EXTENSION;
        //    $url = Storage::disk('public')->putFileAs($name, $image,$ARCHIVO_NOMBRE_GENERADO); 
        //    file_put_contents("//SRV-PORTALSBN/Inventarios_zip$/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
           if(Config::get('app.APP_LINUX')){
            // $url = Storage::disk('public')->put(storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO, $image);
            \Storage::disk('local')->put("public/".$name.$ARCHIVO_NOMBRE_GENERADO, file_get_contents($image));
            // file_put_contents(storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
            $origen = storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO;
            $final = '/mnt/Inventarios_zip/'.$name.$ARCHIVO_NOMBRE_GENERADO;
            shell_exec('cp '.$origen.' '.$final);
            }else{
                file_put_contents("//SRV-PORTALSBN/Inventarios_zip$/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
            }
           $Peso_Arch = filesize($image);

           $result2 = DB::select(
               "exec PA_ADJUNTADO_ACTOS_ADQUISICION_UPD_NOMBRE_ARCHIVO_SW ?,?",[ 
                   $id_adjuntado,$ARCHIVO_NOMBRE_GENERADO
                   ]
           );
           //return response()->json($url);
           return response()->success($result2[0]);
       }
   }
     
   public function CargarAsigTXT(Request $request){
        $id_entidad     = $request->id_entidad;
        $id_adjuntado   = $request->id_adjuntado;
        $nombreArchivo  = $request->nombreArchivo;
        $usua_creacion  = $request->usua_creacion;

        $dataCarga = DB::select( "exec PA_CARGA_MASIVA_ASIGNACION_SW ?, ?, ?, ?",[
            $id_entidad,
            $id_adjuntado,
            $nombreArchivo,
            $usua_creacion
        ]);
        //return response()->success(true);
        return response()->success($dataCarga[0]);
    }
   
    public function ValidarAsigTXT(Request $request){
        $id_entidad     = $request->id_entidad;
        $id_adjuntado   = $request->id_adjuntado;
        $nombreArchivo  = $request->nombreArchivo;
        $usua_creacion  = $request->usua_creacion;

        $dataValidacion = DB::select( "exec PA_VALIDACION_MASIVA_ASIGNACION_SW ?, ?, ?",[
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);
        //return response()->success(true);
        return response()->success($dataValidacion[0]);
    }

    public function ListadoErroresAsigCargaMasiva(Request $request){
        $accion         = 2;
        $id_entidad     = $request->id_entidad;
        $id_adjuntado    = $request->id_adjuntado;
        $usua_creacion  = $request->usua_creacion;
        $dataObservaciones_1 = DB::select( "exec PA_OBSERVACIONES_VALIDACION_MASIVA_ACTOS_ADQUISICION_SW ?, ?, ?, ?",[
            $accion,
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);
        $accion         = 4;
        $dataObservaciones_2 = DB::select( "exec PA_OBSERVACIONES_VALIDACION_MASIVA_ACTOS_ADQUISICION_SW ?, ?, ?, ?",[
            $accion,
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);
        $accion         = 5;
        $dataObservaciones_3 = DB::select( "exec PA_OBSERVACIONES_VALIDACION_MASIVA_ACTOS_ADQUISICION_SW ?, ?, ?, ?",[
            $accion,
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);
        $dataContador = DB::select( "exec PA_CONTADOR_ERROR_ASIG ?, ?",[
            $id_entidad,
            $id_adjuntado
        ]);

        return response()->success([
            "duplicado_bienes" => (count($dataObservaciones_1) > 0) ?$dataObservaciones_1 : [],
            "errores_columnas"  => (count($dataObservaciones_2) > 0) ?$dataObservaciones_2 : [],
            "errores_local_area"  => (count($dataObservaciones_3) > 0) ?$dataObservaciones_3 : [],
            "cant_error"  => (count($dataContador) > 0) ?$dataContador : []
        ]);
    }
        
    public function FinalizarAsigTXT(Request $request){
        $id_entidad     = $request->id_entidad;
        $id_adjuntado   = $request->id_adjuntado;
        $nombreArchivo  = $request->nombreArchivo;
        $usua_creacion  = $request->usua_creacion;

        $dataFinalizacion = DB::select( "exec PA_FINALIZACION_MASIVA_ASIGNACION_SW ?, ?, ?",[
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);
        //return response()->success(true);
        return response()->success($dataFinalizacion[0]);
    }

    public function Pdf_ResumenAsignacion(Request $request){
        header('Content-type: application/pdf');
        $id                = $request->id;
        $nrasig_act        = $request->nrasig_act;

        $NOM_ENTIDAD =  DB::select(
            "SELECT NOM_ENTIDAD FROM TBL_PADRON_ENTIDAD where COD_ENTIDAD = ?",[$id]
        );  
    
        $data_cab = DB::select(
            "exec PA_PDF_RESUMEN_ASIGNACION ?,?",
            [$id,$nrasig_act]
            // [$id,$asig_personal,$asig_predio,$asig_area,$nrasig_act]
        );
    
        $NOMBRE_COMPLETO		= $data_cab[0]->NOMBRE_COMPLETO;
        $NRO_DOCUMENTO			= $data_cab[0]->NRO_DOCUMENTO;
        $DENOMINACION_PREDIO	= $data_cab[0]->DENOMINACION_PREDIO;
        $DESC_AREA		        = $data_cab[0]->DESC_AREA;	
        $NUMERO_ASIG			= $data_cab[0]->NUMERO_ASIG;
        $FECHA_REGISTRO			= $data_cab[0]->FECHA_REGISTRO;
        
         $pdf = new Pdf_Resumen_Firma;
    
         $pdf->AliasNbPages();
    
            $pdf->SetFont('Arial','',10);
            $pdf->AddPage();
            $pdf->Ln(4);
    
            //****************************************
            //****************************************
            
            $pdf->SetFillColor(205,205,205);//color de fondo tabla
            $pdf->SetTextColor(10);
            $pdf->SetDrawColor(153,153,153);
            $pdf->SetLineWidth(.3);
            $pdf->SetFont('Arial','B',9);
            
            $pdf->SetFillColor(205,205,205);
            $pdf->Cell(45,8,'ENTIDAD',1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(140,8,$NOM_ENTIDAD[0]->NOM_ENTIDAD,1,0,'L',true);
                    
            $pdf->Ln();
            $pdf->Ln(3);
            
            //--------------------		
            //RESUMEN DE ASIGNACIÓN DE BIENES MUEBLES
            //--------------------
            $pdf->SetFillColor(205,205,205);	
            $pdf->Cell(185,7,utf8_decode('RESUMEN DE ASIGNACIÓN DE BIENES MUEBLES '),1,0,'C',true);		
            
            $pdf->Ln();
            
            $pdf->SetFillColor(230,230,230);	
            $pdf->Cell(52,6,utf8_decode('Nro. Asignación:'),1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);		
            $pdf->Cell(40,6,$NUMERO_ASIG,1,0,'L',true);	
            
            $pdf->SetFillColor(230,230,230);
            $pdf->Cell(52,6,utf8_decode('Fecha Asignación:'),1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);
            $pdf->Cell(41,6,$FECHA_REGISTRO,1,0,'L',true);
         
            $pdf->Ln();
            $pdf->Ln(3);
            
            //--------------------		
            //DATOS DEL PERSONAL ASIGNADO 
            //--------------------
            $pdf->SetFillColor(205,205,205);	
            $pdf->Cell(185,7,utf8_decode('DATOS DEL PERSONAL ASIGNADO'),1,0,'L',true);		
                    
            $pdf->Ln();

            $pdf->SetFillColor(230,230,230);	
            $pdf->Cell(45,8,utf8_decode('Nro de Documento:'),1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);		
            $pdf->Cell(140,8,$NRO_DOCUMENTO,1,0,'L',true);	
            
            $pdf->Ln();
            
            $pdf->SetFillColor(230,230,230);	
            $pdf->Cell(45,8,utf8_decode('Nombre:'),1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);		
            $pdf->Cell(140,8,utf8_decode($NOMBRE_COMPLETO),1,0,'L',true);
                                        
            $pdf->Ln();
            $pdf->Ln(3);
            
            //--------------------
            //DATOS DE UBICACIÓN DEL BIEN
            //--------------------
            
            $pdf->SetFillColor(205,205,205);
            $pdf->Cell(185,7,utf8_decode('DATOS DE UBICACIÓN DEL BIEN'),1,0,'L',true);		
            
            $pdf->Ln();
            
            $pdf->SetFillColor(230,230,230);	
            $pdf->Cell(45,8,utf8_decode('Local o Predio:'),1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);		
            $pdf->Cell(140,8,utf8_decode($DENOMINACION_PREDIO),1,0,'L',true);	
            
            $pdf->Ln();
            
            $pdf->SetFillColor(230,230,230);	
            $pdf->Cell(45,8,utf8_decode('Area:'),1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);		
            $pdf->Cell(140,8,utf8_decode($DESC_AREA),1,0,'L',true);
                                                          
            /*******************************************************/
            //DETALLE DEL BIEN
            /*******************************************************/
            
            $pdf->Ln();
            $pdf->Ln(5);

            //DETALLE DEL BIEN
            $pdf->SetFont('Arial','B',9);
            $pdf->SetFillColor(205,205,205);	
            $pdf->Cell(185,7,utf8_decode('DETALLE DE LOS BIENES ASIGNADOS'),1,0,'L',true);
            
            $pdf->Ln();
            
            $pdf->SetFont('Arial','B',8);
            $w = array(10, 70, 105);
            $pdf->Cell($w[0],5,utf8_decode('Item'),1,0,'C',true);
            $pdf->Cell($w[1],5,utf8_decode('Codigo Patrimonial'),1,0,'C',true);
            $pdf->Cell($w[2],5,utf8_decode('Denominación'),1,0,'C',true);
            $pdf->Ln();
            
            // Restauración de colores y fuentes
            $pdf->SetFillColor(224,235,255);
            $pdf->SetTextColor(0);
            $pdf->SetFont('Arial','',7);
            // Datos
            $fill = false;
            
            $contador = 0; 

            $data_det = DB::select(
                "exec PA_PDF_DET_RESUMEN_ASIGNACION ?,?",[$id,$nrasig_act]
            );
    
            foreach ($data_det as $key => $value) {

                $DENOMINACION_BIEN = $value->DENOMINACION_BIEN;
                $CODIGO_PATRIMONIAL_DET = $value->CODIGO_PATRIMONIAL;
                
                $contador++;
                $linea = 0;
                
                if($contador == 40){
                    $pdf->Cell(array_sum($w),0,'','T');
                    $pdf->AddPage();
                    $linea = 0;
                }else{
                    $linea++;
                }
                            
                if($linea!= 0 && $linea%58==0 ){
                    $pdf->Cell(array_sum($w),0,'','T');
                    $pdf->AddPage();
                 }
        
                $pdf->Cell($w[0],4,$contador,'LR',0,'C');
                $pdf->Cell($w[1],4,$CODIGO_PATRIMONIAL_DET,'LR',0,'C');
                $pdf->Cell($w[2],4,utf8_decode($DENOMINACION_BIEN),'LR',0,'C');
                $pdf->Ln();
                $fill = !$fill;
            }
    
            // Línea de cierre
            $pdf->Cell(array_sum($w),0,'','T');
            
            $pdf->Ln();
    
            $pdf->Output();
            exit;
           
        }

}
