<?php

namespace sinabipmuebles\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use DB;
use funciones\funciones;
use Pdf_Resumen_Firma;
use Illuminate\Support\Facades\Storage;
use Validator;

class TransladoController extends Controller
{
    
    public function index()
    {
        return 'FUNCTION INDEX';
    }

    public function ListadoPersonalTraslado(Request $request){ 
        // header("Access-Control-Allow-Origin: *");

        $reglas = [
            'id'            => 'int',
            'nrodocument'   => 'max:11',
            'page'          => 'int',
            'records'       => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
                ]);
        }

        $id                 = $request->id;
        $nrodocument        = $request->nrodocument;
        $appaterpers        = $request->appaterpers;
        $apmaterpers        = $request->apmaterpers;
        $nombre             = $request->nombre;
		$page               = $request->page;
        $records            = $request->records;

        $data = DB::select(
            "exec PA_CONSULTA_PERSONAL ?,?,?,?,?,?,?",
            [$id,$nrodocument,$appaterpers,$apmaterpers,$nombre,$page,$records]);
        return response()->success($data);
    }


    public function CantBienesaTraslado(Request $request,$id,$idpers){ 
        // header("Access-Control-Allow-Origin: *");
        $data = DB::select(
            "exec PA_CANT_BIENES_DEVOLVER ?,?",[$id,$idpers]
        );
        return response()->success($data);
    }

    public function ListadoTrasladoPerson(Request $request){ 
        // header("Access-Control-Allow-Origin: *");
        $id                 = $request->id;
        $cod_pers           = $request->cod_pers;
        $cod_patrimonial    = $request->cod_patrimonial;
        $denominacion       = $request->denominacion;
		$page               = $request->page;
        $records            = $request->records;

        $data = DB::select(
            "exec PA_LISTA_DEVOLUCION_BIENES ?,?,?,?,?,?",
            [$id,$cod_pers,$cod_patrimonial,$denominacion,$page,$records]
        );
        return response()->success($data);
    }
    
    public function ListadoTipoTraslado(Request $request){ 
        // header("Access-Control-Allow-Origin: *");
        $data = DB::select(
            "exec PA_LISTA_TIPO_TRASLADO ",[]
        );
        return response()->success($data);
    }

  
    public function AgregarCabTraslado(Request $request){ 
        // header("Access-Control-Allow-Origin: *");

        $reglas = [
            'id'                => 'int',
            'id_predio_dest'    => 'int',
            'id_area_dest'      => 'int',
            'id_personal_dest'  => 'int',
            'id_personal_orig'  => 'int',
            'id_usuario'        => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

        $id                 = $request->id;
        $id_predio_dest     = $request->id_predio_dest;
		$id_area_dest       = $request->id_area_dest;
        $id_personal_dest   = $request->id_personal_dest;
        $id_personal_orig   = $request->id_personal_orig;
        $tipo_traslado      = $request->tipo_traslado;
        $id_usuario         = $request->id_usuario;

        $data = DB::select(
            "exec PA_INSERT_CAB_TRASLADO ?,?,?,?,?,?,?",
            [$id,$id_predio_dest,$id_area_dest,$id_personal_dest,$id_personal_orig,$tipo_traslado,$id_usuario]
        );
        return response()->success($data);
    }
    
    public function ListadoTipoTrasladoxid(Request $request,$tipo){ 
        // header("Access-Control-Allow-Origin: *");
        $data = DB::select(
            "SELECT DESCRIPCION_ESTADO FROM TBL_PARAMETRIZACION_TL 
            WHERE COD_PARAMETRO_CAB='TIPO_TRASLADO' AND ESTADO_PARAMETRO=?
            AND ESTADO=1",[$tipo]
        );
        return response()->success($data);
    }

    public function EliminarCabTraslado(Request $request){ 
		$id       = $request->id;
        $num_asignacion = $request->num_asignacion;
        $num_devolucion = $request->num_devolucion;
        $num_traslado = $request->num_traslado;
        
		DB::beginTransaction();
        try {
            DB::statement("exec PA_DELETE_CAB_TRASLADO ?,?,?,?",
            [$id,$num_asignacion,$num_devolucion,$num_traslado]);
            DB::commit();
        } catch (\Illuminate\database\QueryException $e) {
            DB::rollBack();
        	return response()->error($e->getMessage());
        }
        return response()->success(true);
    }
    
    public function AgregarDetTraslado(Request $request){ 
        $id                 = $request->id;
        $Listcod_patrimonial = $request->Listcod_patrimonial;
        $num_asignacion     = $request->num_asignacion;
        $num_devolucion     = $request->num_devolucion;
        $num_traslado       = $request->num_traslado;
        $id_predio_dest     = $request->id_predio_dest;
        $id_area_dest       = $request->id_area_dest;
        $id_personal_dest   = $request->id_personal_dest;
        $id_personal_orig   = $request->id_personal_orig;
        $id_usuario         = $request->id_usuario;
        
		DB::beginTransaction();
        try {
            DB::statement("exec PA_INSERT_DET_TRASLADO ?,?,?,?,?,?,?,?,?,?",
            [$id,$Listcod_patrimonial,$num_asignacion,$num_devolucion,$num_traslado,$id_predio_dest,$id_area_dest,$id_personal_dest,$id_personal_orig,$id_usuario]);
            DB::commit();
        } catch (\Illuminate\database\QueryException $e) {
            DB::rollBack();
        	return response()->error($e->getMessage());
        }
        return response()->success(true);
    }



    public function Pdf_ResumenTraslados(Request $request){
        header('Content-type: application/pdf');
        $id                = $request->id;
        $nrTrasl_act       = $request->nrTrasl_act;

        $NOM_ENTIDAD =  DB::select(
            "SELECT NOM_ENTIDAD FROM TBL_PADRON_ENTIDAD where COD_ENTIDAD = ?",[$id]
        );  
    
        $data_cab = DB::select(
            "exec PA_PDF_RESUMEN_TRASLADO ?,?",
            [$id,$nrTrasl_act]
            // [$id,$asig_personal,$asig_predio,$asig_area,$nrasig_act]
        );
    
        $NOMBRE_COMPLETO_DEST		= $data_cab[0]->NOMBRE_COMPLETO_DEST;
        $NRO_DOCUMENTO_DEST			= $data_cab[0]->NRO_DOCUMENTO_DEST;
        $NOMBRE_COMPLETO_ORIG		= $data_cab[0]->NOMBRE_COMPLETO_ORIG;
        $NRO_DOCUMENTO_ORIG			= $data_cab[0]->NRO_DOCUMENTO_ORIG;
        $NUMERO_TRASL			    = $data_cab[0]->NUMERO_TRASL;
        $FECHA_REGISTRO			    = $data_cab[0]->FECHA_REGISTRO;
        $DESC_AREA			        = $data_cab[0]->DESC_AREA;
        $DENOMINACION_PREDIO		= $data_cab[0]->DENOMINACION_PREDIO;
        $TIPO_TRASLADO_DESC			= $data_cab[0]->TIPO_TRASLADO_DESC;
        
         $pdf = new Pdf_Resumen_Firma;
    
         $pdf->AliasNbPages();
    
            $pdf->SetFont('Arial','',10);
            $pdf->AddPage();
            $pdf->Ln(4);
    
            //****************************************
            //****************************************
            
            $pdf->SetFillColor(205,205,205);//color de fondo tabla
            $pdf->SetTextColor(10);
            $pdf->SetDrawColor(153,153,153);
            $pdf->SetLineWidth(.3);
            $pdf->SetFont('Arial','B',9);
            
            $pdf->SetFillColor(205,205,205);
            $pdf->Cell(45,8,'ENTIDAD',1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(140,8,$NOM_ENTIDAD[0]->NOM_ENTIDAD,1,0,'L',true);
                    
            $pdf->Ln();
            $pdf->Ln(3);
            
            //--------------------		
            //RESUMEN DE Devolución DE BIENES MUEBLES
            //--------------------
            $pdf->SetFillColor(205,205,205);	
            $pdf->Cell(185,7,utf8_decode('RESUMEN DE TRASLADO DE BIENES MUEBLES '),1,0,'C',true);		
            
            $pdf->Ln();
            
            $pdf->SetFillColor(230,230,230);	
            $pdf->Cell(29,6,utf8_decode('Nro. Traslado:'),1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);		
            $pdf->Cell(61,6,$NUMERO_TRASL,1,0,'L',true);	
            
            $pdf->SetFillColor(230,230,230);
            $pdf->Cell(29,6,utf8_decode('Fecha Traslado:'),1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);
            $pdf->Cell(66,6,$FECHA_REGISTRO,1,0,'L',true);
         
             $pdf->Ln();
            // $pdf->Ln(3);
             
            $pdf->SetFillColor(230,230,230);	
            $pdf->Cell(29,6,utf8_decode('Tipo de Traslado:'),1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);		
            $pdf->Cell(156,6,utf8_decode($TIPO_TRASLADO_DESC),1,0,'L',true);	
            
             $pdf->Ln();
            // $pdf->Ln(3);

            $pdf->SetFillColor(230,230,230);
            $pdf->Cell(29,6,utf8_decode('Local:'),1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);
            $pdf->Cell(61,6,utf8_decode($DENOMINACION_PREDIO),1,0,'L',true);

            $pdf->SetFillColor(230,230,230);
            $pdf->Cell(29,6,utf8_decode('Area:'),1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);
            $pdf->Cell(66,6,utf8_decode($DESC_AREA),1,0,'L',true);
            $pdf->Ln();
            $pdf->Ln(3);
            
            //--------------------		
            //DATOS DEL PERSONAL  
            //--------------------
            $pdf->SetFillColor(205,205,205);	
            $pdf->Cell(90,7,utf8_decode('DATOS DEL SERVIDOR ORIGEN '),1,0,'L',true);	
            $pdf->Cell(95,7,utf8_decode('DATOS DEL SERVIDOR DESTINO '),1,0,'L',true);			
                    
            $pdf->Ln();

            $pdf->SetFillColor(230,230,230);	
            $pdf->Cell(29,8,utf8_decode('Nro Documento:'),1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);		
            $pdf->Cell(61,8,$NRO_DOCUMENTO_ORIG,1,0,'L',true);	
            
            $pdf->SetFillColor(230,230,230);	
            $pdf->Cell(29,8,utf8_decode('Nro Documento:'),1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);		
            $pdf->Cell(66,8,$NRO_DOCUMENTO_DEST,1,0,'L',true);	

            $pdf->Ln();
            
            $pdf->SetFillColor(230,230,230);	
            $pdf->Cell(29,8,utf8_decode('Nombre:'),1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);		
            $pdf->Cell(61,8,utf8_decode($NOMBRE_COMPLETO_ORIG),1,0,'L',true);
             
            $pdf->SetFillColor(230,230,230);	
            $pdf->Cell(29,8,utf8_decode('Nombre:'),1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);		
            $pdf->Cell(66,8,utf8_decode($NOMBRE_COMPLETO_DEST),1,0,'L',true);

            /*******************************************************/
            //DETALLE DEL BIEN
            /*******************************************************/
            
            $pdf->Ln();
            $pdf->Ln(5);

            //DETALLE DEL BIEN
            $pdf->SetFont('Arial','B',9);
            $pdf->SetFillColor(205,205,205);	
            $pdf->Cell(185,7,utf8_decode('DETALLE DE LOS BIENES A Trasladar'),1,0,'L',true);
            
            $pdf->Ln();
            
            $pdf->SetFont('Arial','B',8);
            $w = array(10, 70, 105);
            $pdf->Cell($w[0],5,utf8_decode('Item'),1,0,'C',true);
            $pdf->Cell($w[1],5,utf8_decode('Codigo Patrimonial'),1,0,'C',true);
            $pdf->Cell($w[2],5,utf8_decode('Denominación'),1,0,'C',true);
            $pdf->Ln();
            
            // Restauración de colores y fuentes
            $pdf->SetFillColor(224,235,255);
            $pdf->SetTextColor(0);
            $pdf->SetFont('Arial','',7);
            // Datos
            $fill = false;
            
            $contador = 0; 

            $data_det = DB::select(
                "exec PA_PDF_DET_RESUMEN_TRASLADO ?,?",[$id,$nrTrasl_act]
            );
    
            foreach ($data_det as $key => $value) {
                $DENOMINACION_BIEN = $value->DENOMINACION_BIEN;
                $CODIGO_PATRIMONIAL_DET = $value->CODIGO_PATRIMONIAL;
                
                $contador++;
                $linea = 0;
                
                if($contador == 42){
                    $pdf->Cell(array_sum($w),0,'','T');
                    $pdf->AddPage();
                    $linea = 0;
                }else{
                    $linea++;
                }
 
                if($linea!= 0 && $linea%58==0 ){
                    $pdf->Cell(array_sum($w),0,'','T');
                    $pdf->AddPage();
                 }
        
                $pdf->Cell($w[0],4,$contador,'LR',0,'C');
                $pdf->Cell($w[1],4,$CODIGO_PATRIMONIAL_DET,'LR',0,'C');
                $pdf->Cell($w[2],4,utf8_decode($DENOMINACION_BIEN),'LR',0,'C');
                $pdf->Ln();
                $fill = !$fill;
            }
            // Línea de cierre
            $pdf->Cell(array_sum($w),0,'','T');
            
            $pdf->Ln();
    
            $pdf->Output();
            exit;
           
        }

}
