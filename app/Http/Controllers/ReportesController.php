<?php

namespace sinabipmuebles\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use DB;
use funciones\funciones;
use Pdf_Resumen;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Validator;

class ReportesController extends Controller
{

    public function index()
    {
         return 'FUNCTION INDEX';
    }

    public function ListadoGrupo(Request $request){ 
        // header("Access-Control-Allow-Origin: *");
        $data = DB::select(
            "SELECT DESC_GRUPO,COD_GRUPO,NRO_GRUPO FROM  TBL_MUEBLES_GRUPO WHERE ID_ESTADO = 1  ORDER BY COD_GRUPO"
        );
        return response()->success($data);
    }
    
    public function ListadoClase(Request $request,$idclase){ 
        // header("Access-Control-Allow-Origin: *");
        $data = DB::select(
            "exec PA_CLASE_DESC ?",[$idclase]
        );
        return response()->success($data);
    }
    
    public function ListadoEstadoBien(Request $request){ 
        // header("Access-Control-Allow-Origin: *");
        $data = DB::select(
            "SELECT NOM_EST_BIEN,COD_ESTADO_BIEN,ABRE_EST_BIEN FROM TBL_MUEBLES_ESTADO_BIEN WHERE ID_ESTADO = '1' ORDER BY COD_ESTADO_BIEN"
        );
        return response()->success($data);
    }
    
    public function ListadoTipoMotivo(Request $request){ 
        // header("Access-Control-Allow-Origin: *");
        $data = DB::select(
            "exec PA_TIPO_MOTIVO "
        );
        return response()->success($data);
    }

    

    
    public function ListadoSituacionActual(Request $request){ 
        // header("Access-Control-Allow-Origin: *");

        $reglas = [
            'id'            => 'int',
            'cod_patrimonial' => 'max:12',
            'page'          => 'int',
            'records'       => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
                ]);
        }

        $id                 = $request->id;
        $cod_patrimonial    = $request->cod_patrimonial;    
        $denominacion       = $request->denominacion;
        $cod_grupo          = $request->cod_grupo;
        $cod_clase          = $request->cod_clase;
        $estado_bien        = $request->estado_bien;
        $cod_tipo_mov       = $request->cod_tipo_mov;
		$page               = $request->page;
        $records            = $request->records;
        
        $data = DB::select(
            "exec PA_SITUACION_ACTUAL_BIENES ?,?,?,?,?,?,?,?,?",
            [$id,$cod_patrimonial,$denominacion,$cod_grupo,$cod_clase,$estado_bien,$cod_tipo_mov,$page,$records]
        );
        return response()->success($data);
    }
    
    public function ListadoCaracteristicas(Request $request,$id,$codBienPat){ 
        // header("Access-Control-Allow-Origin: *");

        $validator = Validator::make(
            [
                'id' => $id,
                'codBienPat' => $codBienPat
            ],
            [
                'id' => 'int',
                'codBienPat' => 'int'
            ]
        );
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }


        $data = DB::select(
            "exec PA_CARACTERISTICAS_SB ?,?",[$id,$codBienPat]
        );
        return response()->success($data);
    }
   
    public function ListadoCuentaContable(Request $request){ 
        // header("Access-Control-Allow-Origin: *");
        $data = DB::select(
            "exec PA_LISTADO_CUENTAS_CONTABLES "
        );
        return response()->success($data);
    }

    public function ListadoUbicacionActual(Request $request,$id,$codBienPat){ 
        // header("Access-Control-Allow-Origin: *");

        $validator = Validator::make(
            [
                'id' => $id,
                'codBienPat' => $codBienPat
            ],
            [
                'id' => 'int',
                'codBienPat' => 'int'
            ]
        );
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

        $data = DB::select(
            "exec PA_UBICACION_ACTUAL_BIEN ?,?",[$id,$codBienPat]
        );
        return response()->success($data);
    }
   
    
    public function excel(Request $request)
    {        
        /**
         * toma en cuenta que para ver los mismos 
         * datos debemos hacer la misma consulta
        **/
        Excel::create('Laravel Excel', function($excel) {
            $excel->sheet('Excel sheet', function($sheet) {
                //otra opción -> $products = Product::select('name')->get();

                ini_set('memory_limit', '-1');
                $id       = $request->id;
                $data     = $request->data;
                $data     = base64_decode($data);

                $data_det = DB::select(
                    "exec PA_SITUACION_ACTUAL_BIENES_PDF ?",
                    [$data]
                );


                //$products = Product::all();
                $sheet->fromArray($data_det);
                $sheet->setOrientation('landscape');
            });
        })->export('xls');
    }


    public function downloadExcelaa() 
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }
    
    public function import() 
    {
        return Excel::import(new UsersImport, 'users.xlsx');
    }


    public function Pdf_ResumenSituacionActual(Request $request){
        // header('Content-type: application/pdf');
        ini_set('memory_limit', '-1');
        $id       = $request->id;
        $data     = $request->data;
        $data     = base64_decode($data);

        // dd($data);   
        // return;

        $NOM_ENTIDAD =  DB::select(
            "SELECT NOM_ENTIDAD FROM TBL_PADRON_ENTIDAD where COD_ENTIDAD = ?",[$id]
        );  
    
        //  $pdf = new Pdf_Resumen('L', 'mm', 'A4'); //voltear hoja
         $pdf = new Pdf_Resumen;

         $pdf->AliasNbPages();
    
            $pdf->SetFont('Arial','',10);
            $pdf->AddPage();
            $pdf->Ln(4);

            /*******************************************************/
            //DETALLE DEL BIEN
            /*******************************************************/
            
            //DETALLE DEL BIEN
            $pdf->SetFont('Arial','B',9);
            $pdf->SetFillColor(205,205,205);	
            $pdf->Cell(185,7,utf8_decode('SITUACIÓN ACTUAL DEL BIEN'),1,0,'C',true);
            
            $pdf->Ln();
            
            $pdf->SetFont('Arial','B',8);
            $w = array(7, 23, 68, 15, 15, 20, 37);
            $pdf->Cell($w[0],5,utf8_decode('Item'),1,0,'C',true);
            $pdf->Cell($w[1],5,utf8_decode('Cod. Patrimonial'),1,0,'C',true);
            $pdf->Cell($w[2],5,utf8_decode('Denominación'),1,0,'C',true);
            $pdf->Cell($w[3],5,utf8_decode('Estado'),1,0,'C',true);
            $pdf->Cell($w[4],5,utf8_decode('Nro Doc.'),1,0,'C',true);
            $pdf->Cell($w[5],5,utf8_decode('Valor Adquis.'),1,0,'C',true);
            $pdf->Cell($w[6],5,utf8_decode('Proceso Actual'),1,0,'C',true);
            $pdf->Ln();
            
            // Restauración de colores y fuentes
            $pdf->SetFillColor(224,235,255);
            $pdf->SetTextColor(0);
            $pdf->SetFont('Arial','',7);
            // Datos
            $fill = false;
            
            $contador = 0; 

            $data_det = DB::select(
                "exec PA_SITUACION_ACTUAL_BIENES_PDF ?",
                [$data]
            );

            foreach ($data_det as $key => $value) {

                $CODIGO_PATRIMONIAL_DET = $value->CODIGO_PATRIMONIAL;
                $DENOMINACION_BIEN = $value->DENOMINACION_BIEN;
                $NOM_EST_BIEN = $value->NOM_EST_BIEN;
                $NRO_DOCUMENTO_ADQUIS = $value->NRO_DOCUMENTO_ADQUIS;
                $VALOR_ADQUIS = number_format($value->VALOR_ADQUIS,2);
                //$VALOR_ADQUIS = '0';
                $DES_TIPO_MOV = $value->DES_TIPO_MOV;
                
                $contador++;
                $linea = 0;
                
                if($contador == 60){
                    $pdf->Cell(array_sum($w),0,'','T');
                    $pdf->AddPage();
                    $linea = 0;
                }else{
                    $linea++;
                }
                            
                if($linea!= 0 && $linea%58==0 ){
                    $pdf->Cell(array_sum($w),0,'','T');
                    $pdf->AddPage();
                 }
        
                $pdf->Cell($w[0],4,$contador,'LR',0,'C');
                $pdf->Cell($w[1],4,$CODIGO_PATRIMONIAL_DET,'LR',0,'C');
                $pdf->Cell($w[2],4,$DENOMINACION_BIEN,'LR',0,'L');
                $pdf->Cell($w[3],4,$NOM_EST_BIEN,'LR',0,'C');
                $pdf->Cell($w[4],4,$NRO_DOCUMENTO_ADQUIS,'LR',0,'C');
                $pdf->Cell($w[5],4,$VALOR_ADQUIS,'LR',0,'C');
                $pdf->Cell($w[6],4,$DES_TIPO_MOV,'LR',0,'C');
                $pdf->Ln();
                $fill = !$fill;
            }
    
            // Línea de cierre
            $pdf->Cell(array_sum($w),0,'','T');
            
            $pdf->Ln();
    
            $pdf->Output();
            exit;
           
        }


        public function ImportarFormatoTXT(Request $request){

            $id_entidad = $request->id_entidad;
    
            $dataGrupoDescargas = DB::select("exec SP_GRUPO_DESCARGA_TXT ?", [
                $id_entidad
            ]);
     
            return response()->success([
                "GrupoDescargas" => (count($dataGrupoDescargas) > 0) ?$dataGrupoDescargas : []
            ]);
        }

        public function Listar_Bienes_TXT(Request $request,$id, $grupo){
            //$procedure=New OracleProcedure();
            //$data = ['p_codigo_sem' => [$id,'INTEGER']];
            //$semestres=$procedure->executeCursor(config('database.connections.oracle.userdefault'). '.PV_INGRESANTE.pa_ingresantesSem',$data,'c_ingresantes');
            $string = "";
            $nombresem = 'entidad' . $id . '_info_bienes_archivoNro_' . $grupo;
            
    
            $data_det = DB::select("exec SP_DESCARGA_BIENES_TXT  ?,?", [
                $id,
                $grupo
    
            ]);
    
            // dd($grupo);
            // return;
    
    
            header('Content-type: text/plain');
    
            $string.= 'NRO'.'|'.'CODIGO_PATRIMONIAL'.'|'.'DENOMINACION_BIEN'.'|'.'NRO_CTA_CONTABLE'.'|'.'NOM_CTA_CONTABLE'.'|'.'NRO_DOCUMENTO_ADQUIS'.'|'.'FECHA_DOCUMENTO_ADQUIS'.'|'.'VALOR_ADQUIS'.'|'.'VALOR_NETO'.'|'.'NOM_EST_BIEN'.'|'.'OPC_ASEGURADO'.'|'.'CONDICION'.'|'.'NRO_RESOLUCION_BAJA'.'|'.'FECHA_RESOLUCION_BAJA'.'|'.'NRO_RESOLUCION_DISPOSIC'.'|'.'FECHA_RESOLUCION_DISPOSIC'.'|'.'NRO_RESOLUCION_ADMINIS'.'|'.'FECHA_RESOLUCION_ADMINIS'.'|'.'TIPO_MOV'.'|'.'DENOMINACION_PREDIO'.'|'.'DESC_AREA'.'|'.'NRO_DOCUMENTO'.'|'.'UE_NOMBRES_PERS'.'|'.'UE_APEPAT_PERS'.'|'.'UE_APEMAT_PERS'.'|'.'MARCA'.'|'.'MODELO'.'|'.'TIPO'.'|'.'COLOR'.'|'.'SERIE'.'|'.'DIMENSION'.'|'.'PLACA'.'|'.'NRO_MOTOR'.'|'.'NRO_CHASIS'.'|'.'MATRICULA'.'|'.'ANIO_FABRICACION'.'|'.'RAZA'.'|'.'ESPECIE'.'|'.'EDAD'.'|'.'PAIS'.'|'.'OTRAS_CARACT'.'|'.'VALOR_DEPREC_ACUM_AL_2018'.'|'.'VALOR_NETO_AL_2018'."\r\n";
            //$string.= 'NRO'.'|'.'CODIGO_PATRIMONIAL'."\r\n";
    
            foreach ($data_det as $key => $value) {
                // print_r($value);
                // die();
                
                $string.= $value->ROW_NUMBER_ID.'|'.$value->CODIGO_PATRIMONIAL.'|'.$value->DENOMINACION_BIEN.'|'.$value->NRO_CTA_CONTABLE.'|'.$value->NOM_CTA_CONTABLE.'|'.$value->NRO_DOCUMENTO_ADQUIS.'|'.$value->FECHA_DOCUMENTO_ADQUIS.'|'.$value->VALOR_ADQUIS.'|'.$value->VALOR_NETO.'|'.$value->NOM_EST_BIEN.'|'.$value->OPC_ASEGURADO.'|'.$value->CONDICION.'|'.$value->NRO_RESOLUCION_BAJA.'|'.$value->FECHA_RESOLUCION_BAJA.'|'.$value->NRO_RESOLUCION_DISPOSIC.'|'.$value->FECHA_RESOLUCION_DISPOSIC.'|'.$value->NRO_RESOLUCION_ADMINIS.'|'.$value->FECHA_RESOLUCION_ADMINIS.'|'.$value->TIPO_MOV.'|'.$value->DENOMINACION_PREDIO.'|'.$value->DESC_AREA.'|'.$value->NRO_DOCUMENTO.'|'.$value->UE_NOMBRES_PERS.'|'.$value->UE_APEPAT_PERS.'|'.$value->UE_APEMAT_PERS.'|'.$value->MARCA.'|'.$value->MODELO.'|'.$value->TIPO.'|'.$value->COLOR.'|'.$value->SERIE.'|'.$value->DIMENSION.'|'.$value->PLACA.'|'.$value->NRO_MOTOR.'|'.$value->NRO_CHASIS.'|'.$value->MATRICULA.'|'.$value->ANIO_FABRICACION.'|'.$value->RAZA.'|'.$value->ESPECIE.'|'.$value->EDAD.'|'.$value->PAIS.'|'.$value->OTRAS_CARACT.'|'.$value->VALOR_DEPREC_ACUM_AL_2018.'|'.$value->VALOR_NETO_AL_2018."\r\n";
                //$string.= $value->ROW_NUMBER_ID.'|'.$value->CODIGO_PATRIMONIAL."\r\n";
    
            }
            
            return response($string)
            ->withHeaders([
                'Content-Type' => 'text/plain',
                'Content-Disposition' => 'attachment; filename="'.$nombresem.'.txt',
            ]);
        }

        public function ListadoAsignarPersonReporte(Request $request){ 
            // header("Access-Control-Allow-Origin: *");

            $reglas = [
                'id'            => 'int',
                'fecha_adquis'  => 'max:10',
                'page'          => 'int',
                'records'       => 'int'
            ];
            $validator = Validator::make($request->all(), $reglas);
            if ($validator->fails()){
                return response()->success([
                    'error' => true,
                    'reco' => $validator->errors()
                    ]);
            }

            $id                 = $request->id;
            $cod_patrimonial    = $request->cod_patrimonial;
            $denominacion       = $request->denominacion;
            $fecha_adquis       = $request->fecha_adquis;
            $nrodocument        = $request->nrodocument;
            $nombre             = $request->nombre;
            $apellidoP          = $request->apellidoP;
            $apellidoM          = $request->apellidoM;
            $predio             = $request->predio;
            $area               = $request->area;
            $page               = $request->page;
            $records            = $request->records;
            
            $data = DB::select(
                "exec PA_LISTA_ASIGNACION_BIENES_REPORTE ?,?,?,?,?,?,?,?,?,?,?,?",
                [$id,$cod_patrimonial,$denominacion,$fecha_adquis,$nrodocument,$nombre,$apellidoP,$apellidoM,$predio,$area,$page,$records]
            );
            return response()->success($data);
        }

        public function ListadoAsignarPersonReporteDet(Request $request){ 
            // header("Access-Control-Allow-Origin: *");
            $id                 = $request->id;
            $cod_pers           = $request->cod_pers;
            $page               = $request->page;
            $records            = $request->records;
            
            $data = DB::select(
                "exec PA_LISTA_ASIGNACION_BIENES_REPORTE_DET ?,?,?,?",
                [$id,$cod_pers,$page,$records]
            );
            return response()->success($data);
        }
    


        public function Pdf_ResumenAsignacionReporte(Request $request,$id,$COD_UE_PERS){
            // header('Content-type: application/pdf');
            // ini_set('memory_limit', '-1');
            // $id                = $request->id;
            // $COD_UE_PERS       = $request->COD_UE_PERS;
            // $data     = base64_decode($data);
            // dd($COD_UE_PERS);   
            // return;
    
            $NOM_ENTIDAD =  DB::select(
                "SELECT NOM_ENTIDAD FROM TBL_PADRON_ENTIDAD where COD_ENTIDAD = ?",[$id]
            );  

            $data =  DB::select(
                "SELECT NRO_DOCUMENTO, 	UE_NOMBRES_PERS + ' ' + UE_APEPAT_PERS + ' ' + UE_APEMAT_PERS [PERSONAL] FROM TBL_MUEBLES_UE_PERSONAL WHERE COD_ENTIDAD = ? AND COD_UE_PERS = ?",
                [$id,$COD_UE_PERS]
            );
    
            //  $pdf = new Pdf_Resumen('L', 'mm', 'A4'); //voltear hoja
             $pdf = new Pdf_Resumen('L', 'mm', 'A4');
    
             $pdf->AliasNbPages();
        
                $pdf->SetFont('Arial','',10);
                $pdf->AddPage();
                $pdf->Ln(4);
            
                $pdf->SetFillColor(205,205,205);//color de fondo tabla
                $pdf->SetTextColor(10);
                $pdf->SetDrawColor(153,153,153);
                $pdf->SetLineWidth(.3);
                $pdf->SetFont('Arial','B',9);
                
                $pdf->SetFillColor(205,205,205);
                $pdf->Cell(45,8,'ENTIDAD',1,0,'L',true);		
                $pdf->SetFillColor(255,255,255);	
                $pdf->Cell(233,8,$NOM_ENTIDAD[0]->NOM_ENTIDAD,1,0,'L',true);
                $pdf->Ln();
                $pdf->SetFillColor(230,230,230);	
                $pdf->Cell(45,8,utf8_decode('Nro de Documento:'),1,0,'L',true);		
                $pdf->SetFillColor(255,255,255);		
                $pdf->Cell(233,8,$data[0]->NRO_DOCUMENTO,1,0,'L',true);	
                
                $pdf->Ln();
                
                $pdf->SetFillColor(230,230,230);	
                $pdf->Cell(45,8,utf8_decode('Nombre:'),1,0,'L',true);		
                $pdf->SetFillColor(255,255,255);		
                $pdf->Cell(233,8,utf8_decode($data[0]->PERSONAL),1,0,'L',true);
                                            
                $pdf->Ln();
                $pdf->Ln(3);

			
                /*******************************************************/
                //DETALLE DEL BIEN
                /*******************************************************/
                
                //DETALLE DEL BIEN
                $pdf->SetFont('Arial','B',9);
                $pdf->SetFillColor(205,205,205);	
                $pdf->Cell(278,7,utf8_decode('BIENES ASIGNADOS'),1,0,'C',true);
                
                $pdf->Ln();
                
                $pdf->SetFont('Arial','B',8);
                $w = array(7, 23, 80, 15, 80, 73);
                $pdf->Cell($w[0],5,utf8_decode('Item'),1,0,'C',true);
                $pdf->Cell($w[1],5,utf8_decode('Cod. Patrimonial'),1,0,'C',true);
                $pdf->Cell($w[2],5,utf8_decode('Denominación'),1,0,'C',true);
                $pdf->Cell($w[3],5,utf8_decode('Fecha'),1,0,'C',true);
                $pdf->Cell($w[4],5,utf8_decode('Predio'),1,0,'C',true);
                $pdf->Cell($w[5],5,utf8_decode('Area'),1,0,'C',true);
                $pdf->Ln();
                
                // Restauración de colores y fuentes
                $pdf->SetFillColor(224,235,255);
                $pdf->SetTextColor(0);
                $pdf->SetFont('Arial','',7);
                // Datos
                $fill = false;
                
                $contador = 0; 
    
                $data_det = DB::select(
                    "exec PA_LISTA_ASIGNACION_BIENES_REPORTE_DET ?,?,?,?",
                    [$id,$COD_UE_PERS,1,20]
                );
    
                foreach ($data_det as $key => $value) {
    
                    $CODIGO_PATRIMONIAL_DET = $value->CODIGO_PATRIMONIAL;
                    $DENOMINACION_BIEN = $value->DENOMINACION_BIEN;
                    $FECHA_DOCUMENTO_ADQUIS = $value->FECHA_DOCUMENTO_ADQUIS;
                    $DENOMINACION_PREDIO = $value->DENOMINACION_PREDIO;
                    $DESC_AREA = $value->DESC_AREA;
                    
                    $contador++;
                    $linea = 0;
                    
                    if($contador == 60){
                        $pdf->Cell(array_sum($w),0,'','T');
                        $pdf->AddPage();
                        $linea = 0;
                    }else{
                        $linea++;
                    }
                                
                    if($linea!= 0 && $linea%58==0 ){
                        $pdf->Cell(array_sum($w),0,'','T');
                        $pdf->AddPage();
                     }
            
                    $pdf->Cell($w[0],4,$contador,'LR',0,'C');
                    $pdf->Cell($w[1],4,$CODIGO_PATRIMONIAL_DET,'LR',0,'C');
                    $pdf->Cell($w[2],4,$DENOMINACION_BIEN,'LR',0,'L');
                    $pdf->Cell($w[3],4,$FECHA_DOCUMENTO_ADQUIS,'LR',0,'C');
                    $pdf->Cell($w[4],4,$DENOMINACION_PREDIO,'LR',0,'L');
                    $pdf->Cell($w[5],4,$DESC_AREA,'LR',0,'L');
                    $pdf->Ln();
                    $fill = !$fill;
                }
        
                // Línea de cierre
                $pdf->Cell(array_sum($w),0,'','T');
                
                $pdf->Ln();
        
                $pdf->Output();
                exit;
               
            }

    public function FichaTecnicaContar(Request $request){

        $reglas = [
            'id'   => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
                ]);
        }

        ini_set('memory_limit', '-1');
        $id       = $request->id;
        $data     = $request->data;
        $data     = base64_decode($data);

        $dataValidacion = DB::select( "exec PA_CONTADOR_FICHA_TECNICA ?, ?",[
            $id,
            $data
        ]);

        return response()->success([
            "GrupoDescargas" => (count($dataValidacion) > 0) ?$dataValidacion : []
        ]);
    }
            
    public function Pdf_Reportefichatecnica(Request $request){
        // header('Content-type: application/pdf');

        $reglas = [
            'id'            => 'int',
            'page'          => 'int',
            'records'       => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

        ini_set('memory_limit', '-1');
        $id       = $request->id;
        $data     = $request->data;
        $data     = base64_decode($data);
        $page     = $request->page;
        $records  = $request->records;
        // dd($data);   
        // return;

        $NOM_ENTIDAD =  DB::select(
            "SELECT NOM_ENTIDAD FROM TBL_PADRON_ENTIDAD where COD_ENTIDAD = ?",[$id]
        );  
    
        //  $pdf = new Pdf_Resumen('L', 'mm', 'A4'); //voltear hoja
         $pdf = new Pdf_Resumen;

         $pdf->AliasNbPages();
    
            $pdf->SetFont('Arial','',10);
            $pdf->AddPage();
            $pdf->Ln(5);
    
            //****************************************
            //****************************************
            
            $pdf->SetFillColor(205,205,205);//color de fondo tabla
            $pdf->SetTextColor(10);
            $pdf->SetDrawColor(153,153,153);
            $pdf->SetLineWidth(.3);
            $pdf->SetFont('Arial','B',9);
            
            $pdf->SetFillColor(205,205,205);
            $pdf->Cell(33,8,'ENTIDAD',1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(152,8,$NOM_ENTIDAD[0]->NOM_ENTIDAD,1,0,'L',true);
                    
            $pdf->Ln();
            $pdf->Ln(9);


            /*******************************************************/
            //DETALLE DEL BIEN
            /*******************************************************/
            
            // //DETALLE DEL BIEN
            $w = array(152, 152, 160, 160, 160, 160, 152, 90, 64, 63, 29, 98, 29, 152, 160, 160, 160, 160, 160, 160, 160, 160, 152, 30, 97);
            // Restauración de colores y fuentes
            $pdf->SetFillColor(224,235,255);
            $pdf->SetTextColor(0);
            $pdf->SetFont('Arial','',7);
            // Datos
            $fill = false;
            $contador = 0; 

            $data_det = DB::select(
                "exec PA_REPORTE_FICHA_TECNICA ?, ?, ?, ?",
                [$id, $data, $page, $records]
            );

            foreach ($data_det as $key => $value) {
                $DENOMINACION_BIEN = $value->DENOMINACION_BIEN;
                $CODIGO_PATRIMONIAL_DET = $value->CODIGO_PATRIMONIAL;
                $DENOMINACION_PREDIO = $value->DENOMINACION_PREDIO;
                $DESC_AREA = $value->DESC_AREA;
                $UE_NOMBRES_PERS = $value->UE_NOMBRES_PERS;
                $APELLIDOS = $value->APELLIDOS;
                $NRO_CTA_CONTABLE = $value->NRO_CTA_CONTABLE;
                $NOM_EST_BIEN = $value->NOM_EST_BIEN;
                $VALOR_NETO = $value->VALOR_NETO;
                $VALOR_ADQUIS = $value->VALOR_ADQUIS;
                $NOM_FORM_ADQUIS = $value->NOM_FORM_ADQUIS;
                $FECHA_DOCUMENTO_ADQUIS = $value->FECHA_DOCUMENTO_ADQUIS;
                $OPC_ASEGURADO = $value->OPC_ASEGURADO;
                $NRO_DOCUMENTO_ADQUIS = $value->NRO_DOCUMENTO_ADQUIS;
                $MARCA = $value->MARCA;
                $MODELO = $value->MODELO;
                $TIPO = $value->TIPO;
                $COLOR = $value->COLOR;
                $SERIE = $value->SERIE;
                $NRO_CHASIS = $value->NRO_CHASIS;
                $ANIO_FABRICACION = $value->ANIO_FABRICACION;
                $OTRAS_CARACT = $value->OTRAS_CARACT;
                $NRO_RESOLUCION_BAJA = $value->NRO_RESOLUCION_BAJA;
                $FECHA_RESOLUCION_BAJA = $value->FECHA_RESOLUCION_BAJA;
                $NOM_ACTO_BAJA = $value->NOM_ACTO_BAJA;


                $pdf->SetFillColor(205,205,205);//color de fondo tabla
                $pdf->SetTextColor(10);
                $pdf->SetDrawColor(153,153,153);
                $pdf->SetLineWidth(.3);
                $pdf->SetFont('Arial','B',9);
                
                $pdf->SetFillColor(205,205,205);
                $pdf->Cell(33,6,utf8_decode('Código Patrimonial'),1,0,'L',true);		
                $pdf->SetFillColor(255,255,255);	
                $pdf->Cell($w[1],6,$CODIGO_PATRIMONIAL_DET,1,0,'L',true);
                                    
                $pdf->Ln();         
                $pdf->Ln(2);

                $pdf->SetFillColor(205,205,205);
                $pdf->Cell(33,6,utf8_decode('Denominación'),1,0,'L',true);		
                $pdf->SetFillColor(255,255,255);	
                $pdf->Cell($w[0],6,$DENOMINACION_BIEN,1,0,'L',true);
                                    
                $pdf->Ln();
                $pdf->Ln(9);

                $pdf->Cell(33,6,utf8_decode('DATOS DE UBICACIÓN'),0,0,'L',true);	
                           
                $pdf->Ln();

                $pdf->SetFillColor(205,205,205);
                $pdf->Cell(25,6,utf8_decode('Local'),1,0,'L',true);		
                $pdf->SetFillColor(255,255,255);	
                $pdf->Cell($w[2],6,$DENOMINACION_PREDIO,1,0,'L',true);
     
                $pdf->Ln();      
                $pdf->Ln(2);

                $pdf->SetFillColor(205,205,205);
                $pdf->Cell(25,6,utf8_decode('Área'),1,0,'L',true);		
                $pdf->SetFillColor(255,255,255);	
                $pdf->Cell($w[3],6,$DESC_AREA,1,0,'L',true);
                                    
                $pdf->Ln();
                $pdf->Ln(9);    

                $pdf->Cell(33,6,utf8_decode('DATOS DE USUARIO'),0,0,'L',true);	
                           
                $pdf->Ln();

                $pdf->SetFillColor(205,205,205);
                $pdf->Cell(25,6,utf8_decode('Nombres'),1,0,'L',true);		
                $pdf->SetFillColor(255,255,255);	
                $pdf->Cell($w[5],6,$UE_NOMBRES_PERS,1,0,'L',true);

                $pdf->Ln();        
                $pdf->Ln(2);               

                $pdf->SetFillColor(205,205,205);
                $pdf->Cell(25,6,utf8_decode('Apellidos'),1,0,'L',true);		
                $pdf->SetFillColor(255,255,255);	
                $pdf->Cell($w[4],6,$APELLIDOS,1,0,'L',true);

                $pdf->Ln();
                $pdf->Ln(9);  

                $pdf->Cell(33,6,utf8_decode('DATOS SOBRE EL BIEN'),0,0,'L',true);	
                           
                $pdf->Ln();

                $pdf->SetFillColor(205,205,205);
                $pdf->Cell(33,6,utf8_decode('Cuenta'),1,0,'L',true);		
                $pdf->SetFillColor(255,255,255);	
                $pdf->Cell($w[6],6,$NRO_CTA_CONTABLE,1,0,'L',true);

                $pdf->Ln();      
                $pdf->Ln(2);

                $pdf->SetFillColor(205,205,205);
                $pdf->Cell(33,6,utf8_decode('Estado del Bien'),1,0,'l',true);		
                $pdf->SetFillColor(255,255,255);	
                $pdf->Cell($w[7],6,$NOM_EST_BIEN,1,0,'L',true);

                $pdf->SetFillColor(205,205,205);
                $pdf->Cell(33,6,utf8_decode('Asegurado'),1,0,'C',true);		
                $pdf->SetFillColor(255,255,255);	
                $pdf->Cell($w[10],6,$OPC_ASEGURADO,1,0,'L',true);

                $pdf->Ln();      
                $pdf->Ln(2);

                $pdf->SetFillColor(205,205,205);
                $pdf->Cell(33,6,utf8_decode('Valor Adqui.'),1,0,'L',true);		
                $pdf->SetFillColor(255,255,255);	
                $pdf->Cell($w[8],6,$VALOR_ADQUIS,1,0,'L',true);

                $pdf->SetFillColor(205,205,205);
                $pdf->Cell(25,6,utf8_decode('Valor Neto'),1,0,'C',true);		
                $pdf->SetFillColor(255,255,255);	
                $pdf->Cell($w[9],6,$VALOR_NETO,1,0,'L',true);

                $pdf->Ln();
                $pdf->Ln(3);

                $pdf->SetFillColor(205,205,205);
                $pdf->Cell(33,6,utf8_decode('Docum. Adqui.'),1,0,'L',true);		
                $pdf->SetFillColor(255,255,255);	
                $pdf->Cell($w[13],6,$NRO_DOCUMENTO_ADQUIS,1,0,'L',true);

                $pdf->Ln();      
                $pdf->Ln(2);

                $pdf->SetFillColor(205,205,205);
                $pdf->Cell(33,6,utf8_decode('Fecha Adqui.'),1,0,'L',true);		
                $pdf->SetFillColor(255,255,255);	
                $pdf->Cell($w[12],6,$FECHA_DOCUMENTO_ADQUIS,1,0,'L',true);

                $pdf->SetFillColor(205,205,205);
                $pdf->Cell(25,6,utf8_decode('Forma Adqui.'),1,0,'L',true);		
                $pdf->SetFillColor(255,255,255);	
                $pdf->Cell($w[11],6,$NOM_FORM_ADQUIS,1,0,'L',true);

                $pdf->Ln();
                $pdf->Ln(9);        

                $pdf->Cell(33,6,utf8_decode('DETALLES TÉCNICOS DEL BIEN'),0,0,'L',true);	
                           
                $pdf->Ln();

                $pdf->SetFillColor(205,205,205);
                $pdf->Cell(25,6,utf8_decode('Marca'),1,0,'L',true);		
                $pdf->SetFillColor(255,255,255);	
                $pdf->Cell($w[14],6,$MARCA,1,0,'L',true);

                $pdf->Ln();      
                $pdf->Ln(2);

                $pdf->SetFillColor(205,205,205);
                $pdf->Cell(25,6,utf8_decode('Modelo'),1,0,'L',true);		
                $pdf->SetFillColor(255,255,255);	
                $pdf->Cell($w[15],6,$MODELO,1,0,'L',true);

                $pdf->Ln();      
                $pdf->Ln(2);

                $pdf->SetFillColor(205,205,205);
                $pdf->Cell(25,6,utf8_decode('Tipo'),1,0,'L',true);		
                $pdf->SetFillColor(255,255,255);	
                $pdf->Cell($w[16],6,$TIPO,1,0,'L',true);

                $pdf->Ln();      
                $pdf->Ln(2);

                $pdf->SetFillColor(205,205,205);
                $pdf->Cell(25,6,utf8_decode('Color'),1,0,'L',true);		
                $pdf->SetFillColor(255,255,255);	
                $pdf->Cell($w[17],6,$COLOR,1,0,'L',true);

                $pdf->Ln();      
                $pdf->Ln(2);

                $pdf->SetFillColor(205,205,205);
                $pdf->Cell(25,6,utf8_decode('Nº Serie'),1,0,'L',true);		
                $pdf->SetFillColor(255,255,255);	
                $pdf->Cell($w[18],6,$SERIE,1,0,'L',true);

                $pdf->Ln();      
                $pdf->Ln(2);

                $pdf->SetFillColor(205,205,205);
                $pdf->Cell(25,6,utf8_decode('Nº Chasis'),1,0,'L',true);		
                $pdf->SetFillColor(255,255,255);	
                $pdf->Cell($w[19],6,$NRO_CHASIS,1,0,'L',true);
                
                $pdf->Ln();      
                $pdf->Ln(2);

                $pdf->SetFillColor(205,205,205);
                $pdf->Cell(25,6,utf8_decode('Año de Fabric.'),1,0,'L',true);		
                $pdf->SetFillColor(255,255,255);	
                $pdf->Cell($w[20],6,$ANIO_FABRICACION,1,0,'L',true);

                $pdf->Ln();      
                $pdf->Ln(2);

                $pdf->SetFillColor(205,205,205);
                $pdf->Cell(25,6,utf8_decode('Otras Caract.'),1,0,'L',true);		
                $pdf->SetFillColor(255,255,255);	
                $pdf->Cell($w[21],6,$OTRAS_CARACT,1,0,'L',true);
                                        
                $pdf->Ln();
                $pdf->Ln(9); 
                
                $pdf->Cell(33,6,utf8_decode('DATOS DE LA BAJA'),0,0,'L',true);	
                           
                $pdf->Ln();

                $pdf->SetFillColor(205,205,205);
                $pdf->Cell(33,6,utf8_decode('Nº  De Resoluciòn '),1,0,'L',true);		
                $pdf->SetFillColor(255,255,255);	
                $pdf->Cell($w[22],6,$NRO_RESOLUCION_BAJA,1,0,'L',true);

                $pdf->Ln();      
                $pdf->Ln(2);

                $pdf->SetFillColor(205,205,205);
                $pdf->Cell(33,6,utf8_decode('Fecha Baja'),1,0,'L',true);		
                $pdf->SetFillColor(255,255,255);	
                $pdf->Cell($w[23],6,$FECHA_RESOLUCION_BAJA,1,0,'L',true);

                $pdf->SetFillColor(205,205,205);
                $pdf->Cell(25,6,utf8_decode('Causal'),1,0,'C',true);		
                $pdf->SetFillColor(255,255,255);	
                $pdf->Cell($w[24],6,$NOM_ACTO_BAJA,1,0,'L',true);

                $pdf->Ln();
                 $pdf->Ln(15);   

                $contador++;
                $linea = 0;
                
                if($contador == 60){
                    $pdf->Cell(array_sum($w),0,'','T');
                    $pdf->AddPage();
                    $linea = 0;
                }else{
                    $linea++;
                }
                            
                if($linea!= 0 && $linea%58==0 ){
                    $pdf->Cell(array_sum($w),0,'','T');
                    $pdf->AddPage();
                 }

                $pdf->Ln();
                $fill = !$fill;
            }
    
            // Línea de cierre
            // $pdf->Cell(array_sum($w),0,'','T');
            
            $pdf->Ln();
    
            $pdf->Output();
            exit;
           
        }

    

}
