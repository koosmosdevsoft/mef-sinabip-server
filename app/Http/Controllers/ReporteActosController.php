<?php

namespace sinabipmuebles\Http\Controllers; 

use Illuminate\Http\Request;
use DateTime;
use DB;
use Illuminate\Support\Facades\Storage;
use Pdf_Resumen_Firma;
use Config;
use Validator;

class ReporteActosController extends Controller
{
    

	public function index()
    {
        return 'FUNCTION INDEX';
    }

    public function ListadoActos(Request $request) 
    {

        $reglas = [
            'cod_entidad'    => 'int',
            'tipo_acto'      => 'int',
            'nro_documento'  => 'max:20',
            'fechaDesde'     => 'max:10',
            'fechaHasta'     => 'max:10',
            'page'           => 'int',
            'records'        => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
                ]);
        }

        $cod_entidad        = $request->cod_entidad;
        $tipo_acto          = $request->tipo_acto;
        $forma_adquisicion  = $request->forma_adquisicion;
        $nro_documento      = $request->nro_documento;
        $fechaDesde         = $request->fechaDesde;
        $fechaHasta         = $request->fechaHasta;
        $page               = $request->page;
        $records            = $request->records;
        
        $dataFormaAdq = DB::select('exec PA_LISTADO_TIPO_ACTOS'); 
        
        $data = DB::select('exec PA_LISTADO_ACTOS ?,?,?,?,?,?,?,?', [ 
            $cod_entidad,
            $tipo_acto,
            $forma_adquisicion,
            $nro_documento,
            $fechaDesde,
            $fechaHasta,
            $page,
            $records
        ]);

	    return response()->success([
            "documento" => (count($data) > 0) ?$data : [],
            "formaAdq"  => (count($dataFormaAdq) > 0) ?$dataFormaAdq : []
        ]);
    }
    
    public function ListadoCargaActosDoc(Request $request) 
    {
        $cod_entidad        = $request->cod_entidad;
        $tipo_acto          = $request->tipo_acto;
        $forma_adquisicion  = $request->forma_adquisicion;
        $nro_documento      = $request->nro_documento;
        $fechaDesde         = $request->fechaDesde;
        $fechaHasta         = $request->fechaHasta;
        $page               = $request->page;
        $records            = $request->records;
        
        $dataFormaAdq = DB::select('exec PA_LISTADO_TIPO_ACTOS'); 
                
        $data = DB::select('exec PA_LISTA_CARGA_DOCUM ?,?,?,?,?,?,?,?', [ 
            $cod_entidad,
            $tipo_acto,
            $forma_adquisicion,
            $nro_documento,
            $fechaDesde,
            $fechaHasta,
            $page,
            $records
        ]);

        $datadocumacto = DB::select('exec PA_CANT_DOCUMENTO_CARGADO_ACTO ?', [ 
            $cod_entidad
        ]);

	    return response()->success([
            "documento" => (count($data) > 0) ?$data : [],
            "formaAdq"  => (count($dataFormaAdq) > 0) ?$dataFormaAdq : [],
            "datadocumacto"  => (count($datadocumacto) > 0) ?$datadocumacto : []
        ]);
    }

    public function ListadoFormaActo(Request $request) 
    {
        $tipo_acto = $request->tipo_acto;
        
        $dataFormaAdq = DB::select('exec PA_LISTADO_FORMAS_ACTOS ?', [ 
            $tipo_acto
        ]);

	    return response()->success([
            "formaActos"  => (count($dataFormaAdq) > 0) ?$dataFormaAdq : []
        ]);
    }


    public function verDetalleActo(Request $request){

        $reglas = [
            'id_entidad'  => 'int',
            'id_acto'     => 'int',
            'tipo_acto'   => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

        $id_entidad = $request->id_entidad;
        $id_acto    = $request->id_acto;
        $tipo_acto  = $request->tipo_acto;

        $dataDetActo = DB::select( "exec PA_VER_DETALLE_ACTO ?, ?, ?",[$id_entidad, $id_acto, $tipo_acto]);
        return response()->success([
            "detalles"  => (count($dataDetActo) > 0) ?$dataDetActo : []
        ]);
    }

    public function Pdf_Imprimir_Detalle_Acto(Request $request, $cod_entidad, $id_acto, $tipo_acto){

        $NOM_ENTIDAD =  DB::select(
            "SELECT NOM_ENTIDAD FROM TBL_PADRON_ENTIDAD where COD_ENTIDAD = ?",[$cod_entidad]
        );  
    
        $data_cab = DB::select(
            "exec PA_PDF_CABECERA_ACTOS ?,?,?",
            [$cod_entidad, $tipo_acto, $id_acto]
        );
    
        $NUMERO_DOC			= $data_cab[0]->NRO_DOC;
        $FECHA_DOC			= $data_cab[0]->FECHA_DOC;

        $TITULO             = '';
        $TITULO2            = '';

        if( $tipo_acto == '1' ){
            $TITULO             = 'ACTO DE ADQUISICION';
            $TITULO2            = 'Adquisicion';
        }else if( $tipo_acto == '2' ){
            $TITULO             = 'ACTO DE BAJA';
            $TITULO2            = 'Baja';
        }else if( $tipo_acto == '3' ){
            $TITULO             = 'ACTO DE ADMINISTRACION';
            $TITULO2            = 'Administracion';
        }else{
            $TITULO             = 'ACTO DE DISPOSICION';
            $TITULO2            = 'Disposicion';
        }
        
         $pdf = new Pdf_Resumen_Firma;
    
         $pdf->AliasNbPages();
    
            $pdf->SetFont('Arial','',10);
            $pdf->AddPage();
            $pdf->Ln(4);
    
            //****************************************
            //****************************************
            
            $pdf->SetFillColor(205,205,205);//color de fondo tabla
            $pdf->SetTextColor(10);
            $pdf->SetDrawColor(153,153,153);
            $pdf->SetLineWidth(.3);
            $pdf->SetFont('Arial','B',9);
            
            $pdf->SetFillColor(205,205,205);
            $pdf->Cell(45,8,'ENTIDAD',1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(140,8,$NOM_ENTIDAD[0]->NOM_ENTIDAD,1,0,'L',true);
                    
            $pdf->Ln();
            $pdf->Ln(3);
            
            $pdf->SetFillColor(205,205,205);	
            $pdf->Cell(185,7,utf8_decode('DOCUMENTO DE ' . $TITULO),1,0,'C',true);		
            
            $pdf->Ln();
            
            $pdf->SetFillColor(230,230,230);	
            $pdf->Cell(52,6,utf8_decode('Nro. ' . $TITULO2 . ':'),1,0,'L',true);
            $pdf->SetFillColor(255,255,255);		
            $pdf->Cell(40,6,$NUMERO_DOC,1,0,'L',true);	
            
            $pdf->SetFillColor(230,230,230);
            $pdf->Cell(52,6,utf8_decode('Fecha ' . $TITULO2 . ':'),1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);
            $pdf->Cell(41,6,$FECHA_DOC,1,0,'L',true);
         
            $pdf->Ln();
            $pdf->Ln(2);
            
            //DETALLE DEL BIEN
            $pdf->SetFont('Arial','B',9);
            $pdf->SetFillColor(205,205,205);	
            $pdf->Cell(185,7,utf8_decode('DETALLE DEL ACTO DE ADQUISICION'),1,0,'L',true);
            
            $pdf->Ln();
            
            $pdf->SetFont('Arial','B',8);
            $w = array(10, 50, 125);
            $pdf->Cell($w[0],5,utf8_decode('Item'),1,0,'C',true);
            $pdf->Cell($w[1],5,utf8_decode('CODIGO PATRIMONIAL'),1,0,'C',true);
            $pdf->Cell($w[2],5,utf8_decode('DENOMINACION DEL BIEN'),1,0,'C',true);
            $pdf->Ln();
            
            // Restauración de colores y fuentes
            $pdf->SetFillColor(224,235,255);
            $pdf->SetTextColor(0);
            $pdf->SetFont('Arial','',7);
            // Datos
            $fill = false;
            
            $contador = 0; 

            $data_det = DB::select(
                "exec PA_PDF_DETALLE_ACTOS ?,?,?",[$cod_entidad, $tipo_acto, $id_acto]
            );
    
            foreach ($data_det as $key => $value) {

                $CODIGO_PATRIMONIAL_DET = $value->CODIGO_PATRIMONIAL;
                $DENOMINACION_BIEN = $value->DENOMINACION_BIEN;
                
                $contador++;
                $linea = 0;
                
                if($contador == 40){
                    $pdf->Cell(array_sum($w),0,'','T');
                    $pdf->AddPage();
                    $linea = 0;
                }else{
                    $linea++;
                }
                            
                if($linea!= 0 && $linea%58==0 ){
                    $pdf->Cell(array_sum($w),0,'','T');
                    $pdf->AddPage();
                 }
        
                $pdf->Cell($w[0],4,$contador,'LR',0,'C');
                $pdf->Cell($w[1],4,$CODIGO_PATRIMONIAL_DET,'LR',0,'L');
                $pdf->Cell($w[2],4,utf8_decode($DENOMINACION_BIEN),'LR',0,'L');
                $pdf->Ln();
                $fill = !$fill;
            }
    
            // Línea de cierre
            $pdf->Cell(array_sum($w),0,'','T');
            
            $pdf->Ln();
    
            $pdf->Output();
            exit;
           
    }


    public function Pdf_Imprimir_Consolidado_Acto(Request $request, $cod_entidad, $tipo_acto, $forma_adquisicion, $nro_documento, $fechaDesde, $fechaHasta){

        $NOM_ENTIDAD =  DB::select(
            "SELECT NOM_ENTIDAD FROM TBL_PADRON_ENTIDAD where COD_ENTIDAD = ?",[$cod_entidad]
        );  

        $TITULO             = '';
        $TITULO2            = '';

        if( $tipo_acto == '1' ){
            $TITULO             = 'ACTOS DE ADQUISICION';
            $TITULO2            = 'Adquisicion';
        }else if( $tipo_acto == '2' ){
            $TITULO             = 'ACTOS DE BAJA';
            $TITULO2            = 'Baja';
        }else if( $tipo_acto == '3' ){
            $TITULO             = 'ACTOS DE ADMINISTRACION';
            $TITULO2            = 'Administracion';
        }else{
            $TITULO             = 'ACTOS DE DISPOSICION';
            $TITULO2            = 'Disposicion';
        }
        
         $pdf = new Pdf_Resumen_Firma;
    
         $pdf->AliasNbPages();
    
            $pdf->SetFont('Arial','',10);
            $pdf->AddPage();
            $pdf->Ln(4);
            //****************************************
            //****************************************
            $pdf->SetFillColor(205,205,205);//color de fondo tabla
            $pdf->SetTextColor(10);
            $pdf->SetDrawColor(153,153,153);
            $pdf->SetLineWidth(.3);
            $pdf->SetFont('Arial','B',9);
            
            $pdf->SetFillColor(205,205,205);
            $pdf->Cell(45,8,'ENTIDAD',1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(140,8,$NOM_ENTIDAD[0]->NOM_ENTIDAD,1,0,'L',true);
                    
            $pdf->Ln();
            $pdf->Ln(3);
            
            //DETALLE DEL BIEN
            $pdf->SetFont('Arial','B',9);
            $pdf->SetFillColor(205,205,205);	
            $pdf->Cell(185,7,utf8_decode('LISTADO DE ' . $TITULO),1,0,'L',true);
            
            $pdf->Ln();
            
            $pdf->SetFont('Arial','B',8);
            $w = array(10, 40, 40, 25, 40, 30);
            $pdf->Cell($w[0],5,utf8_decode('Item'),1,0,'C',true);
            $pdf->Cell($w[1],5,utf8_decode('NRO DOCUMENTO ACTO'),1,0,'C',true);
            $pdf->Cell($w[2],5,utf8_decode('FECHA DOCUMENTO'),1,0,'C',true);
            $pdf->Cell($w[3],5,utf8_decode('ESTADO'),1,0,'C',true);
            $pdf->Cell($w[4],5,utf8_decode('FORMA DE ACTO'),1,0,'C',true);
            $pdf->Cell($w[5],5,utf8_decode('TOTAL BIENES'),1,0,'C',true);
            $pdf->Ln();
            
            // Restauración de colores y fuentes
            $pdf->SetFillColor(224,235,255);
            $pdf->SetTextColor(0);
            $pdf->SetFont('Arial','',7);

            // Datos
            $fill = false;
            $contador = 0; 

            $data_det = DB::select(
                "exec PA_PDF_CONSOLIDADO_ACTOS ?,?,?,?,?,?", [$cod_entidad, $tipo_acto, $forma_adquisicion, $nro_documento, $fechaDesde, $fechaHasta]
            );
    
            foreach ($data_det as $key => $value) {

                $ROW_NUMBER_ID          = $value->ROW_NUMBER_ID;
                $NRO_DOCUMENTO_ADQUIS   = $value->NRO_DOCUMENTO_ADQUIS;
                $FECHA_DOCUMENTO_ADQUIS = $value->FECHA_DOCUMENTO_ADQUIS;
                $ESTADO                 = $value->ESTADO;
                $NOM_FORM_ADQUIS        = $value->NOM_FORM_ADQUIS;
                $TOTAL_BIENES           = $value->TOTAL_BIENES;
                
                $contador++;
                $linea = 0;
                
                if($contador == 40){
                    $pdf->Cell(array_sum($w),0,'','T');
                    $pdf->AddPage();
                    $linea = 0;
                }else{
                    $linea++;
                }
                            
                if($linea!= 0 && $linea%58==0 ){
                    $pdf->Cell(array_sum($w),0,'','T');
                    $pdf->AddPage();
                 }
        
                $pdf->Cell($w[0],4,$contador,'LR',0,'C');
                $pdf->Cell($w[1],4,$NRO_DOCUMENTO_ADQUIS,'LR',0,'L');
                $pdf->Cell($w[2],4,cambiaf_a_normal_2($FECHA_DOCUMENTO_ADQUIS),'LR',0,'L');
                $pdf->Cell($w[3],4,utf8_decode($ESTADO),'LR',0,'L');
                $pdf->Cell($w[4],4,utf8_decode($NOM_FORM_ADQUIS),'LR',0,'L');
                $pdf->Cell($w[5],4,utf8_decode($TOTAL_BIENES),'LR',0,'L');
                $pdf->Ln();
                $fill = !$fill;
            }
    
            // Línea de cierre
            $pdf->Cell(array_sum($w),0,'','T'); 
            $pdf->Ln();
            $pdf->Output();
            exit;
        }

    public function fecha_sql($fecha){
        date_default_timezone_set('America/Lima');
        $datetime_variable = new DateTime($fecha);
        $datetime_formatted = $datetime_variable->format('Y-m-d H:i:s');
       return  $datetime_formatted;  
    }

	public function SubirDocActo(Request $request,$data){
		$data = explode('-',$data);
		$cod_entidad	= $data[0];
		$id_acto = $data[1];
		$tipo_acto = $data[2];

        if ($request->hasFile('file0')) {
            $image = $request->file('file0');
			$ARCHIVO_EXTENSION = 'pdf';
			$NUEVA_CADENA				= substr( md5(microtime()), 1, 4);
			$FECHA_ARCHIVO				= date('Y').''.date('m').''.date('d');
            $nom_acto             = '';
    
            if( $tipo_acto == '1' ){
                $nom_acto = 'Alta';
            }else if( $tipo_acto == '2' ){
                $nom_acto = 'Baja';
            }else if( $tipo_acto == '3' ){
                $nom_acto = 'Administracion';
            }else{
                $nom_acto = 'Disposicion';
            }

            $name = "doc_actos/";
            $ARCHIVO_NOMBRE_GENERADO ='Doc_Acto_'.$nom_acto.'_'.$cod_entidad.'_IA'.$id_acto.'_'.$FECHA_ARCHIVO.'_'.$NUEVA_CADENA.'.'.$ARCHIVO_EXTENSION;
                    
            if(Config::get('app.APP_LINUX')){
				file_put_contents(storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
				$origen = storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO;
				$url = '/mnt/Inventarios_zip/'.$name.$ARCHIVO_NOMBRE_GENERADO;
				// shell_exec('cp '.$origen.' '.$url);
                exec("cp '$origen' '$url'", $output, $retrun_var);
			}else{
				$url = Storage::disk('public')->putFileAs($name, $image,$ARCHIVO_NOMBRE_GENERADO);
            }

			//shell_exec('mv '.storage_path('app/public').$name.$ARCHIVO_NOMBRE_GENERADO.' /mnt/Invenarios_zip/'.$name.$ARCHIVO_NOMBRE_GENERADO);
			$Peso_Arch = filesize($image);
			$result = DB::statement(
                "exec PA_ACT_DOC_CARGA ?,?,?,?,?,?",[
					$cod_entidad,$id_acto,$tipo_acto,$ARCHIVO_NOMBRE_GENERADO,$Peso_Arch,1
					]
            );
            return response()->json($url);
        }
    }
    
    public function NombDocActo(Request $request,$cod_entidad,$id_acto,$tipo_acto){ 
        
        $data = DB::select(
            "exec PA_ACT_DOC_CARGA ?,?,?,?,?,?",
            [$cod_entidad,$id_acto,$tipo_acto,'','',2]
        );
        return response()->success($data[0]);
	}


	public function VerPDFDocAct(Request $request,$archivo){
		
        if(Config::get('app.APP_LINUX'))
            $name = '/mnt/Inventarios_zip/'."doc_actos/";
        else
            $name = ruta_server()."doc_actos/";
    
		$contents = $name.$archivo;
		header("Content-type: application/pdf");
		header('Content-Disposition: inline; filename="'.$contents.'"');
		$data = file_get_contents($contents);
		echo $data;
		exit;
    }
    
    
	public function EliminarArchivoDocActo(Request $request,$NomArch){ 
		$name = "doc_actos/";
		$exists = Storage::disk('public')->exists($name.$NomArch);
		Storage::disk('public')->delete($name.$NomArch);
		return response()->json($exists);
    }
	

}
