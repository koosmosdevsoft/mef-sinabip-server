<?php

namespace sinabipmuebles\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use DB;
use Config;
use Illuminate\Support\Facades\Storage;

class RecepcionController extends Controller
{
    
    public function AgregarBien_para_Recepcion(Request $request){

        $TipoagregarBien    = $request->TipoagregarBien;
        $id_acto            = $request->id_acto;
        $id_entidad         = $request->id_entidad;
        $id_forma           = $request->id_forma;
        $nro_documento      = $request->nro_documento;
        $fecha_documento    = $request->fecha_documento;
        $usua_creacion      = $request->usua_creacion; 
        $codigo_patrimonial = $request->codigo_patrimonial;
        $denominacion_bien  = $request->denominacion_bien;
        $cantidad           = $request->cantidad;
        $valor_adq          = $request->valor_adq;

        $dataCabActo = DB::select( "exec PA_REGISTRAR_BIENES_POR_RECEPCION ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?",[
            $TipoagregarBien,
            $id_acto, 
            $id_entidad,
            $id_forma,
            $nro_documento,
            $fecha_documento,
            $usua_creacion,
            $codigo_patrimonial,
            $denominacion_bien,
            $cantidad,
            $valor_adq
        ]);
        //dd($id_entidad);
        return response()->success([
            "cabecera" => (count($dataCabActo) > 0) ?$dataCabActo[0] : []
        ]);
        return response()->success(true);
    }

}
