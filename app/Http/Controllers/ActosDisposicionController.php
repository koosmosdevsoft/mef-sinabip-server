<?php

namespace sinabipmuebles\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use DB;
use Config;
use nusoap_client;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Helpers\JwtAuth;

class ActosDisposicionController extends Controller  
{
    
	public function index()
    {
        return 'FUNCTION INDEX';
    }

    
    public function ListadoActosDisposicion(Request $request)  
    {
        $hash = $request->header('Authorization', null);
        $jwtAuth = new JwtAuth();
		$checktoken = $jwtAuth->checkToken($hash);		
        if ($checktoken){

            $reglas = [
                'cod_entidad'   => 'int',
                'nro_documento' => 'max:20',
                'page'          => 'int',
                'records'       => 'int'
            ];
            $validator = Validator::make($request->all(), $reglas);
            if ($validator->fails()){
                return response()->success([
                    'error' => true,
                    'reco' => $validator->errors()
                    ]);
            }

            $cod_entidad        = $request->cod_entidad;
            $forma_disposicion  = $request->forma_disposicion;
            $nro_documento      = $request->nro_documento;
            $estado             = $request->estado;
            $fecha_year         = $request->fecha['year'];
            $fecha_month        = $request->fecha['month'];
            $page               = $request->page;
            $records            = $request->records;
            
            $dataFormaAdq = DB::select('exec PA_LISTADO_FORMAS_DISPOSICION'); 
            $dataAnios = DB::select('exec PA_LISTADO_ANIOS');
            $dataMes = DB::select('exec PA_LISTADO_MESES');

            $data = DB::select('exec PA_LISTADO_ACTOS_DISPOSICION ?,?,?,?,?,?,?,?', [ 
                $cod_entidad,
                $forma_disposicion,
                $nro_documento,
                $fecha_year,
                $fecha_month,
                $estado,
                $page,
                $records

            ]);

            return response()->success([
                "documento" => (count($data) > 0) ?$data : [],
                "formaAdq"  => (count($dataFormaAdq) > 0) ?$dataFormaAdq : [],
                "anios"  => (count($dataAnios) > 0) ?$dataAnios : [],
                "mes"  => (count($dataMes) > 0) ?$dataMes : []
            ]);
        }else{
			return response()->success([
				'error' => true,
				'reco' => array(
					"id"=>'No estas autorizado para acceder a esta ruta'
				)
			]);	
		}
    }

    public function DatosListadosFormaIndividual_Disposicion(){
        $hash = $request->header('Authorization', null);
        $jwtAuth = new JwtAuth();
		$checktoken = $jwtAuth->checkToken($hash);		
        if ($checktoken){
            $dataCabActo = DB::select( "exec PA_LISTADOS_FORMAINDIVIDUAL_DISPOSICION");
            return response()->success([
                "listadoTipoBeneficiario" => (count($dataCabActo) > 0) ?$dataCabActo : []
            ]);
        }else{
			return response()->success([
				'error' => true,
				'reco' => array(
					"id"=>'No estas autorizado para acceder a esta ruta'
				)
			]);	
		}
    }



    public function ListadoBienesFormaIndividual_Disposicion(Request $request)    
    {
        $hash = $request->header('Authorization', null);
        $jwtAuth = new JwtAuth();
		$checktoken = $jwtAuth->checkToken($hash);		
        if ($checktoken){
            $cod_entidad        = $request->cod_entidad;
            $nro_grupo          = $request->nro_grupo;
            $nro_clase          = $request->nro_clase;
            $cod_patrimonial    = $request->cod_patrimonial;
            $denom_bien         = $request->denom_bien;
            $page               = $request->page;
            $records            = $request->records; 

            $dataLstGrupos = DB::select('exec PA_LISTADO_GRUPOS');
            $dataLstClases = DB::select('exec PA_LISTADO_CLASES ?',[ 
                $nro_grupo
            ]);
            $dataLstBienespatrimoniales = DB::select('exec PA_LISTADO_BIENES_DISPONIBLES_DISPOSICION ?,?,?,?,?,?,?',[ 
                $cod_entidad,
                $nro_grupo, 
                $nro_clase,
                $cod_patrimonial,
                $denom_bien,
                $page,
                $records
            ]);

            return response()->success([
                "grupos" => (count($dataLstGrupos) > 0) ?$dataLstGrupos : [],
                "clases"  => (count($dataLstClases) > 0) ?$dataLstClases : [],
                "bienespatrimoniales"  => (count($dataLstBienespatrimoniales) > 0) ?$dataLstBienespatrimoniales : []
            ]);
        }else{
			return response()->success([
				'error' => true,
				'reco' => array(
					"id"=>'No estas autorizado para acceder a esta ruta'
				)
			]);	
		}
    }


    public function Guardar_ActosDisposicion(Request $request){
        $hash = $request->header('Authorization', null);
        $jwtAuth = new JwtAuth();
		$checktoken = $jwtAuth->checkToken($hash);		
        if ($checktoken){
            $reglas = [
                'id_entidad'        => 'int',
                'cod_acto_admin'    => 'int',
                'nro_resolucion'    => 'max:20',
                'fecha_resolucion'  => 'max:10',
                'id_usuario'        => 'int',
                'id_acto'           => 'int'
            ];
            $validator = Validator::make($request->all(), $reglas);
            if ($validator->fails()){
                return response()->success([
                    'error' => true,
                    'reco' => $validator->errors()
                    ]);
            }

            $id_entidad         = $request->id_entidad;
            $cod_acto_admin     = $request->cod_acto_admin;
            $nro_resolucion     = $request->nro_resolucion;
            $fecha_resolucion   = $request->fecha_resolucion;
            $tipoBeneficiario   = $request->tipoBeneficiario;
            $ruc_beneficiario   = $request->ruc_beneficiario;
            $id_usuario         = $request->id_usuario;
            $cod_patrimonial    = $request->cod_patrimonial;
            $id_acto            = $request->id_acto; 

            $dataCabActo = DB::select( "exec PA_REGISTRAR_ACTOS_DISPOSICION ?, ?, ?, ?, ?, ?, ?, ?, ?",[
                $id_entidad,
                $cod_acto_admin,
                $nro_resolucion,
                $fecha_resolucion,
                $tipoBeneficiario,
                $ruc_beneficiario,
                $id_usuario,
                $cod_patrimonial,
                $id_acto
            ]);
            //dd($id_entidad);
            return response()->success($dataCabActo[0]);
        }else{
			return response()->success([
				'error' => true,
				'reco' => array(
					"id"=>'No estas autorizado para acceder a esta ruta'
				)
			]);	
		}
    }

    public function editarActoDisposicion(Request $request){
        $hash = $request->header('Authorization', null);
        $jwtAuth = new JwtAuth();
		$checktoken = $jwtAuth->checkToken($hash);		
        if ($checktoken){
            $reglas = [
                'id_entidad'  => 'int',
                'id_acto'     => 'int'
            ];
            $validator = Validator::make($request->all(), $reglas);
            if ($validator->fails()){
                return response()->success([
                    'error' => true,
                    'reco' => $validator->errors()
                    ]);
            }

            $id_entidad = $request->id_entidad;
            $id_acto    = $request->id_acto;

            $dataCabActo = DB::select( "exec PA_EDITAR_ACTOS_DISPOSICION_CAB ?, ?",[$id_entidad,$id_acto]);
            $dataDetActo = DB::select( "exec PA_EDITAR_ACTOS_DISPOSICION_DET ?, ?",[$id_entidad,$id_acto]);
            //dd($id_entidad);
            return response()->success([
                'error' => false,
                "cabecera" => (count($dataCabActo) > 0) ?$dataCabActo[0] : [],
                "detalles"  => (count($dataDetActo) > 0) ?$dataDetActo : []
            ]);
        }else{
			return response()->success([
				'error' => true,
				'reco' => array(
					"id"=>'No estas autorizado para acceder a esta ruta'
				)
			]);	
		}
    }


    public function Eliminacion_Detalles_Disposicion(Request $request){ 
        $id_entidad         = $request->id_entidad;
        $id_acto            = $request->id_acto;
        $id_bien            = $request->id_bien;
        $id_usuario         = $request->id_usuario;

        $data = DB::select("exec PA_ELIMINAR_DETALLE_DISPOSICION_SW ?,?,?,?",[
            $id_entidad,
            $id_acto,
            $id_bien,
            $id_usuario
        ]);
        
        return response()->success([
            "datos" => (count($data) > 0) ?$data : []
        ]);
    }


    public function Eliminar_ActosDisposicion(Request $request){ 

        $reglas = [
            'id_entidad'  => 'int',
            'id_acto'     => 'int',
            'id_usuario'  => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

        $id_entidad         = $request->id_entidad;
        $id_acto            = $request->id_acto;
        $id_usuario         = $request->id_usuario;

        $data = DB::select("exec PA_ELIMINAR_ACTOS_DISPOSICION ?,?,?",[ 
            $id_entidad,
            $id_acto,
            $id_usuario
        ]);
        return response()->success([
            "error" => false,
            "datos" => (count($data) > 0) ?$data : []
        ]);
    }




    public function AdjuntarDisposiciontxt(Request $request, $data){
        
       $id_entidad	= $data[0];
       $tipo_acto = 4;
       $nomb_archivo = '';
       $usua_creacion = 100; 
       
       //$cod_alta = $cod_alta[0]->CODIGO.'_'; 
       $id_adjuntado =  DB::select( "SELECT TOP 1 ID_ADJUNTADOS_ACTOS_TL FROM TBL_ADJUNTADOS_ACTOS_TL ORDER BY ID_ADJUNTADOS_ACTOS_TL DESC" );
       
       if( empty($id_adjuntado) ){
           $id_adjuntado = 1;
       }else{
           $id_adjuntado = $id_adjuntado[0]->ID_ADJUNTADOS_ACTOS_TL;
           $id_adjuntado++  ;
       }

       if ($request->hasFile('file0')) {  
           $image = $request->file('file0');
           $ARCHIVO_EXTENSION = 'txt'; 
           $NUEVA_CADENA   = substr( md5(microtime()), 1, 4);
           $FECHA_ARCHIVO  = date('Y').''.date('m').''.date('d'); 
           $name = "disposicion/";
           
           //dd($ARCHIVO_NOMBRE_GENERADO);
           $result = DB::select(
               "exec PA_ADJUNTADO_ACTOS_MASIVO_SW ?,?,?,?",[ 
                   $id_entidad,$tipo_acto,"",$usua_creacion
                   ]
           );
           
           $id_adjuntado = $result[0]->ID_ADJUNTADO; 
           
           
           $ARCHIVO_NOMBRE_GENERADO ='TXT_'. strval($id_adjuntado).'Disposicion_Entidad_'.$id_entidad.'_Serie_'.$FECHA_ARCHIVO.'_'.$NUEVA_CADENA.'.'.$ARCHIVO_EXTENSION;
           //$url = Storage::disk('public')->putFileAs($name, $image,$ARCHIVO_NOMBRE_GENERADO); 
        //    file_put_contents("//SRV-PORTALSBN/Inventarios_zip$/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
           if(Config::get('app.APP_LINUX')){
            // file_put_contents(storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
            \Storage::disk('local')->put("public/".$name.$ARCHIVO_NOMBRE_GENERADO, file_get_contents($image));
            $origen = storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO;
            $final = '/mnt/Inventarios_zip/'.$name.$ARCHIVO_NOMBRE_GENERADO;
            shell_exec('mv '.$origen.' '.$final);
            }else{
                file_put_contents("//SRV-PORTALSBN/Inventarios_zip$/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
            }
           $Peso_Arch = filesize($image);

           
           $result2 = DB::select(
               "exec PA_ADJUNTADO_ACTOS_MASIVO_UPD_NOMBRE_ARCHIVO_SW ?,?",[ 
                   $id_adjuntado,$ARCHIVO_NOMBRE_GENERADO
                   ]
           );
           

           //return response()->json($url);
           return response()->success($result2[0]);

       }
   }


   public function CargarDisposicionTXT(Request $request){

    $reglas = [
        'id_entidad'    => 'int',
        'id_adjuntado'  => 'int',
        'usua_creacion' => 'int'
    ];
    $validator = Validator::make($request->all(), $reglas);
    if ($validator->fails()){
        return response()->success([
            'error' => true,
            'reco' => $validator->errors()
            ]);
    }

    $id_entidad     = $request->id_entidad;
    $id_adjuntado   = $request->id_adjuntado;
    $nombreArchivo  = $request->nombreArchivo;
    $usua_creacion  = $request->usua_creacion;

    $dataCarga = DB::select( "exec PA_CARGA_MASIVA_ACTOS_DISPOSICION_SW ?, ?, ?, ?",[
        $id_entidad,
        $id_adjuntado,
        $nombreArchivo,
        $usua_creacion
    ]);
    
    //return response()->success(true);
    return response()->success($dataCarga[0]);

    }

    public function ValidarDisposicionTXT(Request $request){

        $reglas = [
            'id_entidad'    => 'int',
            'id_adjuntado'  => 'int',
            'usua_creacion' => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

        $id_entidad     = $request->id_entidad;
        $id_adjuntado   = $request->id_adjuntado;
        $nombreArchivo  = $request->nombreArchivo;
        $usua_creacion  = $request->usua_creacion;

        $dataValidacion = DB::select( "exec PA_VALIDACION_MASIVA_ACTOS_DISPOSICION_SW ?, ?, ?",[
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);
        
        //return response()->success(true);
        return response()->success($dataValidacion[0]);

    }

    public function FinalizarDisposicionTXT(Request $request){

        $reglas = [
            'id_entidad'    => 'int',
            'id_adjuntado'  => 'int',
            'usua_creacion' => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

        $id_entidad     = $request->id_entidad;
        $id_adjuntado   = $request->id_adjuntado;
        $nombreArchivo  = $request->nombreArchivo;
        $usua_creacion  = $request->usua_creacion;

        $dataFinalizacion = DB::select( "exec PA_FINALIZACION_MASIVA_ACTOS_DISPOSICION_SW ?, ?, ?",[
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);
        
        //return response()->success(true);
        return response()->success($dataFinalizacion[0]);

    }

    public function ListadoErroresCargaMasivaDisposicion(Request $request){

        $accion         = 1;
        $id_entidad     = $request->id_entidad;
        $id_adjuntado    = $request->id_adjuntado;
        $usua_creacion  = $request->usua_creacion;

        $dataObservaciones_1 = DB::select( "exec PA_OBSERVACIONES_VALIDACION_MASIVA_ACTOS_DISPOSICION_SW ?, ?, ?, ?",[
            $accion,
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);

        $accion         = 2;
        $dataObservaciones_2 = DB::select( "exec PA_OBSERVACIONES_VALIDACION_MASIVA_ACTOS_DISPOSICION_SW ?, ?, ?, ?",[
            $accion,
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);

        $accion         = 3;
        $dataObservaciones_3 = DB::select( "exec PA_OBSERVACIONES_VALIDACION_MASIVA_ACTOS_DISPOSICION_SW ?, ?, ?, ?",[
            $accion,
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);

        $accion         = 4;
        $dataObservaciones_4 = DB::select( "exec PA_OBSERVACIONES_VALIDACION_MASIVA_ACTOS_DISPOSICION_SW ?, ?, ?, ?",[
            $accion,
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);

        
        return response()->success([
            "duplicado_actos"  => (count($dataObservaciones_1) > 0) ?$dataObservaciones_1 : [],
            "errores_columnas"  => (count($dataObservaciones_2) > 0) ?$dataObservaciones_2 : [],
            "no_existe_codigos"  => (count($dataObservaciones_3) > 0) ?$dataObservaciones_3 : [],
            "no_disponibles"  => (count($dataObservaciones_4) > 0) ?$dataObservaciones_4 : []
        ]);

    }
   












    public function RegistrarActosDisposicion_Captura_Errores()
    {

        DB::beginTransaction();
        try {
            DB::select('exec PA_REGISTRAR_ALUMNOS');
            DB::select('exec PA_REGISTRAR_NOTAS');
            DB::commit();
        } catch (\Illuminate\database\QueryException $e) {
            DB::rollBack();
            //echo 'Excepción capturada: ',  $e->getMessage(), "\n";

        return response()->error($e->getMessage());
        }

        return response()->success(true);
        
    }


    // public function ActualizarActosDisposicion(Request $request,$id){
    //     $DatosDisposicion = $request->vDatosDisposicion;
    //     $data = DB::select(
    //         "exec PA_ACTUALIZAR_ACTOS_DISPOSICION ?, ?",[$DatosDisposicion,$id]
    //     );
    //     return response()->success($data);
    // }


    public function comprobar_RUC_disposicion(Request $request)
    {
        $ruc = $request->ruc;
        
        $wsdl = Config::get('app.wsdlSunat');
        $client = new nusoap_client($wsdl,'wsdl');
        $client->setEndpoint($wsdl);
        $client->soap_defencoding = 'UTF-8'; 
        $client->decode_utf8 = false;
        $result = $client->call('getDatosPrincipales', ['numruc' => $ruc]);
        if ($client->getError()) {
            throw new \Exception($client->getError());
        }
        return response()->success($result['getDatosPrincipalesReturn']);
    }


    public function fecha_sql($fecha){
        date_default_timezone_set('America/Lima');
        $datetime_variable = new DateTime($fecha);
        $datetime_formatted = $datetime_variable->format('Y-m-d H:i:s');
       return  $datetime_formatted;  
    }

}
