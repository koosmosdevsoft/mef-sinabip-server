<?php

namespace sinabipmuebles\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use DB;
use Pdf;
use Pdf_Error;
use Config;
use funciones\funciones;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Helpers\JwtAuth;

class InventarioMueblesController extends Controller
{
    //
	public function index()
    {
        return 'FUNCTION INDEX';
    }

    public function ListadoHistoricoInv(Request $request,$id){ 
		// header("Access-Control-Allow-Origin: *");
		$hash = $request->header('Authorization', null);
        $jwtAuth = new JwtAuth();
		$checktoken = $jwtAuth->checkToken($hash);		
        if ($checktoken) {
			$validator = Validator::make(
				[
					'id' => $id
				],
				[
					'id' => 'int'
				]
			);
			if ($validator->fails()){
				return response()->success([
					'error' => true,
					'reco' => $validator->errors()
				]);
			}
	
			$data = DB::select(
				"exec PA_LISTADO_HISTORIAL_INV_X_ENTIDAD ?",[$id]
			);
		}else{
			$data = array(
				'status' => 'error',
				'message' => 'No estas autorizado para acceder a esta ruta'
			);
		}
		
        return response()->success($data);
    }

    public function ListadoHabilitadoInv(Request $request,$id){ 
        // header("Access-Control-Allow-Origin: *");
        $data = DB::select(
            "exec PA_INV_X_ENTIDAD_HABILITADO ?",[$id]
        );
        return response()->success($data);
    }

    public function ListadoPrediosInvEntidad(Request $request){ 
		$id    = $request->id;
		$predio    = $request->predio;
		$page    = $request->page;
		$records    = $request->records;

		if($predio == ''){
            $predio 	= '-1';
        }		
        $data = DB::select(
            "exec PA_LISTA_PADRON_PREDIOS_X_PARAMETROS_PARA_INVENTARIO ?,?,?,?",[$id,$predio,$page,$records]
        );
        return response()->success($data);
	} 
	
	public function RegistroInvSinBienes(Request $request){ 
		$id = $request->id;
		$idusuario = $request ->idusuario;
		$idpredio = $request->idpredio;

		// dd($request -> all());
		DB::beginTransaction();
        try {
            DB::statement("exec PA_REGISTRAR_INV_SIN_BIENES ?,?,?",[$id,$idusuario,$idpredio]);

            DB::commit();
        } catch (\Illuminate\database\QueryException $e) {
            DB::rollBack();
        	return response()->error($e->getMessage());
        }
        return response()->success(true);
	}
	
	public function RegistrarcabInventario(Request $request){ 

		$reglas = [
            'id'   		=> 'int',
            'cod_inv'   => 'int',
            'anno'      => 'int',
            'idusuario' => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
                ]);
        }

		$id = $request->id;
		$cod_inv = $request ->cod_inv;
		$anno = $request ->anno;
		$idusuario = $request->idusuario;

		// dd($request -> all());
		DB::beginTransaction();
        try {
            DB::statement("exec PA_CREAR_CABECERA_INVENTARIO_ENTIDAD ?,?,?,?",[$id,$cod_inv,$anno,$idusuario]);
            DB::commit();
        } catch (\Illuminate\database\QueryException $e) {
            DB::rollBack();
        	return response()->error($e->getMessage());
		}		
        $data = DB::select(
            "exec PA_CODINV_ENT ?",[$id]
        );
		return response()->success($data[0]);
	}

	public function EliminarcabInventario(Request $request){ 

		$reglas = [
            'id'   		=> 'int',
            'cod_inv'   => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

		$id = $request->id;
		$COD_INVENTARIO = $request->cod_inv;
		DB::beginTransaction();
        try {
            DB::statement("exec PA_ELIMINAR_CABECERA_INVENTARIO_ENTIDAD ?,?",[$id,$COD_INVENTARIO]);
            DB::commit();
        } catch (\Illuminate\database\QueryException $e) {
            DB::rollBack();
        	return response()->error($e->getMessage());
        }
        return response()->success(true);
	}

	public function EliminarArchivo(Request $request,$estado,$NomArch){ 
		if ($estado == 1 ){
			$name = "doc_informe_final_inv/";
		} else {
			$name = "doc_acta_conciliacion/";
		}
		$exists = Storage::disk('public')->exists($name.$NomArch);
		Storage::disk('public')->delete($name.$NomArch);
		return response()->json($exists);
    }
	
	public function SubirActaInfFinal(Request $request,$data){
		$data = explode('-',$data);
		$id	= $data[0];
		$COD_INVENTARIO = $data[1];
		$COD_INVENTARIO_ENT = $data[2];
		$ESTADO = $data[3];

        if ($request->hasFile('file0')) {
            $image = $request->file('file0');
			$ARCHIVO_EXTENSION = 'pdf';
			$NUEVA_CADENA				= substr( md5(microtime()), 1, 4);
			$FECHA_ARCHIVO				= date('Y').''.date('m').''.date('d');

			if ($ESTADO == "1"){
				$name = "doc_informe_final_inv/";
				$ARCHIVO_NOMBRE_GENERADO ='Inf_Final_Inv_'.$COD_INVENTARIO.'_E'.$id.'_IE'.$COD_INVENTARIO_ENT.'_'.$FECHA_ARCHIVO.'_'.$NUEVA_CADENA.'.'.$ARCHIVO_EXTENSION;
			} else {
				$name = "doc_acta_conciliacion/";
				$ARCHIVO_NOMBRE_GENERADO ='Acta_Concil_Inv_'.$COD_INVENTARIO.'_E'.$id.'_IE'.$COD_INVENTARIO_ENT.'_'.$FECHA_ARCHIVO.'_'.$NUEVA_CADENA.'.'.$ARCHIVO_EXTENSION;
			}

			if(Config::get('app.APP_LINUX')){
				file_put_contents(storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
				$origen = storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO;
				$url = '/mnt/Inventarios_zip/'.$name.$ARCHIVO_NOMBRE_GENERADO;
				shell_exec('cp '.$origen.' '.$url);
			}else{
				$url = Storage::disk('public')->putFileAs($name, $image,$ARCHIVO_NOMBRE_GENERADO);
			}
			//shell_exec('mv '.storage_path('app/public').$name.$ARCHIVO_NOMBRE_GENERADO.' /mnt/Invenarios_zip/'.$name.$ARCHIVO_NOMBRE_GENERADO);
			$Peso_Arch = filesize($image);
			$result = DB::statement(
                "exec PA_ACT_INFFINAL_ACTCONC ?,?,?,?,?",[
					$id,$COD_INVENTARIO_ENT,$ARCHIVO_NOMBRE_GENERADO,$Peso_Arch,$ESTADO
					]
            );
            return response()->json($url);
        }
	}

	public function EliminarArchivoInvPdf(Request $request){ 
		$id = $request->id;
		$COD_INVENTARIO_ENT = $request->COD_INVENTARIO_ENT;
		$ARCHIVO_NOMBRE_GENERADO = $request->ARCHIVO_NOMBRE_GENERADO;
		$Peso_Arch = $request->Peso_Arch;
		$ESTADO = $request ->ESTADO;
		// dd($request -> all());
		DB::beginTransaction();
		try {
			DB::statement("exec PA_ACT_INFFINAL_ACTCONC ?,?,?,?,?",[
				$id,$COD_INVENTARIO_ENT,$ARCHIVO_NOMBRE_GENERADO,$Peso_Arch,$ESTADO
				]);
			DB::commit();
		} catch (\Illuminate\database\QueryException $e) {
			DB::rollBack();
			return response()->error($e->getMessage());
		}
		return response()->success(true);
	}

	public function NombArchInffActConc(Request $request){ 
		$cod_inventario_ent = $request->cod_inventario_ent;
		$estado = $request ->estado;

        $data = DB::select(
            "exec PA_NOM_INFFINAL_ACTCONC ?,?",[$cod_inventario_ent,$estado]
        );
        return response()->success($data[0]);
	}

	public function EstadoFinalInv(Request $request){ 

		$hash = $request->header('Authorization', null);
        $jwtAuth = new JwtAuth();
		$checktoken = $jwtAuth->checkToken($hash);		
        if ($checktoken) {
				
			$reglas = [
				'id'   		=> 'int',
				'cod_inv'   => 'int'
			];
			$validator = Validator::make($request->all(), $reglas);
			if ($validator->fails()){
				return response()->success([
					'error' => true,
					'reco' => $validator->errors()
				]);
			}

			$id = $request->id;
			$COD_INVENTARIO = $request ->cod_inv;

			$datas = DB::select(
				"exec PA_ESTADO_FINAL_INV ?,?",[$id,$COD_INVENTARIO]
			);
			$data =  $datas[0];
			return response()->success($data);
		}else{
			return response()->success([
				'error' => true,
				'reco' => array(
					"id"=>'No estas autorizado para acceder a esta ruta'
				)
			]);	
		}



        
	}

	public function NombArchInffActConcUnion(Request $request,$cod_inventario_ent){ 
		// $cod_inventario_ent = $request->cod_inventario_ent;

        $data = DB::select(
            "exec PA_NOM_INFFINAL_ACTCONC_UNION ?",[$cod_inventario_ent]
        );
        return response()->success($data);
    }

	public function HabilitarBotonInv(Request $request,$id){ 
        $data = DB::select(
            "exec PA_HABILITAR_BOTON_FINALIZA_INV ?",[$id]
        );
        return response()->success($data[0]);
    }

	public function ListadoNombreAdjunto(Request $request){ 
		$id = $request->id;
		$idpredio = $request->idpredio;

        $data = DB::select(
            "exec PA_NOMARCHIVO_ADJUNTADO ?,?",[$id,$idpredio]
        );
        return response()->success($data);
    }

	public function ValidaAsignacionInv(Request $request,$id){ 
        $data = DB::select(
            "exec PA_VALIDA_ASIGNACION_INV ?",[$id]
        );
        return response()->success($data[0]);
    }

	public function EliminarAdjunto(Request $request){ 
		$id = $request->id;
		$idpredio = $request->idpredio;
		$idusuario = $request->idusuario;

		// dd($request -> all());
		DB::beginTransaction();
        try {
            DB::statement("exec PA_ELIMINAR_ADJUNTADO ?,?,?",[$id,$idpredio,$idusuario]);
            DB::commit();
        } catch (\Illuminate\database\QueryException $e) {
            DB::rollBack();
        	return response()->error($e->getMessage());
        }
        return response()->success(true);
	}
	
	// public function FinalizarInvertario(Request $request){ 
	// 	$id = $request->id;
	// 	$COD_INVENTARIO = $request->cod_inv;
	// 	$ANIO = $request->anno;
	// 	$COD_USUARIO = $request->idusuario;

	// 	DB::beginTransaction();
    //     try {
    //         DB::statement("exec SP_FINALIZAR_PROCESO_INVENTARIO ?,?,?,?",[$id,$COD_INVENTARIO,$ANIO,$COD_USUARIO]);
    //         DB::commit();
    //     } catch (\Illuminate\database\QueryException $e) {
    //         DB::rollBack();
    //     	return response()->error($e->getMessage());
    //     }
    //     return response()->success(true);
	// }
	
	public function FinalizarInvertario(Request $request){ 
		$id = $request->id;
		$COD_INVENTARIO = $request->cod_inv;
		$ANIO = $request->anno;
		$COD_USUARIO = $request->idusuario;

		$data = DB::select(
            "exec SP_FINALIZAR_PROCESO_INVENTARIO ?,?,?,?",[$id,$COD_INVENTARIO,$ANIO,$COD_USUARIO]
		);
		
		return response()->success(true);
	}


	public function EnviarAdjunto(Request $request){ 
		$id = $request->id;
		$idpredio = $request->idpredio;
		$idusuario = $request->idusuario;
		// dd($request -> all());
		DB::beginTransaction();
        try {
            DB::statement("exec PA_ENVIA_CARGA_INV ?,?,?",[$id,$idpredio,$idusuario]);
            DB::commit();
        } catch (\Illuminate\database\QueryException $e) {
            DB::rollBack();
        	return response()->error($e->getMessage());
        }
        return response()->success(true);
	}

	public function VerPD(Request $request,$estado,$archivo){
		// $archivo = $request->archivo;
		// $estado = $request->estado;
		
		if ($estado == "1"){
			if(Config::get('app.APP_LINUX'))
				$name = '/mnt/Inventarios_zip/'."doc_informe_final_inv/";
			else
				$name = ruta_server()."doc_informe_final_inv/";
		}else{
			if(Config::get('app.APP_LINUX'))
				$name = '/mnt/Inventarios_zip/'."doc_acta_conciliacion/";
			else
				$name = ruta_server()."doc_acta_conciliacion/";
		}
		$contents = $name.$archivo;
		header("Content-type: application/pdf");
		header('Content-Disposition: inline; filename="'.$contents.'"');
		$data = file_get_contents($contents);
		echo $data;
		exit;
	}

	public function SubirZip(Request $request,$data){ 
		$data = explode('-',$data);
		$id	= $data[0];
		$idpredio = $data[1];
		$idusuario = $data[2];
        if ($request->hasFile('file0')) {
			$image = $request->file('file0');
			$name = "Sin_procesar1/";
			$ARCHIVO_EXTENSION = '.TXT';
			$ARCHIVO_NOMBRE_GENERADO = explode('.zip',$image->getClientOriginalName()); 
			$ARCHIVO_NOMBRE = $ARCHIVO_NOMBRE_GENERADO[0].$ARCHIVO_EXTENSION;	 

			$VERSION =  DB::select(
				"SELECT ESTADO_PARAMETRO FROM TBL_PARAMETRIZACION_TL WHERE COD_PARAMETRO_CAB = 'VERSION_ZIP' AND ESTADO = 1 AND ID_PARAM = 53"
			);

			$validaNombre = explode('InventarioInicial_'.$VERSION[0]->ESTADO_PARAMETRO.'_',$ARCHIVO_NOMBRE_GENERADO[0]);
			
			if(count($validaNombre)> 1){	
				//$ARCHIVO_NOMBRE =  uniqid(rand(), true).$ARCHIVO_EXTENSION;	
				// $url = Storage::disk('public')->putFileAs($name, $image,$image->getClientOriginalName());
				// file_put_contents("//SRV-PORTALSBN/Inventarios_zip$/".$name.$image->getClientOriginalName(),file_get_contents($image));
				if(Config::get('app.APP_LINUX')){
					file_put_contents(storage_path('app/public')."/".$name.$image->getClientOriginalName(),file_get_contents($image));
					$origen = storage_path('app/public')."/".$name.$image->getClientOriginalName();
					$final = '/mnt/Inventarios_zip/'.$name.$image->getClientOriginalName();
					// shell_exec('mv '.$origen.' '.$final);
					exec("cp '$origen' '$final'", $output, $retrun_var);
				}else{
					file_put_contents("//SRV-PORTALSBN/Inventarios_zip$/".$name.$image->getClientOriginalName(),file_get_contents($image));
				}
				$Peso_Arch = filesize($image);
				$result = DB::statement(
					"exec PA_ADJUNTAR_ARCHIVO ?,?,?,?,?",[
						$id,$idpredio,$ARCHIVO_NOMBRE,$Peso_Arch,$idusuario
						]
				);
				return response()->json($result);
			}else{
				return response()->json([
					'type' => 'error',
					'message' => 'Nombre de archivo incorrecto'
				]);
			}
        }
	}
	
    public function TotalInvActivo(Request $request,$id){ 
        // header("Access-Control-Allow-Origin: *");
        $data = DB::select(
            "exec PA_TOTAL_INV_ACTIVO_X_ENTIDAD ?",[$id]
        );
        $TOTAL_ACTIVO = $data[0]->TOTAL_ACTIVO;
        if($TOTAL_ACTIVO >= '1'){
            $ruta 	= 'inv-resumen';
        }else{
            $ruta 	= 'inv-lista-predios' ;
        }
        $data[0]->Ruta = $ruta;
        return response()->success((count($data) > 0) ?$data[0] : []
        );
	}

	public function ListadoPadronPredios_Id(Request $request,$predio_id){ 
		// header("Access-Control-Allow-Origin: *");
		// dd($request -> all());
        $data = DB::select(
            "exec PA_LISTA_PADRON_PREDIOS_ID ?",[$predio_id]
        );
        return response()->success($data[0]);
	}

	public function ListadoUsuario_Id(Request $request,$id,$cod_inv){ 
        $data = DB::select(
            "exec PA_LISTA_DATOS_SIMI_PERSONAL_CODIGO ?,?",[$id,$cod_inv]
        );
        return response()->success($data[0]);
	}
	
	public function ListadoPreviaInvFinal(Request $request,$id,$periodo){ 
        $data = DB::select(
            "exec PA_MOSTRAR_PREVIA_INVENTARIO_FINAL ?,?",[$id,$periodo]
        );
        return response()->success($data[0]);
	}
	
	public function ListadoPreviaDetalleInvFinal(Request $request,$id,$page,$records){ 
        $data = DB::select(
            "exec PA_TOTAL_DETALLE_X_LOCAL ?,?,?",[$id,$page,$records]
        );
        return response()->success($data);
	}
		
	public function ValidaResponsablePredio(Request $request){ 
		// header("Access-Control-Allow-Origin: *");

		$reglas = [
            'id'   => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

		$id = $request->id;
		// dd($id);
        $data = DB::select(
            "exec PA_INV_RESP_PREDIO ?",[$id]
        );
        return response()->success($data);
	}

	public function NombreEntidad(Request $request,$id){ 
        $data = DB::select(
            "exec PA_NOMBRE_ENTIDAD ?",[$id]
        );
        return response()->success($data);
	}

	public function BuscarEntidad(Request $request,$id,$token){ 
        // $id	= $request->id;
		// $hash = $request->header('Authorization', null);
        $jwtAuth = new JwtAuth();
		$checktoken = $jwtAuth->checkToken($token);		
        if ($checktoken) {
            $data = DB::select(
				"exec PA_BUSCAR_ENTIDAD ?",[$id]
			);
			return response()->success($data);
        }else{
			return response()->success([
				'error' => true,
				'reco' => array(
					"id"=>'No estas autorizado para acceder a esta ruta'
				)
			]);	
		}
        
    }

	// public function ValidaDepreciacioInv(Request $request){ 
    //     $id	= $request->id;

    //     $data = DB::select(
    //         "exec PA_VALIDA_DEPRECIACION_INV ?",[$id]
    //     );
    //     return response()->success($data);
    // }

    public function Pdf_ObservacionInv(Request $request,$id,$predio_id){
		header('Content-type: application/pdf');
		$NOM_ENTIDAD =  DB::select(
			"SELECT NOM_ENTIDAD FROM TBL_PADRON_ENTIDAD where COD_ENTIDAD = ?",[$id]
		);

		$NOM_PREDIO =  DB::select(
			"SELECT DENOMINACION_PREDIO FROM TBL_PADRON_PREDIOS WHERE ID_ESTADO=1 AND COD_ENTIDAD=? AND ID_PREDIO_SBN= ?",[$id,$predio_id]
		);

		// return response()->success($NOM_ENTIDAD);  
		$data_ErrTotal = DB::select(
			"exec PA_ERRORES_INV_ADJUNTADO ?,?,?",[$id,$predio_id,4]
		);
		$TOTAL_ARC		= number_format($data_ErrTotal[0]->TOTAL_ARC,0);
		$TOTAL_BIEN		= number_format($data_ErrTotal[0]->TOTAL_BIEN,0);
		$TOTAL_ACTO		= number_format($data_ErrTotal[0]->TOTAL_ACTO,0);
		$TOTAL		    = number_format($data_ErrTotal[0]->TOTAL,0);

		$Pdf_Error = new Pdf_Error;

		$Pdf_Error->AliasNbPages();
		$Pdf_Error->SetFont('Arial','',10);
		$Pdf_Error->AddPage();
		$Pdf_Error->Ln(4);

		// //****************************************
		// //****************************************
		
		$Pdf_Error->SetFillColor(205,205,205);//color de fondo tabla
		$Pdf_Error->SetTextColor(10);
		$Pdf_Error->SetDrawColor(153,153,153);
		$Pdf_Error->SetLineWidth(.3);
		$Pdf_Error->SetFont('Arial','B',9);
		
		$Pdf_Error->SetFillColor(205,205,205);
		$Pdf_Error->Cell(45,8,'ENTIDAD',1,0,'L',true);		
		$Pdf_Error->SetFillColor(255,255,255);	
		$Pdf_Error->Cell(140,8,$NOM_ENTIDAD[0]->NOM_ENTIDAD,1,0,'L',true);
		$Pdf_Error->Ln();
		$Pdf_Error->SetFillColor(205,205,205);
		$Pdf_Error->Cell(45,8,'PREDIO Y/O LOCAL',1,0,'L',true);		
		$Pdf_Error->SetFillColor(255,255,255);	
		$Pdf_Error->Cell(140,8,$NOM_PREDIO[0]->DENOMINACION_PREDIO,1,0,'L',true);
						
		$Pdf_Error->Ln();
		$Pdf_Error->Ln(3);

		$Pdf_Error->SetFont('Arial','B',9);
		$Pdf_Error->SetFillColor(205,205,205);	
		$Pdf_Error->Cell(185,5,utf8_decode('TOTAL DE ERRORES DETECTADOS'),1,0,'C',true);
		$Pdf_Error->Ln();
		$Pdf_Error->SetFillColor(230,230,230);	
		$Pdf_Error->Cell(100,6,utf8_decode('TOTAL DE ERRORES DE ARCHIVO'),1,0,'L',true);		
		$Pdf_Error->SetFillColor(255,255,255);		
		$Pdf_Error->Cell(85,6,$TOTAL_ARC,1,0,'L',true);	
		$Pdf_Error->Ln();
		$Pdf_Error->SetFillColor(230,230,230);	
		$Pdf_Error->Cell(100,6,utf8_decode('TOTAL DE ERRORES DE DUPLICADO DE BIENES'),1,0,'L',true);		
		$Pdf_Error->SetFillColor(255,255,255);		
		$Pdf_Error->Cell(85,6,$TOTAL_BIEN,1,0,'L',true);	
		$Pdf_Error->Ln();
		$Pdf_Error->SetFillColor(230,230,230);	
		$Pdf_Error->Cell(100,6,utf8_decode('TOTAL DE ERRORES DE DUPLICADO DE ACTO'),1,0,'L',true);		
		$Pdf_Error->SetFillColor(255,255,255);		
		$Pdf_Error->Cell(85,6,$TOTAL_ACTO,1,0,'L',true);	
		$Pdf_Error->Ln();
		$Pdf_Error->SetFillColor(230,230,230);	
		$Pdf_Error->Cell(100,6,utf8_decode('SUMA TOTAL DE ERRORES'),1,0,'L',true);		
		$Pdf_Error->SetFillColor(255,255,255);		
		$Pdf_Error->Cell(85,6,$TOTAL,1,0,'L',true);	
				
		/*******************************************************/
		//DETALLE DE ERRORES DE ARCHIVO
		/*******************************************************/
		
		$Pdf_Error->Ln();
		$Pdf_Error->Ln(3);

		$data_ErrArch = DB::select(
			"exec PA_ERRORES_INV_ADJUNTADO ?,?,?",[$id,$predio_id,1]
		);

		if (!empty($data_ErrArch)) {

		//DETALLE DEL BIEN
		$Pdf_Error->SetFont('Arial','B',9);
		$Pdf_Error->SetFillColor(205,205,205);	
		$Pdf_Error->Cell(185,5,utf8_decode('DETALLE DE ERRORES DE ARCHIVO'),1,0,'L',true);

		$Pdf_Error->Ln();
		
		$Pdf_Error->SetFont('Arial','B',8);
		$w = array(10, 45, 130);
		$Pdf_Error->Cell($w[0],5,utf8_decode('ITEM'),1,0,'C',true);
		$Pdf_Error->Cell($w[1],5,utf8_decode('COLUMNA'),1,0,'C',true);
		$Pdf_Error->Cell($w[2],5,utf8_decode('DESCRIPCION ERRORES'),1,0,'C',true);
		$Pdf_Error->Ln();
		
		// Restauración de colores y fuentes
		$Pdf_Error->SetFillColor(224,235,255);
		$Pdf_Error->SetTextColor(0);
		$Pdf_Error->SetFont('Arial','',7);
		// Datos
		$fill = false;
		
		$contador = 0;
		
        foreach ($data_ErrArch as $key => $value) {
			$ITEM					= $value->ITEM;
			$COLUMNA				= $value->COLUMNA;
			$DESCRIPCION_ERRORES	= $value->DESCRIPCION_ERRORES;

			$contador++;
            $linea = 0;
            
			if($contador == 38){
				$Pdf_Error->Cell(array_sum($w),0,'','T');
				$Pdf_Error->AddPage();
				$linea = 0;
			}else{
				$linea++;
			}
				
			if($linea!= 0 && $linea%60==0 ){
				$Pdf_Error->Cell(array_sum($w),0,'','T');
				$Pdf_Error->AddPage();
			 }
	
			$Pdf_Error->Cell($w[0],4,$ITEM,'LR',0,'C');
			$Pdf_Error->Cell($w[1],4,$COLUMNA,'LR',0,'C');
			$Pdf_Error->Cell($w[2],4,utf8_decode($DESCRIPCION_ERRORES),'LR',0,'L');
			$Pdf_Error->Ln();
			$fill = !$fill;
        }

		// Línea de cierre
		$Pdf_Error->Cell(array_sum($w),0,'','T');
			
		$Pdf_Error->Ln();

	 	}
		/*******************************************************/
		//DETALLE DE ERRORES DE DUPLICADO DE BIENES
		/*******************************************************/
		
		$data_ErrBien = DB::select(
			"exec PA_ERRORES_INV_ADJUNTADO ?,?,?",[$id,$predio_id,2]
		);
		
		if (!empty($data_ErrBien)) {

		$Pdf_Error->Ln(3);
		//DETALLE DEL BIEN
		$Pdf_Error->SetFont('Arial','B',9);
		$Pdf_Error->SetFillColor(205,205,205);	
		$Pdf_Error->Cell(185,5,utf8_decode('DETALLE DE ERRORES DE DUPLICADO DE BIENES'),1,0,'L',true);

		$Pdf_Error->Ln();
		
		$Pdf_Error->SetFont('Arial','B',8);
		$w = array(10, 45, 35,60,35);
		$Pdf_Error->Cell($w[0],5,utf8_decode('FILA'),1,0,'C',true);
		$Pdf_Error->Cell($w[1],5,utf8_decode('PREDIO ORIGEN'),1,0,'C',true);
		$Pdf_Error->Cell($w[2],5,utf8_decode('COD. PATRIMONIAL'),1,0,'C',true);
		$Pdf_Error->Cell($w[3],5,utf8_decode('DENOMINACION BIEN'),1,0,'C',true);
		$Pdf_Error->Cell($w[4],5,utf8_decode('NRO DOC ADQUISICION'),1,0,'C',true);
		$Pdf_Error->Ln();
		
		// Restauración de colores y fuentes
		$Pdf_Error->SetFillColor(224,235,255);
		$Pdf_Error->SetTextColor(0);
		$Pdf_Error->SetFont('Arial','',7);
		// Datos
		$fill = false;
		
		$contador = 0;
		
        foreach ($data_ErrBien as $key => $value) {
			$FILA					= $value->Row;
			$ID_PREDIO_ORG			= $value->ID_PREDIO_ORG;
			$CODIGO_PATRIMONIAL		= $value->CODIGO_PATRIMONIAL;
			$DENOMINACION_BIEN		= $value->DENOMINACION_BIEN;
			$NRO_DOC_ADQUISICION	= $value->NRO_DOC_ADQUISICION;

			$contador++;
            $linea = 0;
            
			if($contador == 38){
				$Pdf_Error->Cell(array_sum($w),0,'','T');
				$Pdf_Error->AddPage();
				$linea = 0;
			}else{
				$linea++;
			}
						
			if($linea!= 0 && $linea%60==0 ){
				$Pdf_Error->Cell(array_sum($w),0,'','T');
				$Pdf_Error->AddPage();
			 }
	
			$Pdf_Error->Cell($w[0],4,$contador,'LR',0,'C');
			$Pdf_Error->Cell($w[1],4,$ID_PREDIO_ORG,'LR',0,'C');
			$Pdf_Error->Cell($w[2],4,$CODIGO_PATRIMONIAL,'LR',0,'C');
			$Pdf_Error->Cell($w[3],4,utf8_decode($DENOMINACION_BIEN),'LR',0,'L');
			$Pdf_Error->Cell($w[4],4,$NRO_DOC_ADQUISICION,'LR',0,'L');
			$Pdf_Error->Ln();
			$fill = !$fill;
        }

		// Línea de cierre
		$Pdf_Error->Cell(array_sum($w),0,'','T');
			
		$Pdf_Error->Ln();

		}
		/*******************************************************/
		//DETALLE DE ERRORES DE DUPLICADO DE ACTOS
		/*******************************************************/

		$Pdf_Error->Ln(3);

		$data_ErrActo = DB::select(
			"exec PA_ERRORES_INV_ADJUNTADO ?,?,?",[$id,$predio_id,3]
		);

		if (!empty($data_ErrActo)) {

		$Pdf_Error->Ln();
		$Pdf_Error->SetFont('Arial','',9);
		$Pdf_Error->SetFillColor(205,205,205);	
		$Pdf_Error->Cell(185,5,utf8_decode('Nota: La siguiente lista muestra las veces que se repiten la columna "Nro. Doc Actos" en la carga del inventario con diferentes'),0,0,'L',false);		
		$Pdf_Error->Ln();
		$Pdf_Error->Cell(185,5,utf8_decode('fechas y/o tipo causal alta. La columna "Cantidad Repetidos" es el número de veces que cambia la fecha del documento.'),0,0,'L',false);		
		$Pdf_Error->Ln();
		$Pdf_Error->Cell(185,5,utf8_decode('Ej. El Nro. Doc Actos 100-2018 se registraron 10 veces con diferentes fechas, para corregir se tienen que actualizar las fechas'),0,0,'L',false);
		$Pdf_Error->Ln();
		$Pdf_Error->Cell(185,5,utf8_decode('de los documentos.'),0,0,'L',false);

		$Pdf_Error->Ln();
		$Pdf_Error->Ln();
		
		//DETALLE DEL BIEN
		$Pdf_Error->SetFont('Arial','B',9);
		$Pdf_Error->SetFillColor(205,205,205);	
		$Pdf_Error->Cell(185,5,utf8_decode('DETALLE DE ERRORES DE DUPLICADO POR ACTO'),1,0,'L',true);

		$Pdf_Error->Ln();
		$Pdf_Error->SetFont('Arial','B',8);
		$w = array(10, 45, 96,34);
		$Pdf_Error->Cell($w[0],5,utf8_decode('FILA'),1,0,'C',true);
		$Pdf_Error->Cell($w[1],5,utf8_decode('TIPO ACTOS'),1,0,'C',true);
		$Pdf_Error->Cell($w[2],5,utf8_decode('NRO DOC ACTOS'),1,0,'C',true);
		$Pdf_Error->Cell($w[3],5,utf8_decode('CANTIDAD REPETIDOS'),1,0,'C',true);
		$Pdf_Error->Ln();
		
		// Restauración de colores y fuentes
		$Pdf_Error->SetFillColor(224,235,255);
		$Pdf_Error->SetTextColor(0);
		$Pdf_Error->SetFont('Arial','',7);
		// Datos
		$fill = false;
		
		$contador = 0;
		
        foreach ($data_ErrActo as $key => $value) {
			$FILA				= $value->Row;
			$TIPO_ACTOS			= $value->TIPO_ACTOS;
			$NRO_DOC_ACTOS		= $value->NRO_DOC_ACTOS;
			$CANTIDAD_REPETIDOS	= $value->CANTIDAD_REPETIDOS;

			$contador++;
            $linea = 0;
            
			if($contador == 38){
				$Pdf_Error->Cell(array_sum($w),0,'','T');
				$Pdf_Error->AddPage();
				$linea = 0;
			}else{
				$linea++;
			}
						
			if($linea!= 0 && $linea%60==0 ){
				$Pdf_Error->Cell(array_sum($w),0,'','T');
				$Pdf_Error->AddPage();
			 }
	
			$Pdf_Error->Cell($w[0],4,$FILA,'LR',0,'C');
			$Pdf_Error->Cell($w[1],4,utf8_decode($TIPO_ACTOS),'LR',0,'C');
			$Pdf_Error->Cell($w[2],4,$NRO_DOC_ACTOS,'LR',0,'L');
			$Pdf_Error->Cell($w[3],4,$CANTIDAD_REPETIDOS,'LR',0,'R');
			$Pdf_Error->Ln();
			$fill = !$fill;
        }

		// Línea de cierre
		$Pdf_Error->Cell(array_sum($w),0,'','T');
			
		$Pdf_Error->Ln();
		}

		$Pdf_Error->Output();
		exit;
   	}

    public function Pdf_SustentoInventario(Request $request,$id,$anno){
    header('Content-type: application/pdf');
    $NOM_ENTIDAD =  DB::select(
        "SELECT NOM_ENTIDAD FROM TBL_PADRON_ENTIDAD where COD_ENTIDAD = ?",[$id]
    );  

    $data_cab = DB::select(
        "exec PA_LISTAR_INV_HABILITADO_ENTIDAD_AÑO ?,?",[$id,$anno]
    );

    $COD_INVENTARIO			= $data_cab[0]->COD_INVENTARIO;
	$NOM_INVENTARIO			= $data_cab[0]->NOM_INVENTARIO;
	$PERIODO				= $data_cab[0]->PERIODO;
    $FECHA_INICIO		    = $data_cab[0]->FECHA_INICIO;	
	$FECHA_TERMINO			= $data_cab[0]->FECHA_TERMINO;
	$HABILITADO				= $data_cab[0]->HABILITADO;
	$COD_INVENTARIO_ENT		= $data_cab[0]->COD_INVENTARIO_ENT;
	$TOTAL_LOCAL_GRAL		= number_format($data_cab[0]->TOTAL_LOCAL_GRAL,0);
	$TOTAL_LOCAL_SI_INV		= number_format($data_cab[0]->TOTAL_LOCAL_SI_INV,0);
	$TOTAL_LOCAL_NO_INV		= number_format($data_cab[0]->TOTAL_LOCAL_NO_INV,0);
	$TOTAL_BIENES_GRAL		= number_format($data_cab[0]->TOTAL_BIENES_GRAL,0);
	$TOTAL_BIENES_ACTIVOS	= number_format($data_cab[0]->TOTAL_BIENES_ACTIVOS,0);
	$TOTAL_BIENES_BAJAS		= number_format($data_cab[0]->TOTAL_BIENES_BAJAS,0);
	$FECHA_FINALIZA_INV		= $data_cab[0]->FECHA_FINALIZA_INV;
	$ID_ESTADO_INV			= $data_cab[0]->ID_ESTADO_INV;
	$COD_UE_PERS_CP			= $data_cab[0]->COD_UE_PERS_CP;	
	$COD_VERIFICACION		= $data_cab[0]->COD_VERIFICACION;
		
	$NOM_ARCHIVO_INF_FINAL_INV		= $data_cab[0]->NOM_ARCHIVO_INF_FINAL_INV;
	$ADJ_ARCHIVO_INF_FINAL_INV 		= ($NOM_ARCHIVO_INF_FINAL_INV != '') ? "X" : "" ;
	$FECHA_ARCHIVO_INF_FINAL_INV	= $data_cab[0]->FECHA_ARCHIVO_INF_FINAL_INV;
	
	
	$NOM_ARCHIVO_ACTA_CONCILI_INV		= $data_cab[0]->NOM_ARCHIVO_ACTA_CONCILI_INV;
	$ADJ_ARCHIVO_ACTA_CONCILI_INV 		= ($NOM_ARCHIVO_ACTA_CONCILI_INV != '') ? "X" : "" ;
	$FECHA_ARCHIVO_ACTA_CONCILI_INV		= $data_cab[0]->FECHA_ARCHIVO_ACTA_CONCILI_INV;

    $data_personal = DB::select(
        "exec PA_LISTA_DATOS_SIMI_PERSONAL_CODIGO ?,?",[$id,$COD_INVENTARIO] 
    );
    
	$NRO_DOCUMENTO			= $data_personal[0]->NRO_DOCUMENTO;
	$RESP_UE_NOMBRES_PERS	= $data_personal[0]->UE_NOMBRES_PERS;
	$RESP_UE_APEPAT_PERS	= $data_personal[0]->UE_APEPAT_PERS;
	$RESP_UE_APEMAT_PERS	= $data_personal[0]->UE_APEMAT_PERS;

    if($ID_ESTADO_INV == '1'){
		$Desc_Estado 	= 'FINALIZADO';
	}else{
		$Desc_Estado 	= 'SIN REGISTRAR';
	}
	
     $pdf = new Pdf;

     $pdf->AliasNbPages();

		$pdf->SetFont('Arial','',10);
		$pdf->AddPage();
		$pdf->Ln(4);

		//****************************************
		//****************************************
		
		$pdf->SetFillColor(205,205,205);//color de fondo tabla
		$pdf->SetTextColor(10);
		$pdf->SetDrawColor(153,153,153);
		$pdf->SetLineWidth(.3);
		$pdf->SetFont('Arial','B',9);
		
		$pdf->SetFillColor(205,205,205);
		$pdf->Cell(45,8,'ENTIDAD',1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);	
		$pdf->Cell(140,8,$NOM_ENTIDAD[0]->NOM_ENTIDAD,1,0,'L',true);
				
		$pdf->Ln();
		$pdf->Ln(3);
		
		//****************************************
		// DATOS DEL PERSONAL Y CONTROL PATRIMONIAL
		//****************************************
		
		//--------------------		
		//DATOS DEL INVENTARIO
		//--------------------
		$pdf->SetFillColor(205,205,205);	
		$pdf->Cell(185,7,utf8_decode('DATOS DEL INVENTARIO '. $PERIODO),1,0,'L',true);		
		
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(52,6,utf8_decode('Periodo'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(40,6,$PERIODO,1,0,'L',true);	
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(52,6,utf8_decode('Estado del Inventario'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(41,6,utf8_decode($Desc_Estado),1,0,'L',true);
		
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(52,6,utf8_decode('Fecha Inicio del Reg. Inventario'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(40,6,$FECHA_INICIO,1,0,'L',true);
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(52,6,utf8_decode('Fecha Vence el Reg. Inventario'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(41,6,$FECHA_TERMINO,1,0,'L',true);
		
		$pdf->Ln();
		
		$pdf->SetFillColor(205,205,205);	
		$pdf->Cell(185,3,'',1,0,'L',true);	
		
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(52,6,utf8_decode('Fecha Finaliza Inventario'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(40,6,$FECHA_FINALIZA_INV,1,0,'L',true);
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(52,6,utf8_decode('Codigo de Finalización'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(41,6,$COD_VERIFICACION,1,0,'L',true);
		
				
		$pdf->Ln();
		$pdf->Ln(3);
		
		//--------------------		
		//DATOS DEL RESPONSABLE DE CONTROL PATRIMONIAL 
		//--------------------
		$pdf->SetFillColor(205,205,205);	
		$pdf->Cell(185,7,utf8_decode('DATOS DEL RESPONSABLE DE CONTROL PATRIMONIAL'),1,0,'L',true);		
				
		$pdf->Ln();
		
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(32,6,utf8_decode('Nro de DNI'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(60,6,$NRO_DOCUMENTO,1,0,'L',true);	
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(32,6,utf8_decode('Nombres'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(61,6,utf8_decode($RESP_UE_NOMBRES_PERS),1,0,'L',true);
		
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(32,6,utf8_decode('Apellido Paterno'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(60,6,utf8_decode($RESP_UE_APEPAT_PERS),1,0,'L',true);
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(32,6,utf8_decode('Apellido Materno'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(61,6,utf8_decode($RESP_UE_APEMAT_PERS),1,0,'L',true);
		
						
		$pdf->Ln();
		$pdf->Ln(3);
		
		//--------------------
		//TOTALIDAD
		//--------------------
		
		$pdf->SetFillColor(205,205,205);
		$pdf->Cell(185,7,utf8_decode('TOTAL DEL INVENTARIO ACUMULADO A LA FECHA'),1,0,'L',true);		
		
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(68,6,utf8_decode('Total locales y/o predios'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(25,6,$TOTAL_LOCAL_GRAL,1,0,'L',true);	
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(68,6,utf8_decode('Total Bienes en General'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(24,6,$TOTAL_BIENES_GRAL,1,0,'L',true);
		
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(68,6,utf8_decode('Total locales y/o predios con Inventario'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(25,6,$TOTAL_LOCAL_SI_INV,1,0,'L',true);
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(68,6,utf8_decode('Total Bienes Muebles Activos'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(24,6,$TOTAL_BIENES_ACTIVOS,1,0,'L',true);
		
		
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(68,6,utf8_decode('Total locales y/o predios sin Inventario'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(25,6,$TOTAL_LOCAL_NO_INV,1,0,'L',true);
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(68,6,utf8_decode('Total Bienes Muebles Bajas'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(24,6,$TOTAL_BIENES_BAJAS,1,0,'L',true);
		
		
		//--------------------
		//INFORME FINAL
		//--------------------
		
		$pdf->Ln();
		$pdf->Ln(3);

		
		$pdf->SetFillColor(205,205,205);	
		$pdf->Cell(185,7,utf8_decode('INFORME FINAL DEL INVENTARIO'),1,0,'L',true);		
		
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(68,6,utf8_decode('Nombre del Archivo Adjunto'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(117,6,utf8_decode($NOM_ARCHIVO_INF_FINAL_INV),1,0,'L',true);	
		//$pdf->WriteHTML('<a href="http://www.sbn.gob.pe/repositorio_muebles/inventario/doc_informe_final_inv/'.utf8_decode($NOM_ARCHIVO_INF_FINAL_INV).'" target="_blank">'.utf8_decode($NOM_ARCHIVO_INF_FINAL_INV).'</a>');
		
		
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(68,6,utf8_decode('Adjunto Archivo'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(25,6,utf8_decode($ADJ_ARCHIVO_INF_FINAL_INV),1,0,'L',true);	
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(68,6,utf8_decode('Fecha del Adjunto'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(24,6,$FECHA_ARCHIVO_INF_FINAL_INV,1,0,'L',true);
		
		
		//--------------------
		//ACTA DE CONCILIACIÓN
		//--------------------
		
		$pdf->Ln();
		$pdf->Ln(3);

		
		$pdf->SetFillColor(205,205,205);	
		$pdf->Cell(185,7,utf8_decode('ACTA DE CONCILIACIÓN'),1,0,'L',true);		
		
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(68,6,utf8_decode('Nombre del Archivo Adjunto'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(117,6,utf8_decode($NOM_ARCHIVO_ACTA_CONCILI_INV),1,0,'L',true);	
		
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(68,6,utf8_decode('Adjunto Archivo'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(25,6,utf8_decode($ADJ_ARCHIVO_ACTA_CONCILI_INV),1,0,'L',true);	
		//$pdf->WriteHTML('<a href="http://www.sbn.gob.pe/repositorio_muebles/inventario/doc_informe_final_inv/'.utf8_decode($ADJ_ARCHIVO_ACTA_CONCILI_INV).'" target="_blank">'.utf8_decode($ADJ_ARCHIVO_ACTA_CONCILI_INV).'</a>');
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(68,6,utf8_decode('Fecha del Adjunto'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(24,6,$FECHA_ARCHIVO_ACTA_CONCILI_INV,1,0,'L',true);
				
		/*******************************************************/
		//DETALLE DEL BIEN
		/*******************************************************/
		
		$pdf->Ln();
		$pdf->Ln(5);

		
		//DETALLE DEL BIEN
		$pdf->SetFont('Arial','B',9);
		$pdf->SetFillColor(205,205,205);	
		$pdf->Cell(146,5,utf8_decode('DETALLE DEL INVENTARIO ' . $anno . ' REPORTADO'),1,0,'L',true);
		
		$pdf->SetFillColor(205,205,205);
		$pdf->Cell(39,5,utf8_decode('Total Bienes'),1,0,'C',true);
	
		
		$pdf->Ln();
		
		$pdf->SetFont('Arial','B',8);
		$w = array(8, 138, 13, 13, 13);
		$pdf->Cell($w[0],5,utf8_decode('Item'),1,0,'L',true);
		$pdf->Cell($w[1],5,utf8_decode('Local y/o Predio'),1,0,'L',true);
		$pdf->Cell($w[2],5,utf8_decode('General'),1,0,'C',true);
		$pdf->Cell($w[3],5,utf8_decode('Activos'),1,0,'C',true);
		$pdf->Cell($w[4],5,utf8_decode('Bajas'),1,0,'C',true);
		$pdf->Ln();
		
		// Restauración de colores y fuentes
		$pdf->SetFillColor(224,235,255);
		$pdf->SetTextColor(0);
		$pdf->SetFont('Arial','',7);
		// Datos
		$fill = false;
		
		
        $contador = 0;
        
		$COD_INVENTARIO_ENT = $data_cab[0]->COD_INVENTARIO_ENT;
		
        $data_det = DB::select(
            "exec PA_LISTA_PREDIO_CON_INVENTARIO_X_ENTIDAD ?",[$COD_INVENTARIO_ENT]
        );

        foreach ($data_det as $key => $value) {
            $DENOMINACION_PREDIO = $value->DENOMINACION_PREDIO;
            $TOT_BIEN_GRAL		 = number_format($value->TOT_BIEN_GRAL,0);
			$TOT_BIEN_ACTIVO	 = number_format($value->TOT_BIEN_ACTIVO,0);
			$TOT_BIEN_BAJA		 = number_format($value->TOT_BIEN_BAJA,0);
			
			$contador++;
            $linea = 0;
            
			if($contador == 21){
				$pdf->Cell(array_sum($w),0,'','T');
				$pdf->AddPage();
				$linea = 0;
			}else{
				$linea++;
			}
						
			if($linea!= 0 && $linea%58==0 ){
				$pdf->Cell(array_sum($w),0,'','T');
				$pdf->AddPage();
			 }
	
			$pdf->Cell($w[0],4,$contador,'LR',0,'L');
			$pdf->Cell($w[1],4,utf8_decode($DENOMINACION_PREDIO),'LR',0,'L');
			$pdf->Cell($w[2],4,$TOT_BIEN_GRAL,'LR',0,'R');
			$pdf->Cell($w[3],4,$TOT_BIEN_ACTIVO	,'LR',0,'R');
			$pdf->Cell($w[4],4,$TOT_BIEN_BAJA,'LR',0,'R');
			$pdf->Ln();
			$fill = !$fill;
        }

     
		// Línea de cierre
		$pdf->Cell(array_sum($w),0,'','T');
		
		$pdf->Ln();

        $pdf->Output();
        exit;
       
    }
}
