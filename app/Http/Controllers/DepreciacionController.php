<?php

namespace sinabipmuebles\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use DB;
use Config;
use funciones\funciones;
use Illuminate\Support\Facades\Storage;
use sinabipmuebles\Exports\CollectionExport;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use App\Helpers\JwtAuth;

class DepreciacionController extends Controller
{

    public function index()
    {
        return 'FUNCTION INDEX';
    }

  
    public function ListadoModificacionValorNeto(Request $request){ 
        // header("Access-Control-Allow-Origin: *");

        $reglas = [
            'id'        => 'int',
            'page'      => 'int',
            'records'   => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

        $id                 = $request->id;
        $cod_patrimonial    = $request->cod_patrimonial;
        $denominacion       = $request->denominacion;
		$page               = $request->page;
        $records            = $request->records;

        $data = DB::select(
            "exec PA_LISTA_MOD_VALOR_NETO ?,?,?,?,?",
            [$id,$cod_patrimonial,$denominacion,$page,$records]
        );
        return response()->success($data);
    }
       
    public function ActualizarDepreciacion(Request $request){ 
        // header("Access-Control-Allow-Origin: *");

        $reglas = [
            'id'                            => 'int',
            'cod_ue_bien_patri'             => 'int',
            'valordepreciacionEjerciciosi'  => 'numeric',
            'valordeprecacumuladosi'        => 'numeric',
            'valornetosi'                   => 'numeric',
            'porcentajedeprecsi'            => 'numeric'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
                ]);
        }

        $id                     = $request->id;
        $cod_ue_bien_patri      = $request->cod_ue_bien_patri;

        $valordepreciacionEjerciciosi   = $request->valordepreciacionEjerciciosi;   
        $valordeprecacumuladosi         = $request->valordeprecacumuladosi;
        $valornetosi                    = $request->valornetosi;
        $porcentajedeprecsi             = $request->porcentajedeprecsi;
        $cuentaContable                 = $request->cuentaContable;

        $data = DB::select(
            "exec PA_MOD_VALOR_NETO ?,?,?,?,?,?,?",[$id,$cod_ue_bien_patri,$valordepreciacionEjerciciosi,$valordeprecacumuladosi,$valornetosi, $porcentajedeprecsi, $cuentaContable]
        );
        return response()->success($data);
    }

    public function ValidaFechaDepreciacion(Request $request){ 
        // header("Access-Control-Allow-Origin: *");

        $reglas = [
            'id'                => 'int',
            'cod_ue_bien_patri' => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

        $id                     = $request->id;
        $cod_ue_bien_patri      = $request->cod_ue_bien_patri;

        $data = DB::select(
            "exec PA_VALIDA_FECHA ?,?",[$id,$cod_ue_bien_patri]
        );
        return response()->success($data);
    }

    public function ValidaDepreciacionestado(Request $request){ 
        // header("Access-Control-Allow-Origin: *");
        $hash = $request->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checktoken = $jwtAuth->checkToken($hash);
        if ($checktoken) {
            $id = $request->id;
            $data = DB::select(
                "exec PA_VALIDA_DEPRECIACION_ESTADOS ?",[$id]
            );
        }else{
            $data = array(
                'status' => 'error',
                'message' => 'No estas autorizado para acceder a esta ruta'
            );
        }
        return response()->success($data);
    }

    public function ListaFechaDepreciacionSelec(Request $request){ 
        // header("Access-Control-Allow-Origin: *");
        $data = DB::select(
            "SELECT DESCRIPCION_ESTADO, ESTADO_PARAMETRO FROM TBL_PARAMETRIZACION_TL WHERE COD_PARAMETRO_CAB='COMB_FECHA_DEPREC' AND ESTADO='1'"
        );
        return response()->success($data);
    }

    public function GuardarTipoDepreciacion(Request $request){ 
        // header("Access-Control-Allow-Origin: *");
        $id         = $request->id;
        $tipo       = $request->tipo;
        $cod_user   = $request->cod_user;

        $data = DB::select(
            "exec PA_GUARDAR_TIPO_FECHA_DEPRECIACION ?,?,?",[$id,$tipo,$cod_user]
        );
        return response()->success($data);
    }

    public function ListaFechaPecosaActualiza(Request $request){ 
        // header("Access-Control-Allow-Origin: *");
        $hash = $request->header('Authorization', null);
        $jwtAuth = new JwtAuth();
		$checktoken = $jwtAuth->checkToken($hash);		
        if ($checktoken) {
            $reglas = [
                'id'         => 'int',
                'page'       => 'int',
                'records'    => 'int'
            ];
            $validator = Validator::make($request->all(), $reglas);
            if ($validator->fails()){
                return response()->success([
                    'error' => true,
                    'reco' => $validator->errors()
                ]);
            }
            $id                 = $request->id;
            $cod_patrimonial    = $request->cod_patrimonial;
            $denominacion       = $request->denominacion;
            $page               = $request->page;
            $records            = $request->records;
            $data = DB::select(
                "exec PA_LISTA_ACTUALIZA_PECOSA ?,?,?,?,?",
                [$id,$cod_patrimonial,$denominacion,$page,$records]
            );
            return response()->success($data);
        }else{
			return response()->success([
				'error' => true,
				'reco' => array(
					"id"=>'No estas autorizado para acceder a esta ruta'
				)
			]);	
		}
        
    }


    public function AdjuntarDepreciaciontxt(Request $request, $data){
        //$data = explode('-',$data);
       //header("Access-Control-Allow-Origin: *");
       $id_entidad	= $data[0];
       $tipo_acto = 5;
       $nomb_archivo = '';
       $usua_creacion = 100; 
       
       //$cod_alta = $cod_alta[0]->CODIGO.'_'; 
       $id_adjuntado =  DB::select( "SELECT TOP 1 ID_ADJUNTADOS_ACTOS_TL FROM TBL_ADJUNTADOS_ACTOS_TL ORDER BY ID_ADJUNTADOS_ACTOS_TL DESC" );
       
       if( empty($id_adjuntado) ){
           $id_adjuntado = 1;
       }else{
           $id_adjuntado = $id_adjuntado[0]->ID_ADJUNTADOS_ACTOS_TL;
           $id_adjuntado++  ;
       }

       if ($request->hasFile('file0')) {  
           $image = $request->file('file0');
           $ARCHIVO_EXTENSION = 'txt'; 
           $NUEVA_CADENA   = substr( md5(microtime()), 1, 4);
           $FECHA_ARCHIVO  = date('Y').''.date('m').''.date('d'); 
           $name = "depreciacion/";
           
           //dd($ARCHIVO_NOMBRE_GENERADO);
           $result = DB::select(
            "exec PA_ADJUNTADO_ACTOS_MASIVO_SW ?,?,?,?",[ 
                $id_entidad,$tipo_acto,"",$usua_creacion
                ]
            );
           
           $id_adjuntado = $result[0]->ID_ADJUNTADO; 
           
           
           $ARCHIVO_NOMBRE_GENERADO ='TXT_'. strval($id_adjuntado).'Depreciacion_Entidad_'.$id_entidad.'_Serie_'.$FECHA_ARCHIVO.'_'.$NUEVA_CADENA.'.'.$ARCHIVO_EXTENSION;
           //$url = Storage::disk('public')->putFileAs($name, $image,$ARCHIVO_NOMBRE_GENERADO); 
            //    file_put_contents("//SRV-PORTALSBN/Inventarios_zip$/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
           if(Config::get('app.APP_LINUX')){
            \Storage::disk('local')->put("public/".$name.$ARCHIVO_NOMBRE_GENERADO, file_get_contents($image));
            // file_put_contents(storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
            $origen = storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO;
            $final = '/mnt/Inventarios_zip/'.$name.$ARCHIVO_NOMBRE_GENERADO;
            shell_exec('mv '.$origen.' '.$final);
            }else{
                file_put_contents("//SRV-PORTALSBN/Inventarios_zip$/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
            }
           $Peso_Arch = filesize($image);

           
           $result2 = DB::select(
               "exec PA_ADJUNTADO_ACTOS_ADQUISICION_UPD_NOMBRE_ARCHIVO_SW ?,?",[  
                   $id_adjuntado,$ARCHIVO_NOMBRE_GENERADO
                   ]
           );
           

           //return response()->json($url);
           return response()->success($result2[0]);

       }
   }



   public function CargarDepreciacionTXT(Request $request){

    $reglas = [
        'id_entidad'    => 'int',
        'id_adjuntado'  => 'int'
    ];
    $validator = Validator::make($request->all(), $reglas);
    if ($validator->fails()){
        return response()->success([
            'error' => true,
            'reco' => $validator->errors()
            ]);
    }

    $id_entidad     = $request->id_entidad;
    $id_adjuntado   = $request->id_adjuntado;
    $nombreArchivo  = $request->nombreArchivo;
    $usua_creacion  = $request->usua_creacion; 

    $dataCarga = DB::select( "exec PA_CARGA_MASIVA_ACTUALIZACION_DEPRECIACION_SW ?, ?, ?, ?",[
        $id_entidad,
        $id_adjuntado,
        $nombreArchivo,
        $usua_creacion
    ]);
    
    //return response()->success(true);
    return response()->success($dataCarga[0]);

    }

    public function ValidarDepreciacionTXT(Request $request){

        $reglas = [
            'id_entidad'    => 'int',
            'id_adjuntado'  => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
                ]);
        }

        $id_entidad     = $request->id_entidad;
        $id_adjuntado   = $request->id_adjuntado;
        $nombreArchivo  = $request->nombreArchivo;
        $usua_creacion  = $request->usua_creacion;

        $dataValidacion = DB::select( "exec PA_VALIDACION_MASIVA_ACTUALIZACION_DEPRECIACION_SW ?, ?, ?",[
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);
        
        //return response()->success(true);
        return response()->success($dataValidacion[0]);

    }

    public function FinalizarDepreciacionTXT(Request $request){

        $reglas = [
            'id_entidad'    => 'int',
            'id_adjuntado'  => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas); 
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
                ]);
        }

        $id_entidad     = $request->id_entidad;
        $id_adjuntado   = $request->id_adjuntado;
        $nombreArchivo  = $request->nombreArchivo;
        $usua_creacion  = $request->usua_creacion;

        $dataFinalizacion = DB::select( "exec PA_FINALIZACION_MASIVA_ACTUALIZACION_DEPRECIACION_SW ?, ?, ?",[
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);
        
        //return response()->success(true);
        return response()->success($dataFinalizacion[0]);

    }

    public function ListadoErroresCargaMasivaDepreciacion(Request $request){

        $id_entidad     = $request->id_entidad;
        $id_adjuntado    = $request->id_adjuntado;
        $usua_creacion  = $request->usua_creacion;


        $accion         = 2;
        $dataObservaciones_2 = DB::select( "exec PA_OBSERVACIONES_VALIDACION_MASIVA_DEPRECIACION_SW ?, ?, ?, ?",[
            $accion,
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);

        $accion         = 3;
        $dataObservaciones_3 = DB::select( "exec PA_OBSERVACIONES_VALIDACION_MASIVA_DEPRECIACION_SW ?, ?, ?, ?",[
            $accion,
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);

        
        return response()->success([
            "errores_columnas"  => (count($dataObservaciones_2) > 0) ?$dataObservaciones_2 : [],
            "no_existe_codigos"  => (count($dataObservaciones_3) > 0) ?$dataObservaciones_3 : []
        ]);

    }


    public function downloadExcel(Request $request)
    {
        //$data = Item::get()->toArray();

        ini_set('memory_limit', '-1');
        $id       = $request->id;
        $data     = $request->data;
        $data     = base64_decode($data);

        $data_det = DB::select(
            "exec SP_WAS ?",
            [$data]
        )->toArray();
            
        return Excel::create('itsolutionstuff_example', function($excel) use ($data_det) {
            $excel->sheet('mySheet', function($sheet) use ($data_det)
            {
                $sheet->fromArray($data_det);
            });
        })->download('xlsx');
    }
    

    public function ImportarFormatoExcel(Request $request){

        $id_entidad = $request->id_entidad;

        $dataGrupoDescargas = DB::select("exec SP_GRUPO_DESCARGA_ACTUALIZACION_DEPRECIACION ?", [
            $id_entidad
        ]);
 
        return response()->success([
            "GrupoDescargas" => (count($dataGrupoDescargas) > 0) ?$dataGrupoDescargas : []
        ]);
    }

    public function ImportarFormatoExcelxGrupo(Request $request, $id, $grupo){
        // header("Access-Control-Allow-Origin: *");
        // header('Content-type: application/xls');

        ini_set('memory_limit', '256M');

        // $id       = $request->id;
        // $grupo      = $request->grupo;
        

        $data_det = DB::select("exec SP_IMPORTAR_EXCEL_POR_GRUPO  ?,?", [
            $id,
            $grupo

        ]);

        // dd($grupo);
        //return;

        $arrayTrans = [];
        
        $arrayTrans[] = [
            'ITEM',
            'CODIGO PATRIMONIAL',
            'VALOR_DEPREC_EJERCICIO',
            'DEPRECIACION_ACUMULADA',
            'VALOR_NETO',
            'PORCENTAJE_DEPRECIACION',
            'CUENTA_CONTABLE'
        ];
        foreach ($data_det as $key => $value) {
            $arrayTrans[] = [
                $value->ROW_NUMBER_ID,
                // strval('=""&"'.$value->CODIGO_PATRIMONIAL.'"'),
                // strval('="'.$value->CODIGO_PATRIMONIAL.'"'),
                $value->CODIGO_PATRIMONIAL,
                $value->VALOR_DEPREC_EJERCICIO,
                $value->DEPRECIACION_ACUMULADA,
                $value->VALOR_NETO,
                $value->PORC_DEPREC,
                $value->NRO_CTA_CONTABLE
            ];
        }

        
        $collection = new CollectionExport();
        $collection->load($arrayTrans);
        return Excel::download($collection, 'entidad' . $id . '_info_bienes_archivoNro_' . $grupo.'.xlsx');
    }



    public function Listar_Bienes_TXT(Request $request,$id, $grupo){
        //$procedure=New OracleProcedure();
        //$data = ['p_codigo_sem' => [$id,'INTEGER']];
        //$semestres=$procedure->executeCursor(config('database.connections.oracle.userdefault'). '.PV_INGRESANTE.pa_ingresantesSem',$data,'c_ingresantes');
        $string = "";
        $nombresem = 'descarga';

        $data_det = DB::select("exec SP_DESCARGA_BIENES_TXT  ?,?", [
            $id,
            $grupo

        ]);


        header('Content-type: text/plain');

        $string.= 'NRO'.'|'.'CODIGO_PATRIMONIAL'.'|'.'DENOMINACION_BIEN'.'|'.'NRO_CTA_CONTABLE'.'|'.'NOM_CTA_CONTABLE'.'|'.'NRO_DOCUMENTO_ADQUIS'.'|'.'FECHA_DOCUMENTO_ADQUIS'.'|'.'VALOR_ADQUIS'.'|'.'VALOR_NETO'.'|'.'NOM_EST_BIEN'.'|'.'OPC_ASEGURADO'.'|'.'CONDICION'.'|'.'NRO_RESOLUCION_BAJA'.'|'.'FECHA_RESOLUCION_BAJA'.'|'.'NRO_RESOLUCION_DISPOSIC'.'|'.'FECHA_RESOLUCION_DISPOSIC'.'|'.'NRO_RESOLUCION_ADMINIS'.'|'.'FECHA_RESOLUCION_ADMINIS'.'|'.'TIPO_MOV'.'|'.'DENOMINACION_PREDIO'.'|'.'DESC_AREA'.'|'.'NRO_DOCUMENTO'.'|'.'UE_NOMBRES_PERS'.'|'.'UE_APEPAT_PERS'.'|'.'UE_APEMAT_PERS'.'|'.'MARCA'.'|'.'MODELO'.'|'.'TIPO'.'|'.'COLOR'.'|'.'SERIE'.'|'.'DIMENSION'.'|'.'PLACA'.'|'.'NRO_MOTOR'.'|'.'NRO_CHASIS'.'|'.'MATRICULA'.'|'.'ANIO_FABRICACION'.'|'.'RAZA'.'|'.'ESPECIE'.'|'.'EDAD'.'|'.'PAIS'.'|'.'OTRAS_CARACT'.'|'.'VALOR_DEPREC_ACUM_AL_2018'.'|'.'VALOR_NETO_AL_2018'."\r\n";
        

        foreach ($data_det as $key => $value) {
            // print_r($value);
            // die();
            
            $string.= $value->ROW_NUMBER_ID.'|'.$value->CODIGO_PATRIMONIAL.'|'.$value->DENOMINACION_BIEN.'|'.$value->NRO_CTA_CONTABLE.'|'.$value->NOM_CTA_CONTABLE.'|'.$value->NRO_DOCUMENTO_ADQUIS.'|'.$value->FECHA_DOCUMENTO_ADQUIS.'|'.$value->VALOR_ADQUIS.'|'.$value->VALOR_NETO.'|'.$value->NOM_EST_BIEN.'|'.$value->OPC_ASEGURADO.'|'.$value->CONDICION.'|'.$value->NRO_RESOLUCION_BAJA.'|'.$value->FECHA_RESOLUCION_BAJA.'|'.$value->NRO_RESOLUCION_DISPOSIC.'|'.$value->FECHA_RESOLUCION_DISPOSIC.'|'.$value->NRO_RESOLUCION_ADMINIS.'|'.$value->FECHA_RESOLUCION_ADMINIS.'|'.$value->TIPO_MOV.'|'.$value->DENOMINACION_PREDIO.'|'.$value->DESC_AREA.'|'.$value->NRO_DOCUMENTO.'|'.$value->UE_NOMBRES_PERS.'|'.$value->UE_APEPAT_PERS.'|'.$value->UE_APEMAT_PERS.'|'.$value->MARCA.'|'.$value->MODELO.'|'.$value->TIPO.'|'.$value->COLOR.'|'.$value->SERIE.'|'.$value->DIMENSION.'|'.$value->PLACA.'|'.$value->NRO_MOTOR.'|'.$value->NRO_CHASIS.'|'.$value->MATRICULA.'|'.$value->ANIO_FABRICACION.'|'.$value->RAZA.'|'.$value->ESPECIE.'|'.$value->EDAD.'|'.$value->PAIS.'|'.$value->OTRAS_CARACT.'|'.$value->VALOR_DEPREC_ACUM_AL_2018.'|'.$value->VALOR_NETO_AL_2018."\r\n";

        }
        
        return response($string)
        ->withHeaders([
            'Content-Type' => 'text/plain',
            'Content-Disposition' => 'attachment; filename="ing'.$nombresem.'.txt',
        ]);
    }


    

}
