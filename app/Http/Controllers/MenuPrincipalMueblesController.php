<?php

namespace sinabipmuebles\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use DB;
use App\Helpers\JwtAuth;

class MenuPrincipalMueblesController extends Controller
{
    

	public function index()
    {
        return 'FUNCTION INDEX';
    }

    
    public function ListadoMenuPrincipalMuebles(Request $request,$id,$token) 
    {
        $jwtAuth = new JwtAuth();
		$checktoken = $jwtAuth->checkToken($token);
        if ($checktoken) {
            $data1 = DB::select(
                "exec PA_LISTADO_SUBMENU_MUEBLES ?",[$id]
            );
            $data = json_decode(json_encode($data1),true);
            $menu = [];
            foreach($data as $rows){
                $menu[$rows['COD_MUEBLE_MENU']] = [ 
                    'estado' => $rows['COLUMNA'],
                    'descripcion' => $rows['DESC_MUEBLE_MENU'],
                    'hijos' => []
                ];
                
            }
            foreach($data as $rows){
                $menu[$rows['COD_MUEBLE_MENU']]['hijos'][$rows['COD_MUEBLE_MENUSUB']] = [ 
                'descripcion' => $rows['DESC_MUEBLE_MENUSUB'],
                'nombre_carpeta' => $rows['NOMBRE_CARPETA']
                ];
            
            }
            return response()->success($menu);
        }else{
            $data = array(
                'status' => false,
                'message' => 'No estas autorizado para acceder a esta ruta'
            );
        }
        //echo json_encode($menu);
        //return response()->success($data1);
	    
    }        

    
    public function validaTokenAuth(Request $request,$token){ 
		// header("Access-Control-Allow-Origin: *");
        // $hash = $request->header('Authorization', null);
        
        $jwtAuth = new JwtAuth();
		$checktoken = $jwtAuth->checkToken($token);
        if ($checktoken) {
			$data = array(
				'status' => true,
				'message' => 'Validado correctamente'
			);
		}else{
			$data = array(
				'status' => false,
				'message' => 'No estas autorizado para acceder a esta ruta'
			);
		}
		
        return response()->success($data);
    }

}
