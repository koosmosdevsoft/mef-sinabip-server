<?php

namespace sinabipmuebles\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use DB;
use Config;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Helpers\JwtAuth;

class ActosAdquisicionController extends Controller
{
    
	public function index()
    {
        return 'FUNCTION INDEX';
    }

    
    public function ListadoActosAdquisicion(Request $request) 
    {
        $hash = $request->header('Authorization', null);
        $jwtAuth = new JwtAuth();
		$checktoken = $jwtAuth->checkToken($hash);		
        if ($checktoken) {
            $reglas = [
                'cod_entidad'   => 'int',
                'nro_documento' => 'max:20',
                'page'          => 'int',
                'records'       => 'int'
            ];
            $validator = Validator::make($request->all(), $reglas);
            if ($validator->fails()){
                return response()->success([
                    'error' => true,
                    'reco' => $validator->errors()
                ]);
            }

            $cod_entidad        = $request->cod_entidad;
            $forma_adquisicion  = $request->forma_adquisicion;
            $nro_documento      = $request->nro_documento;
            $estado             = $request->estado;
            $fecha_year         = $request->fecha['year'];
            $fecha_month        = $request->fecha['month'];
            $page               = $request->page;
            $records            = $request->records;
            
            $dataFormaAdq = DB::select('exec PA_LISTADO_FORMAS_ADQUISICION'); 

            $dataAnios = DB::select('exec PA_LISTADO_ANIOS');
            $dataMes = DB::select('exec PA_LISTADO_MESES');

            $data = DB::select('exec PA_LISTADO_ACTOS_ADQUISICION ?,?,?,?,?,?,?,?', [
                $cod_entidad,
                $forma_adquisicion,
                $nro_documento,
                $fecha_year,
                $fecha_month,
                $estado,
                $page,
                $records

            ]);
        
            return response()->success([
                "documento" => (count($data) > 0) ?$data : [],
                "formaAdq"  => (count($dataFormaAdq) > 0) ?$dataFormaAdq : [],
                "anios"  => (count($dataAnios) > 0) ?$dataAnios : [],
                "mes"  => (count($dataMes) > 0) ?$dataMes : []
            ]);
        }else{
			return response()->success([
				'error' => true,
				'reco' => array(
					"id"=>'No estas autorizado para acceder a esta ruta'
				)
			]);	
		}
    }

    public function ListadoCatalogoFormaIndividual(Request $request) 
    {
        $hash = $request->header('Authorization', null);
        $jwtAuth = new JwtAuth();
		$checktoken = $jwtAuth->checkToken($hash);		
        if ($checktoken) {
            $cod_entidad= $request->cod_entidad;
            $nro_grupo  = $request->nro_grupo;
            $nro_clase  = $request->nro_clase;
            $denom_bien = $request->denom_bien;
            $nro_bien   = $request->nro_bien;
            $cod_patrimonial= $request->cod_patrimonial;
            $denom_patrimonial= $request->denom_patrimonial;
            $page               = $request->page;
            $records            = $request->records;

            //dd('aaa');
            $dataLstGrupos = DB::select('exec PA_LISTADO_GRUPOS');
            $dataLstClases = DB::select('exec PA_LISTADO_CLASES ?',[ 
                $nro_grupo
            ]);
            $dataLstCatalogo = DB::select('exec PA_LISTADO_CATALOGO_WAS ?,?,?,?,?,?',[ 
                $nro_grupo, 
                $nro_clase,
                $denom_bien,
                $nro_bien,
                $page,
                $records
            ]);
            $dataLstBienesEliminados = DB::select('exec PA_LISTADO_BIENES_ELIMINADOS ?,?,?', [
                $cod_entidad,
                $cod_patrimonial,
                $denom_patrimonial
            ]);

            return response()->success([
                "grupos" => (count($dataLstGrupos) > 0) ?$dataLstGrupos : [],
                "clases"  => (count($dataLstClases) > 0) ?$dataLstClases : [],
                "catalogo"  => (count($dataLstCatalogo) > 0) ?$dataLstCatalogo : [],
                "bienesEliminados"  => (count($dataLstBienesEliminados) > 0) ?$dataLstBienesEliminados : []
            ]);
        }else{
			return response()->success([
				'error' => true,
				'reco' => array(
					"id"=>'No estas autorizado para acceder a esta ruta'
				)
			]);	
		}
    }

    public function ListadoBienesEliminados(Request $request)
    {
        $hash = $request->header('Authorization', null);
        $jwtAuth = new JwtAuth();
		$checktoken = $jwtAuth->checkToken($hash);		
        if ($checktoken){
            $cod_entidad= $request->cod_entidad;
            $cod_patrimonial= $request->cod_patrimonial;
            $denom_patrimonial= $request->denom_patrimonial;
    
            $dataLstBienesEliminados = DB::select('exec PA_LISTADO_BIENES_ELIMINADOS ?,?,?', [
                $cod_entidad,
                $cod_patrimonial,
                $denom_patrimonial
            ]);
    
            return response()->success([
                "bienesEliminados" => (count($dataLstBienesEliminados) > 0) ?$dataLstBienesEliminados : []
            ]);
        }else{
			return response()->success([
				'error' => true,
				'reco' => array(
					"id"=>'No estas autorizado para acceder a esta ruta'
				)
			]);	
		}
    }


    public function Datos_Caracteristicas_Bien(Request $request){

        $id_bien = $request->id_bien;

        $dataLstCuentasContables = DB::select("exec PA_LISTADO_CUENTAS_CONTABLES");
        $dataLstEstadoBien = DB::select("exec PA_LISTADO_ESTADO_BIEN");
        $dataCaracteristicasBien = DB::select("exec PA_CARACTERISTICAS_BIEN ?", [
            $id_bien
        ]);
 
        return response()->success([
            "CuentasContables" => (count($dataLstCuentasContables) > 0) ?$dataLstCuentasContables : [],
            "EstadosBien" => (count($dataLstEstadoBien) > 0) ?$dataLstEstadoBien : [],
            "CaracteristicasBien" => (count($dataCaracteristicasBien) > 0) ?$dataCaracteristicasBien : [],
        ]);
        
    }

    // public function SubirZip(Request $request,$data){
	// 	// header("Access-Control-Allow-Origin: *");
	// 	$data = explode('-',$data);
	// 	$id	= $data[0];
	// 	$idpredio = $data[1];
	// 	$idusuario = $data[2];

    //     if ($request->hasFile('file0')) {
    //         $image = $request->file('file0');
	// 		$name = "Sin_Procesar/";
	// 		$ARCHIVO_EXTENSION = '.TXT';
			
	// 		$ARCHIVO_NOMBRE_GENERADO = explode('.zip',$image->getClientOriginalName());
	// 		$ARCHIVO_NOMBRE = $ARCHIVO_NOMBRE_GENERADO[0].$ARCHIVO_EXTENSION;
			
	// 		$url = Storage::disk('public')->putFileAs($name, $image,$image->getClientOriginalName());
	// 		$Peso_Arch = 0;//Storage::size(storage_path('app/public')."/".$name.$image->getClientOriginalName());
	// 		$result = DB::statement(
    //             "exec PA_ADJUNTAR_ARCHIVO ?,?,?,?,?",[
	// 				$id,$idpredio,$ARCHIVO_NOMBRE,$Peso_Arch,$idusuario
	// 				]
    //         );
    //         return response()->json($result);
    //     }
	// }

    public function Guardar_Caracteristicas_Bien(Request $request){

        $id_entidad = $request->id_entidad;
        $id_bien = $request->id_bien;
        $denomBien = $request->denomBien;
        $marca = $request->marca;
        $modelo = $request->modelo;
        $tipo = $request->tipo;
        $color = $request->color;
        $nroSerie = $request->nroSerie;
        $dimension = $request->dimension;
        $placa_matri = $request->placa_matri;
        $nroMotor = $request->nroMotor;
        $nroChasis = $request->nroChasis;
        $anioFabric = $request->anioFabric;
        $raza = $request->raza;
        $especie = $request->especie;
        $edad = $request->edad;
        $otrasCaract = $request->otrasCaract;
        $usoCta = $request->usoCta;
        $TipoCta = $request->TipoCta;
        $CtaContable = $request->CtaContable;
        $valorAdquis = $request->valorAdquis;
        $porcDeprec = $request->porcDeprec;
        $asegurado = $request->asegurado;
        $estadoBien = $request->estadoBien;
        $observacion = $request->observacion;

        $data = DB::select("exec PA_GUARDAR_CARACTERISTICAS_BIEN ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?",[ 
            $id_entidad,
            $id_bien,
            $denomBien, 
            $marca,
            $modelo,
            $tipo,
            $color,
            $nroSerie,
            $dimension,
            $placa_matri,
            $nroMotor,
            $nroChasis,
            $anioFabric,
            $raza,
            $especie,
            $edad,
            $otrasCaract,
            $usoCta,
            $TipoCta,
            $CtaContable,
            $valorAdquis,
            $porcDeprec,
            $asegurado,
            $estadoBien,
            $observacion
        ]);
        return response()->success($data);

    }

    public function BqserieExiste(Request $request){
        $id_entidad     = $request->id_entidad;
        $nroSerie   = $request->nroSerie;
        $cod_bien   = $request->cod_bien;
        
    
        $dataCarga = DB::select( "exec PA_BUSQUERA_SERIE_EXISTENTE ?, ?, ?",[
            $id_entidad,
            $nroSerie,
            $cod_bien 
        ]);
        
        //return response()->success(true);
        return response()->success($dataCarga[0]);
    
        }

    public function RegistrarActosAdquisicion_Captura_Errores(Request $request)
    {

        $hash = $request->header('Authorization', null);
        $jwtAuth = new JwtAuth();
		$checktoken = $jwtAuth->checkToken($hash);		
        if ($checktoken) {
            $ID_ALUMNOS     = $request->ID_ALUMNOS;
            $NOMBRE         = $request->NOMBRE;
            $DIRECCION      = $request->DIRECCION;
            DB::beginTransaction();
            try {
                DB::select("exec PA_REGISTRAR_ALUMNOS ?,?,?", [$ID_ALUMNOS, $NOMBRE, $DIRECCION]);
                DB::select('exec PA_REGISTRAR_NOTAS');
                DB::commit();
            } catch (\Illuminate\database\QueryException $e) {
                DB::rollBack();
                return response()->error($e->getMessage());
            }
            return response()->success(true);
        }else{
			return response()->success([
				'error' => true,
				'reco' => array(
					"id"=>'No estas autorizado para acceder a esta ruta'
				)
			]);	
		}
        
    }


    public function EditarActoAdquisicion(Request $request){

        $reglas = [
            'id_entidad'  => 'int',
            'id_acto'     => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
                ]);
        }

        $id_entidad = $request->id_entidad;
        $id_acto = $request->id_acto;

        $dataCabActo = DB::select( "exec PA_EDITAR_ACTOS_ADQUISICION_CAB ?, ?",[$id_entidad,$id_acto]);
        $dataDetActo = DB::select( "exec PA_EDITAR_ACTOS_ADQUISICION_DET ?, ?",[$id_entidad,$id_acto]);
        //dd($id_entidad);
        return response()->success([
            'error' => false,
            "cabecera" => (count($dataCabActo) > 0) ?$dataCabActo[0] : [],
            "detalles"  => (count($dataDetActo) > 0) ?$dataDetActo : []
        ]);
    }

    public function AgregarBien_Actos(Request $request){

        $TipoagregarBien    = $request->TipoagregarBien;
        $id_acto            = $request->id_acto;
        $id_entidad         = $request->id_entidad;
        $id_forma           = $request->id_forma;
        $nro_documento      = $request->nro_documento;
        $fecha_documento    = $request->fecha_documento;
        $usua_creacion      = $request->usua_creacion; 
        $codigo_patrimonial = $request->codigo_patrimonial;
        $denominacion_bien  = $request->denominacion_bien;
        $cantidad           = $request->cantidad;
        $valor_adq          = $request->valor_adq;

        $dataCabActo = DB::select( "exec PA_REGISTRAR_BIENES_ACTOS_ADQUISICION ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?",[
            $TipoagregarBien,
            $id_acto, 
            $id_entidad,
            $id_forma,
            $nro_documento,
            $fecha_documento,
            $usua_creacion,
            $codigo_patrimonial,
            $denominacion_bien,
            $cantidad,
            $valor_adq
        ]);
        //dd($id_entidad);
        return response()->success([
            "cabecera" => (count($dataCabActo) > 0) ?$dataCabActo[0] : []
        ]);
        return response()->success(true);
    }

    public function Eliminar_Actos(Request $request){

        $reglas = [
            'id_entidad'  => 'int',
            'id_acto'     => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

        $id_entidad         = $request->id_entidad;
        $id_acto            = $request->id_acto;

        $data =  DB::select("exec PA_ELIMINAR_ACTOS_ADQUISICION ?,?",[ 
            $id_entidad,
            $id_acto
        ]);
        return response()->success($data);
    }

    public function Guardar_Actos(Request $request){

        $reglas = [
            'id_acto'           => 'int',
            'id_entidad'        => 'int',
            'id_forma'          => 'int',
            'nro_documento'     => 'max:20',
            'fecha_documento'   => 'max:10',
            'usua_creacion'     => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

        $id_acto            = $request->id_acto;
        $id_entidad         = $request->id_entidad;
        $id_forma           = $request->id_forma;
        $nro_documento      = $request->nro_documento;
        $fecha_documento    = $request->fecha_documento;
        $usua_creacion      = $request->usua_creacion; 

        $dataCabActo = DB::select( "exec PA_GUARDAR_ACTOS_ADQUISICION ?, ?, ?, ?, ?, ?",[
            $id_acto,
            $id_entidad,
            $id_forma,
            $nro_documento,
            $fecha_documento,
            $usua_creacion
        ]);
        //dd($id_entidad);
        return response()->success($dataCabActo[0]);

    }


    public function fecha_sql($fecha){
        date_default_timezone_set('America/Lima');
        $datetime_variable = new DateTime($fecha);
        $datetime_formatted = $datetime_variable->format('Y-m-d H:i:s');
       return  $datetime_formatted;  
    }


    public function AdjuntarAltatxt(Request $request, $data, $token){
         //$data = explode('-',$data);
        //header("Access-Control-Allow-Origin: *");
        // $hash = $request->header('Authorization', null);
        $jwtAuth = new JwtAuth();
		$checktoken = $jwtAuth->checkToken($token);		
        if ($checktoken) {
            $id_entidad	= $data[0];
            $tipo_acto = 1;
            $nomb_archivo = '';
            $usua_creacion = 100; 
            
            //$cod_alta = $cod_alta[0]->CODIGO.'_'; 
            $id_adjuntado =  DB::select( "SELECT TOP 1 ID_ADJUNTADOS_ACTOS_TL FROM TBL_ADJUNTADOS_ACTOS_TL ORDER BY ID_ADJUNTADOS_ACTOS_TL DESC" );
            
            if( empty($id_adjuntado) ){
                $id_adjuntado = 1;
            }else{
                $id_adjuntado = $id_adjuntado[0]->ID_ADJUNTADOS_ACTOS_TL;
                $id_adjuntado++  ;
            }
    
            if ($request->hasFile('file0')) {  
                $image = $request->file('file0');
                $ARCHIVO_EXTENSION = 'txt'; 
                $NUEVA_CADENA   = substr( md5(microtime()), 1, 4);
                $FECHA_ARCHIVO  = date('Y').''.date('m').''.date('d'); 
                $name = "altas/";
                
                //dd($ARCHIVO_NOMBRE_GENERADO);
                $result = DB::select(
                    "exec PA_ADJUNTADO_ACTOS_ADQUISICION_SW ?,?,?,?",[ 
                        $id_entidad,1,"",$usua_creacion
                        ]
                );
                
                $id_adjuntado = $result[0]->ID_ADJUNTADO; 
                
                
                $ARCHIVO_NOMBRE_GENERADO ='TXT_'. strval($id_adjuntado).'Altas_Entidad_'.$id_entidad.'_Serie_'.$FECHA_ARCHIVO.'_'.$NUEVA_CADENA.'.'.$ARCHIVO_EXTENSION;
                //$url = Storage::disk('public')->putFileAs($name, $image,$ARCHIVO_NOMBRE_GENERADO); 
                // file_put_contents("//SRV-PORTALSBN/Inventarios_zip$/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
                if(Config::get('app.APP_LINUX')){
                    // file_put_contents(storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
                    \Storage::disk('local')->put("public/".$name.$ARCHIVO_NOMBRE_GENERADO, file_get_contents($image));
                    $origen = storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO;
                    $final = '/mnt/Inventarios_zip/'.$name.$ARCHIVO_NOMBRE_GENERADO;
                    shell_exec('mv '.$origen.' '.$final);
                }else{
                    file_put_contents("//SRV-PORTALSBN/Inventarios_zip$/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
                }
                $Peso_Arch = filesize($image);
    
                
                $result2 = DB::select(
                    "exec PA_ADJUNTADO_ACTOS_ADQUISICION_UPD_NOMBRE_ARCHIVO_SW ?,?",[  
                        $id_adjuntado,$ARCHIVO_NOMBRE_GENERADO
                        ]
                );
                
    
                //return response()->json($url);
                return response()->success($result2[0]);
    
            }
        }else{
			return response()->success([
				'error' => true,
				'reco' => array(
					"id"=>'No estas autorizado para acceder a esta ruta'
				)
			]);	
		}
    }
    
    public function CargarTXT(Request $request){

        $reglas = [
            'id_entidad'      => 'int',
            'id_adjuntado'    => 'int',
            'usua_creacion'   => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

        $id_entidad     = $request->id_entidad;
        $id_adjuntado   = $request->id_adjuntado;
        $nombreArchivo  = $request->nombreArchivo;
        $usua_creacion  = $request->usua_creacion;

        $dataCarga = DB::select( "exec PA_CARGA_MASIVA_ACTOS_ADQUISICION_SW ?, ?, ?, ?",[
            $id_entidad,
            $id_adjuntado,
            $nombreArchivo,
            $usua_creacion
        ]);
        
        //return response()->success(true);
        return response()->success($dataCarga[0]);

    }

    public function ValidarTXT(Request $request){

        $reglas = [
            'id_entidad'    => 'int',
            'id_adjuntado'  => 'int',
            'usua_creacion' => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

        $id_entidad     = $request->id_entidad;
        $id_adjuntado   = $request->id_adjuntado;
        $nombreArchivo  = $request->nombreArchivo;
        $usua_creacion  = $request->usua_creacion;

        $dataValidacion = DB::select( "exec PA_VALIDACION_MASIVA_ACTOS_ADQUISICION_SW ?, ?, ?",[
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);
        
        //return response()->success(true);
        return response()->success($dataValidacion[0]);

    }

    public function FinalizarTXT(Request $request){

        $reglas = [
            'id_entidad'    => 'int',
            'id_adjuntado'  => 'int',
            'usua_creacion' => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }
        
        $id_entidad     = $request->id_entidad;
        $id_adjuntado   = $request->id_adjuntado;
        $nombreArchivo  = $request->nombreArchivo;
        $usua_creacion  = $request->usua_creacion;

        $dataFinalizacion = DB::select( "exec PA_FINALIZACION_MASIVA_ACTOS_ADQUISICION_SW ?, ?, ?",[
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);
        
        //return response()->success(true);
        return response()->success($dataFinalizacion[0]);

    }


    public function ListadoErroresCargaMasiva(Request $request){

        $accion         = 1;
        $id_entidad     = $request->id_entidad;
        $id_adjuntado    = $request->id_adjuntado;
        $usua_creacion  = $request->usua_creacion;

        $dataObservaciones_1 = DB::select( "exec PA_OBSERVACIONES_VALIDACION_MASIVA_ACTOS_ADQUISICION_SW ?, ?, ?, ?",[
            $accion,
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);

        $accion         = 2;
        $dataObservaciones_2 = DB::select( "exec PA_OBSERVACIONES_VALIDACION_MASIVA_ACTOS_ADQUISICION_SW ?, ?, ?, ?",[
            $accion,
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);

        $accion         = 3;
        $dataObservaciones_3 = DB::select( "exec PA_OBSERVACIONES_VALIDACION_MASIVA_ACTOS_ADQUISICION_SW ?, ?, ?, ?",[
            $accion,
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);
        
        return response()->success([
            "duplicado_bienes" => (count($dataObservaciones_1) > 0) ?$dataObservaciones_1 : [], 
            "duplicado_actos"  => (count($dataObservaciones_2) > 0) ?$dataObservaciones_2 : [],
            "errores_columnas"  => (count($dataObservaciones_3) > 0) ?$dataObservaciones_3 : []
        ]);

    }

    public function Eliminar_Bien_Proceso(request $request){

        $id_entidad = $request->id_entidad;
        $id_usuario = $request->id_usuario;
        $id_bien    = $request->id_bien;

        $dataResultado = DB::select( "exec PA_ELIMINAR_BIEN_SW ?, ?, ?",[
            $id_entidad,
            $id_bien,
            $id_usuario
        ]);

        return response()->success($dataResultado);

    }

    public function Validar_Caracteritica_Bienes(request $request){

        $reglas = [
            'id_entidad'   => 'int',
            'id_acto'     => 'max:11'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

        $id_entidad = $request->id_entidad;
        $id_acto    = $request->id_acto;

        $dataResultado = DB::select( "exec PA_VALIDAR_CARACTERISTICAS_BIENES ?, ?",[
            $id_entidad,
            $id_acto
        ]);

        return response()->success($dataResultado);

    }

        

}
