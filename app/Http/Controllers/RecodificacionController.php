<?php

namespace sinabipmuebles\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use DB;
use Pdf_Resumen;
use funciones\funciones;
use Illuminate\Support\Facades\Storage;
use Validator;

class RecodificacionController extends Controller
{
	public function index()
    {
        return 'FUNCTION INDEX';
    }

    public function ListadoRecodificacion(Request $request) {

        $reglas = [
            'cod_entidad'   => 'int',
            'grupo'         => 'int',
            'clase'         => 'int',
            'page'          => 'int',
            'records'       => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
                ]);
        }

        $cod_entidad        = $request->cod_entidad;
        $tipo_bien          = $request->tipo_bien;
        $nombre_bien        = $request->nombre_bien;
        $grupo              = $request->grupo;
        $clase              = $request->clase;
        $page               = $request->page;
        $records            = $request->records;

        $dataGrupos = DB::select ('exec PA_LISTADO_GRUPOS');
        $dataClases = DB::select ('exec PA_LISTADO_CLASES ?', [$grupo]);

        $data = DB::select('exec PA_LISTADO_RECODIFICACION ?,?,?,?,?,?,?', [
            $cod_entidad,
            $tipo_bien,
            $nombre_bien,
            $grupo,
            $clase,
            $page,
            $records
        ]);

	    return response()->success([
            "reco" => (count($data) > 0) ?$data : [],
            "grupos"  => (count($dataGrupos) > 0) ?$dataGrupos : [],
            "clases"  => (count($dataClases) > 0) ?$dataClases : []
        ]);
    }

    public function ProcesarRecodificacion(Request $request){

        $reglas = [
            'cod_entidad'   => 'int',
            'oficio'        => 'max:20',
            'informe'       => 'max:20',
            'fecha'         => 'max:10'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
                ]);
        }

        $cod_entidad = $request->cod_entidad;
        $denominaciones = $request->denominaciones;
        $oficio = $request->oficio;
        $informe = $request->informe;
        $fecha = $request->fecha;

        $data = DB::select('exec PA_PROCESAR_RECODIFICACION ?,?,?,?,?', [
            $cod_entidad,
            $denominaciones,
            $oficio,
            $informe,
            $fecha
        ]);
        $cod_recod = $data[0]->codigo_recodificacion;
        $dataRecodDetalle = DB::select('exec PA_OBTENER_RECODIFICACION_DETALLE ?', [
            $cod_recod
        ]);
        return response()->success([
            "Resultado" => $data[0],
            "RecodDetalle"  => (count($dataRecodDetalle) > 0) ?$dataRecodDetalle : []
        ]);
        // return response()->success([
        //     "estado_recodificacion" => "RECODIFICACION_SATISFACTORIA"
        // ]);
    }

    public function fecha_sql($fecha){
        date_default_timezone_set('America/Lima');
        $datetime_variable = new DateTime($fecha);
        $datetime_formatted = $datetime_variable->format('Y-m-d H:i:s');
       return  $datetime_formatted;  
    }

    public function ImprimirResumenRecodificacion(Request $request){
        header('Content-type: application/pdf');
        $id = $request->cod_entidad;
        $NOM_ENTIDAD =  DB::select(
            "SELECT NOM_ENTIDAD FROM TBL_PADRON_ENTIDAD where COD_ENTIDAD = ?",[$id]
        );
        $pdf = new Pdf_Resumen;
        $pdf->AliasNbPages();
            $pdf->SetFont('Arial','',10);
            $pdf->AddPage();
            $pdf->Ln(4);
    
            //****************************************
            //****************************************
            
            $pdf->SetFillColor(205,205,205);//color de fondo tabla
            $pdf->SetTextColor(10);
            $pdf->SetDrawColor(153,153,153);
            $pdf->SetLineWidth(.3);
            $pdf->SetFont('Arial','B',9);
            
            $pdf->SetFillColor(205,205,205);
            $pdf->Cell(45,8,'ENTIDAD',1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(140,8,$NOM_ENTIDAD[0]->NOM_ENTIDAD,1,0,'L',true);
                    
            $pdf->Ln();
            $pdf->Ln(3);
            $w = array(10, 70, 105);
            // Línea de cierre
            $pdf->Cell(array_sum($w),0,'','T');
            
            $pdf->Ln();
    
            $pdf->Output();
            return 1;
    }
    
    public function ImprimirResumenRecodificacionGet(Request $request, $id, $codigo_recodificacion, $oficio, $informe, $fecha){
        header('Content-type: application/pdf');
        $NOM_ENTIDAD =  DB::select(
            "SELECT NOM_ENTIDAD FROM TBL_PADRON_ENTIDAD where COD_ENTIDAD = ?",[$id]
        );
        $dataRecodDetalle = DB::select('exec PA_OBTENER_RECODIFICACION_DETALLE ?', [
            $codigo_recodificacion
        ]);
        $pdf = new Pdf_Resumen;
        $pdf->AliasNbPages();
            $pdf->SetFont('Arial','',10);
            $pdf->AddPage();
            $pdf->Ln(4);
    
            //****************************************
            //****************************************
            
            $pdf->SetFillColor(205,205,205);//color de fondo tabla
            $pdf->SetTextColor(10);
            $pdf->SetDrawColor(153,153,153);
            $pdf->SetLineWidth(.3);
            $pdf->SetFont('Arial','B',9);
            
            $pdf->SetFillColor(205,205,205);
            $pdf->Cell(45,8,'ENTIDAD',1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(140,8,$NOM_ENTIDAD[0]->NOM_ENTIDAD,1,0,'L',true);
                    
            $pdf->Ln();
            $pdf->Ln(3);

            $pdf->SetFillColor(205,205,205);
            $pdf->Cell(45,8,'OFICIO',1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(140,8,$oficio,1,0,'L',true);
                    
            $pdf->Ln();
            $pdf->Ln(3);

            $pdf->SetFillColor(205,205,205);
            $pdf->Cell(45,8,'INFORME',1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(140,8,$informe,1,0,'L',true);
                    
            $pdf->Ln();
            $pdf->Ln(3);

            $pdf->SetFillColor(205,205,205);
            $pdf->Cell(45,8,'FECHA',1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(140,8,$fecha,1,0,'L',true);
                    
            $pdf->Ln();
            $pdf->Ln(3);
            $w = array(10, 70, 105);
            // Línea de cierre
            $pdf->Cell(array_sum($w),0,'','T');
            
            $pdf->Ln();
            $pdf->Ln(5);

            //DETALLE DEL BIEN
            $pdf->SetFont('Arial','B',9);
            $pdf->SetFillColor(205,205,205);	
            $pdf->Cell(185,7,utf8_decode('DETALLE DE LOS BIENES RECODIFICADOS'),1,0,'L',true);
            
            $pdf->Ln();
            
            $pdf->SetFont('Arial','B',8);
            $w = array(10, 70, 105);
            $pdf->Cell($w[0],5,utf8_decode('Item'),1,0,'C',true);
            $pdf->Cell($w[1],5,utf8_decode('Nro bien'),1,0,'C',true);
            $pdf->Cell($w[2],5,utf8_decode('Denominación'),1,0,'C',true);
            $pdf->Ln();
            
            // Restauración de colores y fuentes
            $pdf->SetFillColor(224,235,255);
            $pdf->SetTextColor(0);
            $pdf->SetFont('Arial','',7);
            // Datos
            $fill = false;
            $contador = 0;
            //ROW_NUMBER_ID, COD_UE_DENOMINACION, CAT.DENOM_BIEN 
            foreach ($dataRecodDetalle as $key => $value) {

                $COD_UE_DENOMINACION = $value->COD_UE_DENOMINACION;
                $DENOM_BIEN = $value->DENOM_BIEN;
                
                $contador++;
                $linea = 0;
                
                if($contador == 40){
                    $pdf->Cell(array_sum($w),0,'','T');
                    $pdf->AddPage();
                    $linea = 0;
                }else{
                    $linea++;
                }
                            
                if($linea!= 0 && $linea%58==0 ){
                    $pdf->Cell(array_sum($w),0,'','T');
                    $pdf->AddPage();
                 }
        
                $pdf->Cell($w[0],4,$contador,'LR',0,'C');
                $pdf->Cell($w[1],4,$COD_UE_DENOMINACION,'LR',0,'C');
                $pdf->Cell($w[2],4,$DENOM_BIEN,'LR',0,'C');
                $pdf->Ln();
                $fill = !$fill;
            }
            $pdf->Cell(array_sum($w),0,'','T');
            
            $pdf->Ln();
            $pdf->Output();
            return 1;
    }
}
