<?php

namespace sinabipmuebles\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use DB;
use Config;
use Illuminate\Support\Facades\Storage;
use Validator;

class DevolucionBienesActosAdministracion extends Controller
{
    
    public function index()
    {
        return 'FUNCTION INDEX';
    }

    public function ListadoDevolAdministracion(Request $request)
    {

        $reglas = [
            'cod_entidad'   => 'int',
            'nro_documento' => 'max:20',
            'page'          => 'int',
            'records'       => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

	    $cod_entidad        = $request->cod_entidad;
        $nro_documento      = $request->nro_documento;
        $estado             = $request->estado;
        $fecha_year         = $request->fecha['year'];
        $fecha_month        = $request->fecha['month'];
        $page               = $request->page;
        $records            = $request->records;
        
        $dataAnios = DB::select('exec PA_LISTADO_ANIOS');
        $dataMes = DB::select('exec PA_LISTADO_MESES');

        $data = DB::select('exec PA_LISTADO_DEVOLUCION_ADMINISTRACION ?,?,?,?,?,?,?', [ 
            $cod_entidad,
            $nro_documento,
            $fecha_year,
            $fecha_month,
            $estado,
            $page,
            $records
        ]);

	    return response()->success([
            "error" => false,
            "documento" => (count($data) > 0) ?$data : [],
            "anios"  => (count($dataAnios) > 0) ?$dataAnios : [],
            "mes"  => (count($dataMes) > 0) ?$dataMes : []
        ]);
    }

    public function editarDevolAdministracion(Request $request){

        $reglas = [
            'id_entidad'  => 'int',
            'id_acto'     => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

        $id_entidad = $request->id_entidad;
        $id_acto    = $request->id_acto;

        $dataCabActo = DB::select( "exec PA_EDITAR_DEVOL_ADMINISTRACION_CAB ?, ?",[$id_entidad,$id_acto]);
        $dataDetActo = DB::select( "exec PA_EDITAR_DEVOL_ADMINISTRACION_DET ?, ?",[$id_entidad,$id_acto]);
        //dd($id_entidad);
        return response()->success([
            "error" => false,
            "cabecera" => (count($dataCabActo) > 0) ?$dataCabActo[0] : [],
            "detalles"  => (count($dataDetActo) > 0) ?$dataDetActo : []
        ]);
    }

    
    public function ListadoBienesDevolFormaIndividual_Administracion(Request $request)    
    {
        $cod_entidad        = $request->cod_entidad;
        $nro_grupo          = $request->nro_grupo;
        $nro_clase          = $request->nro_clase;
        $cod_patrimonial    = $request->cod_patrimonial;
        $denom_bien         = $request->denom_bien;
        $page               = $request->page2;
        $records            = $request->records; 

        $dataLstGrupos = DB::select('exec PA_LISTADO_GRUPOS');
        $dataLstClases = DB::select('exec PA_LISTADO_CLASES ?',[ 
            $nro_grupo
        ]);
        $dataLstBienespatrimoniales = DB::select('exec PA_LISTADO_BIENES_DISPONIBLES_DEVOL_ADMINISTRACION ?,?,?,?,?,?,?',[ 
            $cod_entidad,
            $nro_grupo, 
            $nro_clase,
            $cod_patrimonial,
            $denom_bien,
            $page,
            $records
        ]);

        return response()->success([
            "grupos" => (count($dataLstGrupos) > 0) ?$dataLstGrupos : [],
            "clases"  => (count($dataLstClases) > 0) ?$dataLstClases : [],
            "bienespatrimoniales"  => (count($dataLstBienespatrimoniales) > 0) ?$dataLstBienespatrimoniales : []
        ]);
    }

    public function Guardar_DevolAdministracion(Request $request){

        $reglas = [
            'id_entidad'        => 'int',
            'nro_resolucion'    => 'max:20',
            'fecha_resolucion'  => 'max:10',
            'id_usuario'        => 'int',
            'id_acto'           => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

        $id_entidad         = $request->id_entidad;
        $nro_resolucion     = $request->nro_resolucion;
        $fecha_resolucion   = $request->fecha_resolucion;
        $id_usuario         = $request->id_usuario;
        $cod_patrimonial    = $request->cod_patrimonial;
        $mod_registro       = $request->mod_registro;
        $id_acto            = $request->id_acto;

        $dataCabActo = DB::select( "exec PA_REGISTRAR_ACTO_DEVOL_ADMINISTRACION ?, ?, ?, ?, ?, ?, ?",[
            $id_entidad,
            $nro_resolucion,
            $fecha_resolucion,
            $id_usuario,
            $cod_patrimonial,
            $mod_registro,
            $id_acto,
        ]);
        //dd($id_entidad);
        return response()->success($dataCabActo[0]);

    }

    public function Eliminar_ActosDevAdministracion(Request $request){ 

        $reglas = [
            'id_entidad'  => 'int',
            'id_acto'     => 'int',
            'id_usuario'  => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

        $id_entidad         = $request->id_entidad;
        $id_acto            = $request->id_acto;
        $id_usuario         = $request->id_usuario;

        $data = DB::select("exec PA_ELIMINAR_ACTOS_DEVOL_ADMINISTRACION ?,?,?",[ 
            $id_entidad,
            $id_acto,
            $id_usuario
        ]);
        return response()->success([
            "error" => false,
            "datos" => (count($data) > 0) ?$data : []
        ]);
    }

    
    public function Eliminacion_Detalles_DevAdministracion(Request $request){ 
        $id_entidad         = $request->id_entidad;
        $id_acto            = $request->id_acto;
        $id_bien            = $request->id_bien;
        $id_usuario         = $request->id_usuario;

        $data = DB::select("exec PA_ELIMINAR_DETALLE_DEVOL_ADMINISTRACION ?,?,?,?",[
            $id_entidad,
            $id_acto,
            $id_bien,
            $id_usuario
        ]);
        
        return response()->success([
            "datos" => (count($data) > 0) ?$data : []
        ]);
    }

    public function Agregar_Bienes_al_Detalle_Dev_Administracion(Request $request){ 
        // header("Access-Control-Allow-Origin: *");
        $id_entidad        = $request->id_entidad;
        $cod_patrimonial    = $request->cod_patrimonial;
       
        $data = DB::select(
            "exec PA_LISTAR_BIENES_SELECCIONADOS_ACTOS_DEVOL_ADMINISTRACION_SW ?,?",[$id_entidad,$cod_patrimonial]
        );

        return response()->success([
            "detalles" => (count($data) > 0) ?$data : []
        ]);
    }

    
    public function AdjuntarDevAdministraciontxt(Request $request, $data){
        
        $id_entidad	= $data[0];
        $tipo_acto = 10;
        $nomb_archivo = '';
        $usua_creacion = 100; 
        
        //$cod_alta = $cod_alta[0]->CODIGO.'_'; 
        $id_adjuntado =  DB::select( "SELECT TOP 1 ID_ADJUNTADOS_ACTOS_TL FROM TBL_ADJUNTADOS_ACTOS_TL ORDER BY ID_ADJUNTADOS_ACTOS_TL DESC" );
        
        if( empty($id_adjuntado) ){
            $id_adjuntado = 1;
        }else{
            $id_adjuntado = $id_adjuntado[0]->ID_ADJUNTADOS_ACTOS_TL;
            $id_adjuntado++  ;
        }
 
        if ($request->hasFile('file0')) {  
            $image = $request->file('file0');
            $ARCHIVO_EXTENSION = 'txt'; 
            $NUEVA_CADENA   = substr( md5(microtime()), 1, 4);
            $FECHA_ARCHIVO  = date('Y').''.date('m').''.date('d'); 
            $name = "devolucion_adm/";
            
            //dd($ARCHIVO_NOMBRE_GENERADO);
            $result = DB::select(
                "exec PA_ADJUNTADO_ACTOS_MASIVO_SW ?,?,?,?",[ 
                    $id_entidad,$tipo_acto,"",$usua_creacion
                    ]
            );
            
            $id_adjuntado = $result[0]->ID_ADJUNTADO; 
            
            
            $ARCHIVO_NOMBRE_GENERADO ='TXT_'. strval($id_adjuntado).'Dev_Administracion_Entidad_'.$id_entidad.'_Serie_'.$FECHA_ARCHIVO.'_'.$NUEVA_CADENA.'.'.$ARCHIVO_EXTENSION;
            //$url = Storage::disk('public')->putFileAs($name, $image,$ARCHIVO_NOMBRE_GENERADO); 
            if(Config::get('app.APP_LINUX')){
                \Storage::disk('local')->put("public/".$name.$ARCHIVO_NOMBRE_GENERADO, file_get_contents($image));
                // file_put_contents(storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
                $origen = storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO;
                $final = '/mnt/Inventarios_zip/'.$name.$ARCHIVO_NOMBRE_GENERADO;
                shell_exec('mv '.$origen.' '.$final);
            }else{
                file_put_contents("//SRV-PORTALSBN/Inventarios_zip$/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
            }
            $Peso_Arch = filesize($image);
 
            
            $result2 = DB::select(
                "exec PA_ADJUNTADO_ACTOS_MASIVO_UPD_NOMBRE_ARCHIVO_SW ?,?",[ 
                    $id_adjuntado,$ARCHIVO_NOMBRE_GENERADO
                    ]
            );
            
 
            //return response()->json($url);
            return response()->success($result2[0]);
 
        }
    }


    public function CargarDevAdministracionTXT(Request $request){

        $reglas = [
            'id_entidad'    => 'int',
            'id_adjuntado'  => 'int',
            'usua_creacion' => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

    $id_entidad     = $request->id_entidad;
    $id_adjuntado   = $request->id_adjuntado;
    $nombreArchivo  = $request->nombreArchivo;
    $usua_creacion  = $request->usua_creacion;

    $dataCarga = DB::select( "exec PA_CARGA_MASIVA_ACTOS_DEV_ADMINISTRACION_SW ?, ?, ?, ?",[
        $id_entidad,
        $id_adjuntado,
        $nombreArchivo,
        $usua_creacion
    ]);
    //return response()->success(true);
    return response()->success($dataCarga[0]);
    }

    public function ValidarDevAdministracionTXT(Request $request){

        $reglas = [
            'id_entidad'    => 'int',
            'id_adjuntado'  => 'int',
            'usua_creacion' => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

        $id_entidad     = $request->id_entidad;
        $id_adjuntado   = $request->id_adjuntado;
        $nombreArchivo  = $request->nombreArchivo;
        $usua_creacion  = $request->usua_creacion;

        $dataValidacion = DB::select( "exec PA_VALIDACION_MASIVA_ACTOS_DEV_ADMINISTRACION_SW ?, ?, ?",[
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);
        
        //return response()->success(true);
        return response()->success($dataValidacion[0]);

    }

    public function ListadoErroresCargaMasivaDevAdministracion(Request $request){
    
        $accion         = 1;
        $id_entidad     = $request->id_entidad;
        $id_adjuntado    = $request->id_adjuntado;
        $usua_creacion  = $request->usua_creacion;

        $dataObservaciones_1 = DB::select( "exec PA_OBSERVACIONES_VALIDACION_MASIVA_DEVOLUCION_ADMINISTRACION_SW ?, ?, ?, ?",[
            $accion,
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);

        $accion         = 2;
        $dataObservaciones_2 = DB::select( "exec PA_OBSERVACIONES_VALIDACION_MASIVA_DEVOLUCION_ADMINISTRACION_SW ?, ?, ?, ?",[
            $accion,
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);

        $accion         = 3;
        $dataObservaciones_3 = DB::select( "exec PA_OBSERVACIONES_VALIDACION_MASIVA_DEVOLUCION_ADMINISTRACION_SW ?, ?, ?, ?",[
            $accion,
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);

        $accion         = 4;
        $dataObservaciones_4 = DB::select( "exec PA_OBSERVACIONES_VALIDACION_MASIVA_ACTOS_ADMINISTRACION_SW ?, ?, ?, ?",[
        $accion,
        $id_entidad,
        $id_adjuntado,
        $usua_creacion
    ]);

        
        return response()->success([
            "duplicado_actos"  => (count($dataObservaciones_1) > 0) ?$dataObservaciones_1 : [],
            "errores_columnas"  => (count($dataObservaciones_2) > 0) ?$dataObservaciones_2 : [],
            "no_existe_codigos"  => (count($dataObservaciones_3) > 0) ?$dataObservaciones_3 : [],
            "no_disponibles"  => (count($dataObservaciones_4) > 0) ?$dataObservaciones_4 : []
        ]);

    }

    public function FinalizarDevAdministracionTXT(Request $request){
    
        $reglas = [
            'id_entidad'    => 'int',
            'id_adjuntado'  => 'int',
            'usua_creacion' => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
                ]);
        }

        $id_entidad     = $request->id_entidad;
        $id_adjuntado   = $request->id_adjuntado;
        $nombreArchivo  = $request->nombreArchivo;
        $usua_creacion  = $request->usua_creacion;

        $dataFinalizacion = DB::select( "exec PA_FINALIZACION_MASIVA_ACTOS_DEV_ADMINISTRACION_SW ?, ?, ?",[
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);
        
        //return response()->success(true);
        return response()->success($dataFinalizacion[0]);

    }


}
