<?php

namespace sinabipmuebles\Http\Controllers; 

use Illuminate\Http\Request;
use DateTime;
use DB;
use Pdf_Resumen;
use funciones\funciones;
use Illuminate\Support\Facades\Storage;
use Iluminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\Helpers\JwtAuth;
use Validator;

class PersonalController extends Controller
{
	public function index()
    {
        return 'FUNCTION INDEX';
    }


    public function ListadoPersonal(Request $request) { 

        $hash = $request->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checktoken = $jwtAuth->checkToken($hash);
        if ($checktoken) {
            $reglas = [
                'cod_entidad'   => 'int',
                'documento'     => 'max:11',
                'page'          => 'int',
                'records'       => 'int'
            ];
            $validator = Validator::make($request->all(), $reglas);
            if ($validator->fails()){
                return response()->success([
                    'error' => true,
                    'reco' => $validator->errors()
                    ]);
            }
            $cod_entidad    = $request->cod_entidad;
            $documento      = $request->documento;
            $nombres        = $request->nombres;
            $apellidopat    = $request->apellidopat;
            $apellidomat    = $request->apellidomat;
            $page           = $request->page;
            $records        = $request->records;
            $data = DB::select('exec PA_LISTADO_PERSONAL ?, ?, ?, ?, ?, ?, ?', [
                $cod_entidad,
                $documento,
                $nombres,
                $apellidopat,
                $apellidomat,
                $page,
                $records
            ]);
            //dd($data); 
            //die();
            $dataModalidad = DB::select('exec PA_LISTADO_MODALIDAD');
            return response()->success([
                'error' => false,
                "reco" => (count($data) > 0) ?$data : [],
                "moda" => (count($dataModalidad) > 0) ?$dataModalidad : []
            ]);
        }else{
            $data = array(
                'status' => 'error',
                'message' => 'No estas autorizado para acceder a esta ruta'
            );
        }
        
    }

    public function RegistrarPersonal(Request $request){

        $reglas = [
            'cod_entidad'   => 'int',
            'documento'     => 'max:11',
            'modalidad'     => 'int',
            'tipo'          => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

        $cod_entidad    = $request->cod_entidad;
        $documento      = $request->documento;
        $nombres        = $request->nombres;
        $apellidopat    = $request->apellidopat;
        $apellidomat    = $request->apellidomat;
        $modalidad      = $request->modalidad;
        $tipo           = $request->tipo;

        $data = DB::select('exec PA_REGISTRAR_PERSONAL ?, ?, ?, ?, ?, ?, ?', [
            $cod_entidad,
            $documento,
            $nombres,
            $apellidopat,
            $apellidomat,
            $modalidad,
            $tipo
        ]);

        return response()->success([
            'error' => false,
            $data[0]
        ]);
    }

    public function EditarPersonal(Request $request){
        
        $cod_entidad        = $request->cod_entidad;
        $documento          = $request->documento;
        $nombres            = $request->nombres;
        $apellidopat        = $request->apellidopat;
        $apellidomat        = $request->apellidomat;
        $modalidad          = $request->modalidad;
        $tipo               = $request->tipo;
        $cod_ue_pers        = $request->cod_ue_pers;


        $data = DB::select('exec PA_EDITAR_PERSONAL ?, ?, ?, ?, ?, ?, ?, ?', [
            $cod_entidad,
            $documento,
            $nombres,
            $apellidopat,
            $apellidomat,
            $modalidad,
            $tipo,
            $cod_ue_pers
        ]);
        return response()->success($data[0]);
    }



    public function ObtenerDataPersonal(Request $request){
        
        $reglas = [
            'paramEditar'   => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
                ]);
        }
        
        $cod_ue_pers = $request->paramEditar;
        $data = DB::select('exec PA_OBTENERDATA_PERSONAL ?', [
            $cod_ue_pers
        ]);
        return response()->success([
            'error' => false,
            $data[0]
        ]);

        
    }

    public function ProcesarBajaPersonal(Request $request){

        $reglas = [
            'paramBaja'   => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

        $cod_ue_pers = $request->paramBaja;
        $data = DB::select('exec PA_BAJA_PERSONAL ?', [
            $cod_ue_pers
        ]);
        return response()->success([
            'error' => false,
            $data[0]
        ]);

    }

    public function ProcesarEliminarPersonal(Request $request){

        $reglas = [
            'paramEliminar'   => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

        $cod_ue_pers = $request->paramEliminar;
        $data = DB::select('exec PA_ELIMINAR_PERSONAL ?', [
            $cod_ue_pers
        ]);
        
        return response()->success([ 
            'error' => false,
            $data[0]
        ]);

    }

    public function fecha_sql($fecha){
        date_default_timezone_set('America/Lima');
        $datetime_variable = new DateTime($fecha);
        $datetime_formatted = $datetime_variable->format('Y-m-d H:i:s');
       return  $datetime_formatted;  
    }
}
