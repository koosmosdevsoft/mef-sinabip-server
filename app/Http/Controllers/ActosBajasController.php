<?php

namespace sinabipmuebles\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use DB;
use Config;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Helpers\JwtAuth;
class ActosBajasController extends Controller
{
    

	public function index()
    {
        return 'FUNCTION INDEX';
    }

    
    public function ListadoActosBajas(Request $request) 
    {
        $hash = $request->header('Authorization', null);
        $jwtAuth = new JwtAuth();
		$checktoken = $jwtAuth->checkToken($hash);		
        if ($checktoken){
            $reglas = [
                'cod_entidad'   => 'int',
                'nro_documento' => 'max:20',
                'page'          => 'int',
                'records'       => 'int'
            ];
            $validator = Validator::make($request->all(), $reglas);
            if ($validator->fails()){
                return response()->success([
                    'error' => true,
                    'reco' => $validator->errors()
                ]);
            }
    
            $cod_entidad        = $request->cod_entidad;
            $forma_adquisicion  = $request->forma_adquisicion;
            $nro_documento      = $request->nro_documento;
            $estado             = $request->estado;
            $fecha_year         = $request->fecha['year'];
            $fecha_month        = $request->fecha['month'];
            $page               = $request->page;
            $records            = $request->records;
            
            $dataFormaAdq = DB::select('exec PA_LISTADO_FORMAS_BAJAS'); 
            $dataAnios = DB::select('exec PA_LISTADO_ANIOS');
            $dataMes = DB::select('exec PA_LISTADO_MESES');
    
            $data = DB::select('exec PA_LISTADO_ACTOS_BAJA ?,?,?,?,?,?,?,?', [ 
                $cod_entidad,
                $forma_adquisicion,
                $nro_documento,
                $fecha_year,
                $fecha_month,
                $estado,
                $page,
                $records
            ]);
    
            return response()->success([
                "documento" => (count($data) > 0) ?$data : [],
                "formaAdq"  => (count($dataFormaAdq) > 0) ?$dataFormaAdq : [],
                "anios"  => (count($dataAnios) > 0) ?$dataAnios : [],
                "mes"  => (count($dataMes) > 0) ?$dataMes : []
            ]);
        }else{
			return response()->success([
				'error' => true,
				'reco' => array(
					"id"=>'No estas autorizado para acceder a esta ruta'
				)
			]);	
		}
    }
    
    
    public function ListadoBienesFormaIndividual(Request $request)    
    {
        $cod_entidad        = $request->cod_entidad;
        $nro_grupo          = $request->nro_grupo;
        $nro_clase          = $request->nro_clase;
        $cod_patrimonial    = $request->cod_patrimonial;
        $denom_bien         = $request->denom_bien;
        $page               = $request->page;
        $records            = $request->records;

        $dataLstGrupos = DB::select('exec PA_LISTADO_GRUPOS');
        $dataLstClases = DB::select('exec PA_LISTADO_CLASES ?',[ 
            $nro_grupo
        ]);
        $dataLstBienespatrimoniales = DB::select('exec PA_LISTADO_BIENES_DISPONIBLES_BAJAS ?,?,?,?,?,?,?',[ 
            $cod_entidad,
            $nro_grupo, 
            $nro_clase,
            $cod_patrimonial,
            $denom_bien,
            $page,
            $records
        ]);

        return response()->success([
            "grupos" => (count($dataLstGrupos) > 0) ?$dataLstGrupos : [],
            "clases"  => (count($dataLstClases) > 0) ?$dataLstClases : [],
            "bienespatrimoniales"  => (count($dataLstBienespatrimoniales) > 0) ?$dataLstBienespatrimoniales : []
        ]);
    }



    public function AdjuntarBajatxt(Request $request, $data)
    {
        //$data = explode('-',$data);
       //header("Access-Control-Allow-Origin: *");
       $id_entidad	= $data[0];
       $tipo_acto = 2;
       $nomb_archivo = '';
       $usua_creacion = 100; 
       
       //$cod_alta = $cod_alta[0]->CODIGO.'_'; 
       $id_adjuntado =  DB::select( "SELECT TOP 1 ID_ADJUNTADOS_ACTOS_TL FROM TBL_ADJUNTADOS_ACTOS_TL ORDER BY ID_ADJUNTADOS_ACTOS_TL DESC" );
       
       if( empty($id_adjuntado) ){
           $id_adjuntado = 1;
       }else{
           $id_adjuntado = $id_adjuntado[0]->ID_ADJUNTADOS_ACTOS_TL;
           $id_adjuntado++  ;
       }

       if ($request->hasFile('file0')) {  
           $image = $request->file('file0');
           $ARCHIVO_EXTENSION = 'txt'; 
           $NUEVA_CADENA   = substr( md5(microtime()), 1, 4);
           $FECHA_ARCHIVO  = date('Y').''.date('m').''.date('d');  
           $name = "bajas/";
           
           //dd($ARCHIVO_NOMBRE_GENERADO);
           $result = DB::select(
               "exec PA_ADJUNTADO_ACTOS_MASIVO_SW ?,?,?,?",[ 
                   $id_entidad,2,"",$usua_creacion
                   ]
           );
           
           $id_adjuntado = $result[0]->ID_ADJUNTADO; 
           
           
           $ARCHIVO_NOMBRE_GENERADO ='TXT_'. strval($id_adjuntado).'Bajas_Entidad_'.$id_entidad.'_Serie_'.$FECHA_ARCHIVO.'_'.$NUEVA_CADENA.'.'.$ARCHIVO_EXTENSION;
           //$url = Storage::disk('public')->putFileAs($name, $image,$ARCHIVO_NOMBRE_GENERADO); 
        //    file_put_contents("//SRV-PORTALSBN/Inventarios_zip$/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
           if(Config::get('app.APP_LINUX')){
            // file_put_contents(storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
            \Storage::disk('local')->put("public/".$name.$ARCHIVO_NOMBRE_GENERADO, file_get_contents($image));
            $origen = storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO;
            $final = '/mnt/Inventarios_zip/'.$name.$ARCHIVO_NOMBRE_GENERADO;
            shell_exec('cp '.$origen.' '.$final);
            }else{
                file_put_contents("//SRV-PORTALSBN/Inventarios_zip$/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
                // file_put_contents("//ATILENDSK0816/Inventarios_zip2$/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
            }
           $Peso_Arch = filesize($image);

           
           $result2 = DB::select(
               "exec PA_ADJUNTADO_ACTOS_MASIVO_UPD_NOMBRE_ARCHIVO_SW ?,?",[ 
                   $id_adjuntado,$ARCHIVO_NOMBRE_GENERADO
                   ]
           );
           

           //return response()->json($url);
           return response()->success($result2[0]);

       }
    }

    public function CargarBajaTXT(Request $request){

        $reglas = [
            'id_entidad'    => 'int',
            'id_adjuntado'  => 'int',
            'usua_creacion' => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
                ]);
        }

        $id_entidad     = $request->id_entidad;
        $id_adjuntado   = $request->id_adjuntado;
        $nombreArchivo  = $request->nombreArchivo;
        $usua_creacion  = $request->usua_creacion;

        $dataCarga = DB::select( "exec PA_CARGA_MASIVA_ACTOS_BAJA_SW ?, ?, ?, ?",[
            $id_entidad,
            $id_adjuntado,
            $nombreArchivo,
            $usua_creacion
        ]);
        
        //return response()->success(true);
        return response()->success($dataCarga[0]);

    }

    public function ValidarBajaTXT(Request $request){

        $reglas = [
            'id_entidad'    => 'int',
            'id_adjuntado'  => 'int',
            'usua_creacion' => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
                ]);
        }

        $id_entidad     = $request->id_entidad;
        $id_adjuntado   = $request->id_adjuntado;
        $nombreArchivo  = $request->nombreArchivo;
        $usua_creacion  = $request->usua_creacion;

        $dataValidacion = DB::select( "exec PA_VALIDACION_MASIVA_ACTOS_BAJA_SW ?, ?, ?",[
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);
        
        //return response()->success(true);
        return response()->success($dataValidacion[0]);

    }

    public function FinalizarBajaTXT(Request $request){

        $reglas = [
            'id_entidad'    => 'int',
            'id_adjuntado'  => 'int',
            'usua_creacion' => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
                ]);
        }

        $id_entidad     = $request->id_entidad;
        $id_adjuntado   = $request->id_adjuntado;
        $nombreArchivo  = $request->nombreArchivo;
        $usua_creacion  = $request->usua_creacion;

        $dataFinalizacion = DB::select( "exec PA_FINALIZACION_MASIVA_ACTOS_BAJA_SW ?, ?, ?",[ 
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);
        
        //return response()->success(true);
        return response()->success($dataFinalizacion[0]);

    }

    public function ListadoErroresCargaMasivaBaja(Request $request){

        $accion         = 1;
        $id_entidad     = $request->id_entidad;
        $id_adjuntado    = $request->id_adjuntado;
        $usua_creacion  = $request->usua_creacion;

        $dataObservaciones_1 = DB::select( "exec PA_OBSERVACIONES_VALIDACION_MASIVA_ACTOS_BAJAS_SW ?, ?, ?, ?",[
            $accion,
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);

        $accion         = 2;
        $dataObservaciones_2 = DB::select( "exec PA_OBSERVACIONES_VALIDACION_MASIVA_ACTOS_BAJAS_SW ?, ?, ?, ?",[
            $accion,
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);

        $accion         = 3;
        $dataObservaciones_3 = DB::select( "exec PA_OBSERVACIONES_VALIDACION_MASIVA_ACTOS_BAJAS_SW ?, ?, ?, ?",[
            $accion,
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);

        $accion         = 4;
        $dataObservaciones_4 = DB::select( "exec PA_OBSERVACIONES_VALIDACION_MASIVA_ACTOS_BAJAS_SW ?, ?, ?, ?",[
            $accion,
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);
        
        return response()->success([
            "duplicado_actos"  => (count($dataObservaciones_1) > 0) ?$dataObservaciones_1 : [],
            "errores_columnas"  => (count($dataObservaciones_2) > 0) ?$dataObservaciones_2 : [],
            "no_existe_codigos"  => (count($dataObservaciones_3) > 0) ?$dataObservaciones_3 : [],
            "no_disponibles"  => (count($dataObservaciones_4) > 0) ?$dataObservaciones_4 : []
        ]);

    }


    public function editarActoBaja(Request $request){

        $reglas = [
            'id_entidad'   => 'int',
            'id_acto'     => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

        $id_entidad = $request->id_entidad;
        $id_acto    = $request->id_acto;

        $dataCabActo = DB::select( "exec PA_EDITAR_ACTOS_BAJA_CAB ?, ?",[$id_entidad,$id_acto]);
        $dataDetActo = DB::select( "exec PA_EDITAR_ACTOS_BAJA_DET ?, ?",[$id_entidad,$id_acto]);
        //dd($id_entidad);
        return response()->success([
            "error" => false,
            "cabecera" => (count($dataCabActo) > 0) ?$dataCabActo[0] : [],
            "detalles"  => (count($dataDetActo) > 0) ?$dataDetActo : []
        ]);
    }


    public function Eliminar_ActosBaja(Request $request){ 
        $id_entidad         = $request->id_entidad;
        $id_acto            = $request->id_acto;

        $data =  DB::select("exec PA_ELIMINAR_ACTOS_BAJA ?,?",[ 
            $id_entidad,
            $id_acto
        ]);
        return response()->success($data);
    }

    public function Agregar_Bienes_al_Detalle(Request $request){ 
        // header("Access-Control-Allow-Origin: *");
        $id_entidad        = $request->id_entidad;
        $cod_patrimonial    = $request->cod_patrimonial;
       
        $data = DB::select(
            "exec PA_LISTAR_BIENES_SELECCIONADOS_ACTOS_BAJA_SW ?,?",[$id_entidad,$cod_patrimonial]
        );

        return response()->success([
            "detalles" => (count($data) > 0) ?$data : []
        ]);
    }

    public function Eliminacion_Detalles_Acto(Request $request){ 
        // header("Access-Control-Allow-Origin: *");

        $reglas = [
            'id_entidad'        => 'int',
            'id_acto'           => 'int',
            'nro_resolucion'    => 'max:20',
            'fecha_resolucion'  => 'max:10',
            'id_usuario'        => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

        $id_entidad         = $request->id_entidad;
        $id_acto            = $request->id_acto;
        $nro_resolucion     = $request->nro_resolucion;
        $fecha_resolucion   = $request->fecha_resolucion;
        $cod_patrimonial    = $request->cod_patrimonial;
        $id_usuario         = $request->id_usuario;
       
        $data = DB::select(
            "exec PA_ELIMINACION_DETALLES_SELECCIONADOS_ACTOS_BAJA_SW ?,?,?,?,?,?",[
                $id_entidad,
                $id_acto,
                $nro_resolucion,
                $fecha_resolucion,
                $cod_patrimonial,
                $id_usuario
        ]);

        return response()->success([
            'error' => false,
            "datos" => (count($data) > 0) ?$data : []
        ]);
    }

    public function Guardar_ActosBaja(Request $request){

        $reglas = [
            'id_entidad'        => 'int',
            'cod_acto_baja'     => 'int',
            'nro_resolucion'    => 'max:20',
            'fecha_resolucion'  => 'max:10',
            'id_usuario'        => 'int',
            'id_acto'           => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
                ]);
        }

        $id_entidad         = $request->id_entidad;
        $cod_acto_baja      = $request->cod_acto_baja;
        $nro_resolucion     = $request->nro_resolucion;
        $fecha_resolucion   = $request->fecha_resolucion;
        $id_usuario         = $request->id_usuario;
        $cod_patrimonial    = $request->cod_patrimonial;
        $id_acto            = $request->id_acto;

        $dataCabActo = DB::select( "exec PA_REGISTRAR_ACTOS_BAJA ?, ?, ?, ?, ?, ?, ?",[
            $id_entidad,
            $cod_acto_baja,
            $nro_resolucion,
            $fecha_resolucion,
            $id_usuario,
            $cod_patrimonial,
            $id_acto
        ]);
        //dd($id_entidad);
        return response()->success($dataCabActo[0]);
    }

    public function ValidarPDFBajaCatalogo(Request $request){
        $id_entidad         = $request->id_entidad;
        $cod_patrimonial    = $request->cod_patrimonial;
        $cod_acto_baja      = $request->cod_acto_baja;

        $data = DB::select( "exec PA_VALIDAR_PDF_BAJA_CATALG ?, ?, ?",[
            $id_entidad,
            $cod_patrimonial,
            $cod_acto_baja
        ]);
        //dd($id_entidad);
        return response()->success($data);
    }

    // public function NombArchInffActConc2(Request $request){ 
	// 	$cod_inventario_ent = $request->cod_inventario_ent;
	// 	$estado = $request ->estado;

    //     $data = DB::select(
    //         "exec PA_NOM_INFFINAL_ACTCONC_2 ?,?",[$cod_inventario_ent,$estado]
    //     );
    //     return response()->success($data[0]);
    // }
    

    public function SubirFormatoBaja(Request $request,$data){
		$data = explode('-',$data);
		$id	= $data[0];	
      
        if ($request->hasFile('file0')) {
            $image = $request->file('file0');
			$ARCHIVO_EXTENSION = 'pdf';
			$NUEVA_CADENA				= substr( md5(microtime()), 1, 4);
			$FECHA_ARCHIVO				= date('Y').''.date('m').''.date('d');

			
            $name = "adjuntar_bajas/";
            $ARCHIVO_NOMBRE_GENERADO ='Inf_Baja'.$id.'_'.$FECHA_ARCHIVO.'_'.$NUEVA_CADENA.'.'.$ARCHIVO_EXTENSION;
			
            if(Config::get('app.APP_LINUX')){
                // file_put_contents(storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
                \Storage::disk('local')->put("public/".$name.$ARCHIVO_NOMBRE_GENERADO, file_get_contents($image));
                $origen = storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO;
                $final = '/mnt/Inventarios_zip/'.$name.$ARCHIVO_NOMBRE_GENERADO;
                shell_exec('cp '.$origen.' '.$final);
            }else{
                file_put_contents("//SRV-PORTALSBN/Inventarios_zip$/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
                // $url = Storage::disk('public')->putFileAs($name, $image,$ARCHIVO_NOMBRE_GENERADO);			

            }
			// $url = Storage::disk('public')->putFileAs($name, $image,$ARCHIVO_NOMBRE_GENERADO);			
			$Peso_Arch = filesize($image);
			// $result = DB::statement(
            //     "exec PA_ACT_INFFINAL_ACTCONC ?,?,?,?,?",[
			// 		$id,$COD_INVENTARIO_ENT,$ARCHIVO_NOMBRE_GENERADO,$Peso_Arch,$ESTADO
			// 		]
            // );
            return response()->json(['NOMBRE'=>$ARCHIVO_NOMBRE_GENERADO,'PESO'=>$Peso_Arch]);
        }
    }
    
    
    public function Consultar_Bienes_Baja_a_Eliminar(Request $request){
        $id_entidad = $request->id_entidad;
        $id_acto    = $request->id_acto;
        $cod_patrimonial    = $request->cod_patrimonial;

        $dataResultado = DB::select( "exec PA_CONSULTAR_BIENES_BAJA_A_ELIMINAR ?, ?, ?",[
            $id_entidad,
            $id_acto,
            $cod_patrimonial
            ]);
    
        return response()->success($dataResultado);
    }

    

    
	public function VerPDFBaja(Request $request,$archivo){
        $name = ruta_server()."adjuntar_bajas/";
		$contents = $name.$archivo;
		header("Content-type: application/pdf");
		header('Content-Disposition: inline; filename="'.$contents.'"');
		$data = file_get_contents($contents);
		echo $data;
		exit;
	}

    
	public function ActualizaPDFBaja(Request $request){ 
		$id = $request->id;
		$cod_ue_bien_baja = $request->cod_ue_bien_baja;
		$nomb_archivo = $request->nomb_archivo;
        $peso = $request->peso;
        
		DB::beginTransaction();
        try {
            DB::statement("exec PA_MODIFICAR_PDF_BAJA ?,?,?,?",
            [$id,$cod_ue_bien_baja,$nomb_archivo,$peso]);
            DB::commit();
        } catch (\Illuminate\database\QueryException $e) {
            DB::rollBack();
        	return response()->error($e->getMessage());
        }
        return response()->success(true);
	}

    public function fecha_sql($fecha){
        date_default_timezone_set('America/Lima');
        $datetime_variable = new DateTime($fecha);
        $datetime_formatted = $datetime_variable->format('Y-m-d H:i:s');
       return  $datetime_formatted;  
    }

    
}
