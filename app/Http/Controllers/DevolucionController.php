<?php

namespace sinabipmuebles\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use DB;
use funciones\funciones;
use Pdf_Resumen_Firma;
use Illuminate\Support\Facades\Storage;
use Validator;

class DevolucionController extends Controller
{

    public function index()
    {
        return 'FUNCTION INDEX';
    }

    public function ListadoPersonalDevolucion(Request $request){ 
        // header("Access-Control-Allow-Origin: *");

        $reglas = [
            'id'            => 'int',
            'nrodocument'   => 'max:11',
            'page'          => 'int',
            'records'       => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

        $id                 = $request->id;
        $nrodocument        = $request->nrodocument;
        $appaterpers        = $request->appaterpers;
        $apmaterpers        = $request->apmaterpers;
        $nombre             = $request->nombre;
		$page               = $request->page;
        $records            = $request->records;

        $data = DB::select(
            "exec PA_CONSULTA_PERSONAL ?,?,?,?,?,?,?",
            [$id,$nrodocument,$appaterpers,$apmaterpers,$nombre,$page,$records]);
        return response()->success($data);
    }

    public function CantBienesaDevolver(Request $request,$id,$idpers){ 
        // header("Access-Control-Allow-Origin: *");

        $validator = Validator::make(
            [
                'id' => $id,
                'idpers' => $idpers
            ],
            [
                'id' => 'int',
                'idpers' => 'int'
            ]
        );

        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
                ]);
        }

        $data = DB::select(
            "exec PA_CANT_BIENES_DEVOLVER ?,?",[$id,$idpers]
        );
        return response()->success($data);
    }
    
    public function ListadoDevolucionPerson(Request $request){ 
        // header("Access-Control-Allow-Origin: *");
        $id                 = $request->id;
        $cod_pers           = $request->cod_pers;
        $cod_patrimonial    = $request->cod_patrimonial;
        $denominacion       = $request->denominacion;
		$page               = $request->page;
        $records            = $request->records;

        $data = DB::select(
            "exec PA_LISTA_DEVOLUCION_BIENES ?,?,?,?,?,?",
            [$id,$cod_pers,$cod_patrimonial,$denominacion,$page,$records]
        );
        return response()->success($data);
    }
    
    public function ListaobtTodosCPDevol(Request $request,$id,$idpers){ 
        // header("Access-Control-Allow-Origin: *");

        $validator = Validator::make(
            [
                'id' => $id,
                'idpers' => $idpers
            ],
            [
                'id' => 'int',
                'idpers' => 'int'
            ]
        );
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
                ]);
        }
        

        $data = DB::select(
            "exec PA_OBTENER_LISTA_CODPATRIMONIAL_DEV ?,?",[$id,$idpers]
        );
        return response()->success($data);
    }
 
    
    public function AgregarCabDevolucion(Request $request){ 
        // header("Access-Control-Allow-Origin: *");

        $reglas = [
            'id'            => 'int',
            'id_personal'   => 'max:11',
            'id_usuario'    => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
                ]);
        }

        $id           = $request->id;
        $id_personal  = $request->id_personal;
        $motivo       = $request->motivo;
        $id_usuario   = $request->id_usuario;

        $data = DB::select(
            "exec PA_INSERT_CAB_DEVOLUCION ?,?,?,?",[$id,$id_personal,$motivo,$id_usuario]
        );
        return response()->success($data);
    }

    public function EliminarCabDevolucion(Request $request){ 
		$id       = $request->id;
        $num_asig = $request->num_asig;
        
		DB::beginTransaction();
        try {
            DB::statement("exec PA_DELETE_CAB_DEVOLUCION ?,?",[$id,$num_asig]);
            DB::commit();
        } catch (\Illuminate\database\QueryException $e) {
            DB::rollBack();
        	return response()->error($e->getMessage());
        }
        return response()->success(true);
    }


    public function AgregarDetDevolucion(Request $request){ 

        $reglas = [
            'id'           => 'int',
            'id_personal'  => 'int',
            'id_usuario'   => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

        $id           = $request->id;
        $Listcod_patrimonial = $request->Listcod_patrimonial;
        $num_devol = $request->num_devol;
        $id_personal  = $request->id_personal;
        $id_usuario   = $request->id_usuario;
        
		DB::beginTransaction();
        try {
            DB::statement("exec PA_INSERT_DET_DEVOLUCION ?,?,?,?,?",
            [$id,$Listcod_patrimonial,$num_devol,$id_personal,$id_usuario]);
            DB::commit();
        } catch (\Illuminate\database\QueryException $e) {
            DB::rollBack();
        	return response()->error($e->getMessage());
        }
        return response()->success(true);
    }


    public function Pdf_ResumenDevolucion(Request $request){
        header('Content-type: application/pdf');
        $id                = $request->id;
        $nrdevolu_act        = $request->nrdevolu_act;

        $NOM_ENTIDAD =  DB::select(
            "SELECT NOM_ENTIDAD FROM TBL_PADRON_ENTIDAD where COD_ENTIDAD = ?",[$id]
        );  
    
        $data_cab = DB::select(
            "exec PA_PDF_RESUMEN_DEVOLUCION ?,?",
            [$id,$nrdevolu_act]
            // [$id,$asig_personal,$asig_predio,$asig_area,$nrasig_act]
        );
    
        $NOMBRE_COMPLETO		= $data_cab[0]->NOMBRE_COMPLETO;
        $NRO_DOCUMENTO			= $data_cab[0]->NRO_DOCUMENTO;
        $NUMERO_DEVOL			= $data_cab[0]->NUMERO_DEVOL;
        $FECHA_REGISTRO			= $data_cab[0]->FECHA_REGISTRO;
        
         $pdf = new Pdf_Resumen_Firma;
    
         $pdf->AliasNbPages();
    
            $pdf->SetFont('Arial','',10);
            $pdf->AddPage();
            $pdf->Ln(4);
    
            //****************************************
            //****************************************
            
            $pdf->SetFillColor(205,205,205);//color de fondo tabla
            $pdf->SetTextColor(10);
            $pdf->SetDrawColor(153,153,153);
            $pdf->SetLineWidth(.3);
            $pdf->SetFont('Arial','B',9);
            
            $pdf->SetFillColor(205,205,205);
            $pdf->Cell(45,8,'ENTIDAD',1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(140,8,$NOM_ENTIDAD[0]->NOM_ENTIDAD,1,0,'L',true);
                    
            $pdf->Ln();
            $pdf->Ln(3);
            
            //--------------------		
            //RESUMEN DE Devolución DE BIENES MUEBLES
            //--------------------
            $pdf->SetFillColor(205,205,205);	
            $pdf->Cell(185,7,utf8_decode('RESUMEN DE DEVOLUCIÓN DE BIENES MUEBLES '),1,0,'C',true);		
            
            $pdf->Ln();
            
            $pdf->SetFillColor(230,230,230);	
            $pdf->Cell(52,6,utf8_decode('Nro. Devolución:'),1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);		
            $pdf->Cell(40,6,$NUMERO_DEVOL,1,0,'L',true);	
            
            $pdf->SetFillColor(230,230,230);
            $pdf->Cell(52,6,utf8_decode('Fecha Devolución:'),1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);
            $pdf->Cell(41,6,$FECHA_REGISTRO,1,0,'L',true);
         
            $pdf->Ln();
            $pdf->Ln(3);
            
            //--------------------		
            //DATOS DEL PERSONAL  
            //--------------------
            $pdf->SetFillColor(205,205,205);	
            $pdf->Cell(185,7,utf8_decode('DATOS DEL PERSONAL'),1,0,'L',true);		
                    
            $pdf->Ln();

            $pdf->SetFillColor(230,230,230);	
            $pdf->Cell(45,8,utf8_decode('Nro de Documento:'),1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);		
            $pdf->Cell(140,8,$NRO_DOCUMENTO,1,0,'L',true);	
            
            $pdf->Ln();
            
            $pdf->SetFillColor(230,230,230);	
            $pdf->Cell(45,8,utf8_decode('Nombre:'),1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);		
            $pdf->Cell(140,8,utf8_decode($NOMBRE_COMPLETO),1,0,'L',true);
             
            /*******************************************************/
            //DETALLE DEL BIEN
            /*******************************************************/
            
            $pdf->Ln();
            $pdf->Ln(5);

            //DETALLE DEL BIEN
            $pdf->SetFont('Arial','B',9);
            $pdf->SetFillColor(205,205,205);	
            $pdf->Cell(185,7,utf8_decode('DETALLE DE LOS BIENES A DEVOLVER'),1,0,'L',true);
            
            $pdf->Ln();
            
            $pdf->SetFont('Arial','B',8);
            $w = array(10, 70, 105);
            $pdf->Cell($w[0],5,utf8_decode('Item'),1,0,'C',true);
            $pdf->Cell($w[1],5,utf8_decode('Codigo Patrimonial'),1,0,'C',true);
            $pdf->Cell($w[2],5,utf8_decode('Denominación'),1,0,'C',true);
            $pdf->Ln();
            
            // Restauración de colores y fuentes
            $pdf->SetFillColor(224,235,255);
            $pdf->SetTextColor(0);
            $pdf->SetFont('Arial','',7);
            // Datos
            $fill = false;
            
            $contador = 0; 

            $data_det = DB::select(
                "exec PA_PDF_DET_RESUMEN_DEVOLUCION ?,?",[$id,$nrdevolu_act]
            );
    
            foreach ($data_det as $key => $value) {

                $DENOMINACION_BIEN = $value->DENOMINACION_BIEN;
                $CODIGO_PATRIMONIAL_DET = $value->CODIGO_PATRIMONIAL;
                
                $contador++;
                $linea = 0;
                
                if($contador == 40){
                    $pdf->Cell(array_sum($w),0,'','T');
                    $pdf->AddPage();
                    $linea = 0;
                }else{
                    $linea++;
                }
                            
                if($linea!= 0 && $linea%58==0 ){
                    $pdf->Cell(array_sum($w),0,'','T');
                    $pdf->AddPage();
                 }
        
                $pdf->Cell($w[0],4,$contador,'LR',0,'C');
                $pdf->Cell($w[1],4,$CODIGO_PATRIMONIAL_DET,'LR',0,'C');
                $pdf->Cell($w[2],4,utf8_decode($DENOMINACION_BIEN),'LR',0,'C');
                $pdf->Ln();
                $fill = !$fill;
            }
    
            // Línea de cierre
            $pdf->Cell(array_sum($w),0,'','T');
            
            $pdf->Ln();
    
            $pdf->Output();
            exit;
           
        }

}
