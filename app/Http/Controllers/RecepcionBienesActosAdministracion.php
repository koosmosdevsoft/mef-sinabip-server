<?php

namespace sinabipmuebles\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use DB;
use Config;
use Illuminate\Support\Facades\Storage;
use Validator;

class RecepcionBienesActosAdministracion extends Controller
{
    
    public function index()
    {
        return 'FUNCTION INDEX';
    }

    public function ListadoTipoAdministracion_Recepcion()
    {
        
        $dataFormaAdq = DB::select('exec PA_LISTADO_TIPO_ADMINISTRACION_RECEPCION'); 
        $dataTipoBen = DB::select('exec PA_LISTADO_TIPO_BENEFICIARIO_RECEPCION');

	    return response()->success([
            "documento" => (count($dataFormaAdq) > 0) ?$dataFormaAdq : [],
            "tipoBenef" => (count($dataTipoBen) > 0) ?$dataTipoBen : []
        ]);
    }

    public function Listado_Recepcion_Bienes(Request $request)
    {

        $reglas = [
            'cod_entidad'   => 'int',
            'nro_documento' => 'max:20',
            'page'          => 'int',
            'records'       => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
                ]);
        }

	    $cod_entidad        = $request->cod_entidad;
        $forma_adquisicion  = $request->forma_adquisicion;
        $nro_documento      = $request->nro_documento;
        $estado             = $request->estado;
        $fecha_year         = $request->fecha['year'];
        $fecha_month        = $request->fecha['month'];
        $page               = $request->page;
        $records            = $request->records;
        
        $dataFormaAdq = DB::select('exec PA_LISTADO_TIPO_ADMINISTRACION_RECEPCION'); 
        $dataAnios = DB::select('exec PA_LISTADO_ANIOS');
        $dataMes = DB::select('exec PA_LISTADO_MESES');

        $data = DB::select('exec PA_LISTADO_RECEPCION_INGRESO_BIENES_ACTO_ADMINISTRACION_V2 ?,?,?,?,?,?,?,?', [ 
            $cod_entidad,
            $forma_adquisicion,
            $nro_documento,
            $fecha_year,
            $fecha_month,
            $estado,
            $page,
            $records

        ]);

	    return response()->success([
            "documento" => (count($data) > 0) ?$data : [],
            "formaAdq"  => (count($dataFormaAdq) > 0) ?$dataFormaAdq : [],
            "anios"  => (count($dataAnios) > 0) ?$dataAnios : [],
            "mes"  => (count($dataMes) > 0) ?$dataMes : []
        ]);
    }


    public function DatosListadosFormaIndividual(Request $request){
        $id_forma = $request->id_forma;

        $dataCabActo = DB::select( "exec PA_LISTADOS_FORMAINDIVIDUAL ? ", [$id_forma]);
        
        return response()->success([
            "listadoTipoBeneficiario" => (count($dataCabActo) > 0) ?$dataCabActo : []
        ]);
    }


    
    // public function editarActoAdministracion(Request $request){
    //     $id_entidad = $request->id_entidad;
    //     $id_acto    = $request->id_acto;

    //     $dataCabActo = DB::select( "exec PA_EDITAR_ACTOS_ADMINISTRACION_CAB ?, ?",[$id_entidad,$id_acto]);
    //     $dataDetActo = DB::select( "exec PA_EDITAR_ACTOS_ADMINISTRACION_DET ?, ?",[$id_entidad,$id_acto]);
    //     //dd($id_entidad);
    //     return response()->success([
    //         "cabecera" => (count($dataCabActo) > 0) ?$dataCabActo[0] : [],
    //         "detalles"  => (count($dataDetActo) > 0) ?$dataDetActo : []
    //     ]);
    // }

    public function ListadoTipoBeneficiario(Request $request){
        $id_forma = $request->id_forma;

        $dataCabActo = DB::select( "exec PA_LISTADOS_FORMAINDIVIDUAL ? ", [$id_forma]);
        
        return response()->success([
            "listadoTipoBeneficiario" => (count($dataCabActo) > 0) ?$dataCabActo : []
        ]);
    }


    public function ListadoBienesFormaIndividual_Administracion(Request $request)    
    {
        $cod_entidad        = $request->cod_entidad;
        $nro_grupo          = $request->nro_grupo;
        $nro_clase          = $request->nro_clase;
        $cod_patrimonial    = $request->cod_patrimonial;
        $denom_bien         = $request->denom_bien;
        $page               = $request->page;
        $records            = $request->records; 

        $dataLstGrupos = DB::select('exec PA_LISTADO_GRUPOS');
        $dataLstClases = DB::select('exec PA_LISTADO_CLASES ?',[ 
            $nro_grupo
        ]);
        $dataLstBienespatrimoniales = DB::select('exec PA_LISTADO_BIENES_DISPONIBLES_ADMINISTRACION ?,?,?,?,?,?,?',[ 
            $cod_entidad,
            $nro_grupo, 
            $nro_clase,
            $cod_patrimonial,
            $denom_bien,
            $page,
            $records
        ]);

        return response()->success([
            "grupos" => (count($dataLstGrupos) > 0) ?$dataLstGrupos : [],
            "clases"  => (count($dataLstClases) > 0) ?$dataLstClases : [],
            "bienespatrimoniales"  => (count($dataLstBienespatrimoniales) > 0) ?$dataLstBienespatrimoniales : []
        ]);
    }


    // public function Agregar_Bienes_al_Detalle_Administracion(Request $request){ 
    //     // header("Access-Control-Allow-Origin: *");
    //     $id_entidad        = $request->id_entidad;
    //     $cod_patrimonial    = $request->cod_patrimonial;
       
    //     $data = DB::select(
    //         "exec PA_LISTAR_BIENES_SELECCIONADOS_ACTOS_ADMINISTRACION_SW ?,?",[$id_entidad,$cod_patrimonial]
    //     );

    //     return response()->success([
    //         "detalles" => (count($data) > 0) ?$data : []
    //     ]);
    // }

    

    // public function Guardar_ActosAdministracion(Request $request){
    //     $id_entidad         = $request->id_entidad;
    //     $cod_acto_admin     = $request->cod_acto_admin;
    //     $nro_resolucion     = $request->nro_resolucion;
    //     $fecha_resolucion   = $request->fecha_resolucion;
    //     $fecha_inicio       = $request->fecha_inicio;
    //     $fecha_vcto         = $request->fecha_vcto;
    //     $tipoBeneficiario   = $request->tipoBeneficiario;
    //     $ruc_beneficiario   = $request->ruc_beneficiario;
    //     $id_usuario         = $request->id_usuario;
    //     $cod_patrimonial    = $request->cod_patrimonial;
    //     $id_acto            = $request->id_acto; 

    //     $dataCabActo = DB::select( "exec PA_REGISTRAR_ACTOS_ADMINISTRACION ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?",[
    //         $id_entidad,
    //         $cod_acto_admin,
    //         $nro_resolucion,
    //         $fecha_resolucion,
    //         $fecha_inicio,
    //         $fecha_vcto,
    //         $tipoBeneficiario,
    //         $ruc_beneficiario,
    //         $id_usuario,
    //         $cod_patrimonial,
    //         $id_acto
    //     ]);
    //     //dd($id_entidad);
    //     return response()->success($dataCabActo[0]);

    // }
  

    public function AdjuntarAdministraciontxt(Request $request, $data){
        
        $id_entidad	= $data[0];
        $tipo_acto = 3;
        $nomb_archivo = '';
        $usua_creacion = 100; 
        
        //$cod_alta = $cod_alta[0]->CODIGO.'_'; 
        $id_adjuntado =  DB::select( "SELECT TOP 1 ID_ADJUNTADOS_ACTOS_TL FROM TBL_ADJUNTADOS_ACTOS_TL ORDER BY ID_ADJUNTADOS_ACTOS_TL DESC" );
        
        if( empty($id_adjuntado) ){
            $id_adjuntado = 1;
        }else{
            $id_adjuntado = $id_adjuntado[0]->ID_ADJUNTADOS_ACTOS_TL;
            $id_adjuntado++  ;
        }
 
        if ($request->hasFile('file0')) {  
            $image = $request->file('file0');
            $ARCHIVO_EXTENSION = 'txt'; 
            $NUEVA_CADENA   = substr( md5(microtime()), 1, 4);
            $FECHA_ARCHIVO  = date('Y').''.date('m').''.date('d'); 
            $name = "administracion/";
            
            //dd($ARCHIVO_NOMBRE_GENERADO);
            $result = DB::select(
                "exec PA_ADJUNTADO_ACTOS_MASIVO_SW ?,?,?,?",[ 
                    $id_entidad,$tipo_acto,"",$usua_creacion
                    ]
            );
            
            $id_adjuntado = $result[0]->ID_ADJUNTADO; 
            
            
            $ARCHIVO_NOMBRE_GENERADO ='TXT_'. strval($id_adjuntado).'Administracion_Entidad_'.$id_entidad.'_Serie_'.$FECHA_ARCHIVO.'_'.$NUEVA_CADENA.'.'.$ARCHIVO_EXTENSION;
            //$url = Storage::disk('public')->putFileAs($name, $image,$ARCHIVO_NOMBRE_GENERADO); 
            if(Config::get('app.APP_LINUX')){
                \Storage::disk('local')->put("public/".$name.$ARCHIVO_NOMBRE_GENERADO, file_get_contents($image));
                // file_put_contents(storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
                $origen = storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO;
                $final = '/mnt/Inventarios_zip/'.$name.$ARCHIVO_NOMBRE_GENERADO;
                shell_exec('mv '.$origen.' '.$final);
            }else{
                file_put_contents("//SRV-PORTALSBN/Inventarios_zip$/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
            }
            $Peso_Arch = filesize($image);
 
            
            $result2 = DB::select(
                "exec PA_ADJUNTADO_ACTOS_MASIVO_UPD_NOMBRE_ARCHIVO_SW ?,?",[ 
                    $id_adjuntado,$ARCHIVO_NOMBRE_GENERADO
                    ]
            );
            
 
            //return response()->json($url);
            return response()->success($result2[0]);
 
        }
    }



    public function CargarRecepcionTXT(Request $request){

        $reglas = [
            'id_entidad'    => 'int',
            'id_adjuntado'  => 'int',
            'usua_creacion' => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
                ]);
        }

        $id_entidad     = $request->id_entidad;
        $id_adjuntado   = $request->id_adjuntado;
        $nombreArchivo  = $request->nombreArchivo;
        $usua_creacion  = $request->usua_creacion;
    
        $dataCarga = DB::select( "exec PA_CARGA_MASIVA_RECEPCION_BIENES_SW ?, ?, ?, ?",[
            $id_entidad,
            $id_adjuntado,
            $nombreArchivo,
            $usua_creacion
        ]);
        
        return response()->success($dataCarga[0]);
    
        }
    
        public function ValidarRecepcionTXT(Request $request){
    
            $reglas = [
                'id_entidad'    => 'int',
                'id_adjuntado'  => 'int',
                'usua_creacion' => 'int'
            ];
            $validator = Validator::make($request->all(), $reglas);
            if ($validator->fails()){
                return response()->success([
                    'error' => true,
                    'reco' => $validator->errors()
                ]);
            }

            $id_entidad     = $request->id_entidad;
            $id_adjuntado   = $request->id_adjuntado;
            $nombreArchivo  = $request->nombreArchivo;
            $usua_creacion  = $request->usua_creacion;
    
            $dataValidacion = DB::select( "exec PA_VALIDACION_MASIVA_RECEPCION_BIENES_SW ?, ?, ?",[
                $id_entidad,
                $id_adjuntado,
                $usua_creacion
            ]);
            
            //return response()->success(true);
            return response()->success($dataValidacion[0]);
    
        }
    
        public function FinalizarRecepcionTXT(Request $request){
    
            $reglas = [
                'id_entidad'    => 'int',
                'id_adjuntado'  => 'int',
                'usua_creacion' => 'int'
            ];
            $validator = Validator::make($request->all(), $reglas);
            if ($validator->fails()){
                return response()->success([
                    'error' => true,
                    'reco' => $validator->errors()
                ]);
            }

            $id_entidad     = $request->id_entidad;
            $id_adjuntado   = $request->id_adjuntado;
            $nombreArchivo  = $request->nombreArchivo;
            $usua_creacion  = $request->usua_creacion;
    
            $dataFinalizacion = DB::select( "exec PA_FINALIZACION_MASIVA_RECEPCION_BIENES_SW_V2 ?, ?, ?",[
                $id_entidad,
                $id_adjuntado,
                $usua_creacion
            ]);
            
            //return response()->success(true);
            return response()->success($dataFinalizacion[0]);
    
        }
    
        public function ListadoErroresCargaMasivarRecepcion(Request $request){

            $accion         = 1;
            $id_entidad     = $request->id_entidad;
            $id_adjuntado    = $request->id_adjuntado;
            $usua_creacion  = $request->usua_creacion;
    
            $dataObservaciones_1 = DB::select( "exec PA_OBSERVACIONES_VALIDACION_MASIVA_RECEPCION_BIENES_SW ?, ?, ?, ?",[
                $accion,
                $id_entidad,
                $id_adjuntado,
                $usua_creacion
            ]);
    
            $accion         = 2;
            $dataObservaciones_2 = DB::select( "exec PA_OBSERVACIONES_VALIDACION_MASIVA_RECEPCION_BIENES_SW ?, ?, ?, ?",[
                $accion,
                $id_entidad,
                $id_adjuntado,
                $usua_creacion
            ]);
    
            $accion         = 3;
            $dataObservaciones_3 = DB::select( "exec PA_OBSERVACIONES_VALIDACION_MASIVA_RECEPCION_BIENES_SW ?, ?, ?, ?",[
                $accion,
                $id_entidad,
                $id_adjuntado,
                $usua_creacion
            ]);
            
            return response()->success([
                "duplicado_bienes" => (count($dataObservaciones_1) > 0) ?$dataObservaciones_1 : [], 
                "duplicado_actos"  => (count($dataObservaciones_2) > 0) ?$dataObservaciones_2 : [],
                "errores_columnas"  => (count($dataObservaciones_3) > 0) ?$dataObservaciones_3 : []
            ]);
    
        }


    public function Eliminacion_Detalles_Administracion(Request $request){ 
        $id_entidad         = $request->id_entidad;
        $id_acto            = $request->id_acto;
        $id_bien            = $request->id_bien;
        $id_usuario         = $request->id_usuario;

        $data = DB::select("exec PA_ELIMINAR_DETALLE_ADMINISTRACION_SW ?,?,?,?",[
            $id_entidad,
            $id_acto,
            $id_bien,
            $id_usuario
        ]);
        
        return response()->success([
            "datos" => (count($data) > 0) ?$data : []
        ]);
    }

    public function Eliminar_ActosAdministracion(Request $request){ 
        $id_entidad         = $request->id_entidad;
        $id_acto            = $request->id_acto;
        $id_usuario         = $request->id_usuario;

        $data = DB::select("exec PA_ELIMINAR_ACTOS_ADMINISTRACION ?,?,?",[ 
            $id_entidad,
            $id_acto,
            $id_usuario
        ]);
        return response()->success([
            "datos" => (count($data) > 0) ?$data : []
        ]);
    }



    public function RegistrarActosAdministracion_Captura_Errores(Request $request)
    {

        DB::beginTransaction();
        try {
            DB::select('exec PA_REGISTRAR_ALUMNOS');
            DB::select('exec PA_REGISTRAR_NOTAS');
            DB::commit();
        } catch (\Illuminate\database\QueryException $e) {
            DB::rollBack();
            //echo 'Excepción capturada: ',  $e->getMessage(), "\n";

        return response()->error($e->getMessage());
        }

        return response()->success(true);
        
    }


    public function ListadoCatalogoBienes(Request $request)
    {

        $cod_entidad        = $request->cod_entidad;
        $nro_grupo          = $request->nro_grupo;
        $nro_clase          = $request->nro_clase;
        $denom_bien         = $request->denom_bien;
        $nro_bien           = $request->nro_bien;
        $cod_patrimonial    = $request->cod_patrimonial;
        $denom_patrimonial  = $request->denom_patrimonial;
        $page               = $request->page;
        $records            = $request->records;

        //dd('aaa');
        $dataLstGrupos = DB::select('exec PA_LISTADO_GRUPOS'); 
        $dataLstClases = DB::select('exec PA_LISTADO_CLASES ?',[ 
            $nro_grupo
        ]);
        $dataLstCatalogo = DB::select('exec PA_LISTADO_CATALOGO_RECEPCION ?,?,?,?,?,?',[ 
            $nro_grupo, 
            $nro_clase,
            $denom_bien,
            $nro_bien,
            $page,
            $records
        ]);

        return response()->success([
            "grupos" => (count($dataLstGrupos) > 0) ?$dataLstGrupos : [],
            "clases"  => (count($dataLstClases) > 0) ?$dataLstClases : [],
            "catalogo"  => (count($dataLstCatalogo) > 0) ?$dataLstCatalogo : []
        ]);

    }


    public function AgregarBien_para_Recepcion(Request $request){

        $id_acto            = $request->id_acto;
        $id_entidad         = $request->id_entidad;
        $id_forma           = $request->id_forma;
        $nro_documento      = $request->nro_documento;
        $fecha_documento    = $request->fecha_documento;
        $fecha_inicio       = $request->fecha_inicio;
        $fecha_vencimiento  = $request->fecha_vencimiento;
        $tipo_benef         = $request->tipo_benef;
        $ruc_beneficiario   = $request->ruc_beneficiario;
        $motivo             = $request->motivo;
        $usua_creacion      = $request->usua_creacion; 
        $codigo_patrimonial = $request->codigo_patrimonial;
        $denominacion_bien  = $request->denominacion_bien;
        $cantidad           = $request->cantidad;
        $valor_adq          = $request->valor_adq;

        $dataCabActo = DB::select( "exec PA_REGISTRAR_BIENES_POR_RECEPCION_V2 ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?",[
            $id_acto, 
            $id_entidad,
            $id_forma,
            $nro_documento,
            $fecha_documento,
            $fecha_inicio,
            $fecha_vencimiento,
            $tipo_benef,
            $ruc_beneficiario,
            $motivo,
            $usua_creacion,
            $codigo_patrimonial,
            $denominacion_bien,
            $cantidad,
            $valor_adq
        ]);
        //dd($id_entidad);
        return response()->success([
            "cabecera" => (count($dataCabActo) > 0) ?$dataCabActo[0] : []
        ]);
        return response()->success(true);
    }


    public function EditarRecepcion(Request $request){

        $reglas = [
            'id_entidad'  => 'int',
            'id_acto'     => 'max:11'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

        $id_entidad = $request->id_entidad;
        $id_acto = $request->id_acto;

        $dataCabActo = DB::select( "exec PA_EDITAR_RECEPCION_CAB_V2 ?, ?",[$id_entidad,$id_acto]);
        $dataDetActo = DB::select( "exec PA_EDITAR_RECEPCION_DET_V2 ?, ?",[$id_entidad,$id_acto]);
        //dd($id_entidad);
        return response()->success([
            "error" => false,
            "cabecera" => (count($dataCabActo) > 0) ?$dataCabActo[0] : [],
            "detalles"  => (count($dataDetActo) > 0) ?$dataDetActo : []
        ]);
    }

    public function Eliminar_Recepcion(Request $request){

        $reglas = [
            'id_entidad'   => 'int',
            'id_acto'     => 'max:11'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
                ]);
        }

        $id_entidad         = $request->id_entidad;
        $id_acto            = $request->id_acto;

        $data =  DB::select("exec PA_ELIMINAR_RECEPCION ?,?",[ 
            $id_entidad,
            $id_acto
        ]);
        return response()->success($data);
    }

    public function AdjuntarRecepciontxt(Request $request, $data){
        //$data = explode('-',$data);
       //header("Access-Control-Allow-Origin: *");
       $id_entidad	= $data[0];
       $tipo_acto = 8;
       $nomb_archivo = '';
       $usua_creacion = 100; 
       
       //$cod_alta = $cod_alta[0]->CODIGO.'_'; 
       $id_adjuntado =  DB::select( "SELECT TOP 1 ID_ADJUNTADOS_ACTOS_TL FROM TBL_ADJUNTADOS_ACTOS_TL ORDER BY ID_ADJUNTADOS_ACTOS_TL DESC" );
       
       if( empty($id_adjuntado) ){
           $id_adjuntado = 1;
       }else{
           $id_adjuntado = $id_adjuntado[0]->ID_ADJUNTADOS_ACTOS_TL;
           $id_adjuntado++  ;
       }

       if ($request->hasFile('file0')) {  
           $image = $request->file('file0');
           $ARCHIVO_EXTENSION = 'txt'; 
           $NUEVA_CADENA   = substr( md5(microtime()), 1, 4);
           $FECHA_ARCHIVO  = date('Y').''.date('m').''.date('d'); 
           $name = "recepcion/";
           
           //dd($ARCHIVO_NOMBRE_GENERADO);
           $result = DB::select(
               "exec PA_ADJUNTADO_ACTOS_MASIVO_SW ?,?,?,?",[ 
                   $id_entidad,8,"",$usua_creacion
                   ]
           );
           
           $id_adjuntado = $result[0]->ID_ADJUNTADO; 
           
           
           $ARCHIVO_NOMBRE_GENERADO ='TXT_'. strval($id_adjuntado).'Recepcion_Entidad_'.$id_entidad.'_Serie_'.$FECHA_ARCHIVO.'_'.$NUEVA_CADENA.'.'.$ARCHIVO_EXTENSION;
           //$url = Storage::disk('public')->putFileAs($name, $image,$ARCHIVO_NOMBRE_GENERADO); 
           // file_put_contents("//SRV-PORTALSBN/Inventarios_zip$/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
           if(Config::get('app.APP_LINUX')){
               // file_put_contents(storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
               \Storage::disk('local')->put("public/".$name.$ARCHIVO_NOMBRE_GENERADO, file_get_contents($image));
               $origen = storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO;
               $final = '/mnt/Inventarios_zip/'.$name.$ARCHIVO_NOMBRE_GENERADO;
               shell_exec('mv '.$origen.' '.$final);
           }else{
               file_put_contents("//SRV-PORTALSBN/Inventarios_zip$/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
           }
           $Peso_Arch = filesize($image);

           
           $result2 = DB::select(
               "exec PA_ADJUNTADO_ACTOS_MASIVO_UPD_NOMBRE_ARCHIVO_SW ?,?",[  
                   $id_adjuntado,$ARCHIVO_NOMBRE_GENERADO
                   ]
           );
           

           //return response()->json($url);
           return response()->success($result2[0]);

       }
   }


   public function Validar_Caracteritica_Bienes_Recepcion(request $request){

    $reglas = [
        'id_entidad'   => 'int',
        'id_acto'     => 'int'
    ];
    $validator = Validator::make($request->all(), $reglas);
    if ($validator->fails()){
        return response()->success([
            'error' => true,
            'reco' => $validator->errors()
        ]);
    }

    $id_entidad = $request->id_entidad;
    $id_acto    = $request->id_acto;

    $dataResultado = DB::select( "exec PA_VALIDAR_CARACTERISTICAS_BIENES_RECEPCION ?, ?",[
        $id_entidad,
        $id_acto
    ]);

    return response()->success($dataResultado);

    }

    

    public function Datos_Caracteristicas_Bien_Recepcion(Request $request){

        $id_bien = $request->id_bien;

        $dataLstCuentasContables = DB::select("exec PA_LISTADO_CUENTAS_CONTABLES");
        $dataLstEstadoBien = DB::select("exec PA_LISTADO_ESTADO_BIEN");
        $dataCaracteristicasBien = DB::select("exec PA_CARACTERISTICAS_BIEN_RECEPCION ?", [
            $id_bien
        ]);
 
        return response()->success([
            "CuentasContables" => (count($dataLstCuentasContables) > 0) ?$dataLstCuentasContables : [],
            "EstadosBien" => (count($dataLstEstadoBien) > 0) ?$dataLstEstadoBien : [],
            "CaracteristicasBien" => (count($dataCaracteristicasBien) > 0) ?$dataCaracteristicasBien : [],
        ]);
        
    }



    public function Guardar_Actos_Recepcion(Request $request){
        $id_acto            = $request->id_acto;
        $id_entidad         = $request->id_entidad;
        $id_forma           = $request->id_forma;
        $nro_documento      = $request->nro_documento;
        $fecha_documento    = $request->fecha_documento;
        $fecha_inicio       = $request->fecha_inicio;
        $fecha_vencimiento  = $request->fecha_vencimiento;
        $tipo_benef         = $request->tipo_benef;
        $ruc_beneficiario   = $request->ruc_beneficiario;
        $usua_creacion      = $request->usua_creacion; 

        $dataCabActo = DB::select( "exec PA_GUARDAR_RECEPCION_V2 ?, ?, ?, ?, ?, ?, ?, ?, ?, ?",[
            $id_acto,
            $id_entidad,
            $id_forma,
            $nro_documento,
            $fecha_documento,
            $fecha_inicio,
            $fecha_vencimiento,
            $tipo_benef,
            $ruc_beneficiario,
            $usua_creacion
        ]);
        //dd($id_entidad);
        return response()->success($dataCabActo[0]);

    }

    public function Eliminar_Bien_Recepcion(request $request){

        $id_entidad = $request->id_entidad;
        $id_bien    = $request->id_bien;

        $dataResultado = DB::select( "exec PA_ELIMINAR_BIEN_RECEPCION_SW ?, ?",[
            $id_entidad,
            $id_bien
        ]);

        return response()->success($dataResultado);

    }





    public function fecha_sql($fecha){
        date_default_timezone_set('America/Lima');
        $datetime_variable = new DateTime($fecha);
        $datetime_formatted = $datetime_variable->format('Y-m-d H:i:s');
       return  $datetime_formatted;  
    }

}
