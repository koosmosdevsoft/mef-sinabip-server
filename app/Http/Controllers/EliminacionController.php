<?php

namespace sinabipmuebles\Http\Controllers;

use Illuminate\Http\Request;
use DateTime;
use DB;
use Pdf_Resumen;
use funciones\funciones;
use Illuminate\Support\Facades\Storage;
use Config;
use Validator;

class EliminacionController extends Controller
{
	public function index()
    {
        return 'FUNCTION INDEX';
    }

    public function ListadoEliminacion(Request $request) {

        $reglas = [
            'COD_ENTIDAD'                   => 'int',
            'NRO_DOCUMENTO_ADQUIS'          => 'max:20',
            'FECHA_DOCUMENTO_ADQUIS'        => 'max:10',
            'FECHA_DOCUMENTO_ADQUISHASTA'   => 'max:10',
            'page'                          => 'int',
            'records'                       => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
            ]);
        }

        $COD_ENTIDAD = $request->COD_ENTIDAD;
        $CODIGO_PATRIMONIAL = $request->CODIGO_PATRIMONIAL;
        $DENOMINACION = $request->DENOMINACION;
        $NRO_DOCUMENTO_ADQUIS = $request->NRO_DOCUMENTO_ADQUIS;
        $FECHA_DOCUMENTO_ADQUIS = $request->FECHA_DOCUMENTO_ADQUIS;
        $FECHA_DOCUMENTO_ADQUISHASTA = $request->FECHA_DOCUMENTO_ADQUISHASTA;
        $page = $request->page;
        $records = $request->records;
        $dataMotivos = DB::select ('exec PA_LISTADO_MOTIVOS_ELIMINACION'); 
        $dataB = DB::select('exec PA_LISTADO_ELIMINACION ?,?,?,?,?,?,?,?', [
            $COD_ENTIDAD,
            $CODIGO_PATRIMONIAL,
            $DENOMINACION,
            $NRO_DOCUMENTO_ADQUIS,
            $FECHA_DOCUMENTO_ADQUIS,
            $FECHA_DOCUMENTO_ADQUISHASTA,
            $page,
            $records
        ]);
	    return response()->success([
            "elim" => $dataB,
            "motivos"  => (count($dataMotivos) > 0) ?$dataMotivos : []
        ]);
    }

    public function ProcesarEliminacion(Request $request){
        $cod_entidad = $request->cod_entidad;
        $bienes = $request->bienes;
        $oficio = $request->oficio;
        $resolucion = $request->resolucion;
        $informe = $request->informe;
        $motivos = $request->motivos;
        $fecha = $request->fecha;

        $data = DB::select('exec PA_PROCESAR_ELIMINACION ?, ?, ?, ?, ?, ?, ?', [
            $cod_entidad,
            $bienes,
            $oficio,
            $resolucion,
            $informe,
            $motivos,
            $fecha
        ]);
        $cod_elim = $data[0]->codigo_eliminacion;
        $dataElimDetalle = DB::select('exec PA_OBTENER_ELIMINACION_DETALLE ?', [
            $cod_elim
        ]);
        $dataMotivos = DB::select(
            "select MOTIVO_ELIMINACION_BIEN[DESCRIPCION] from TBL_MUEBLES_UE_BIEN_ELIMINACION where COD_UE_BIEN_ELIMINACION = ?", [$cod_elim]
        );
        return response()->success([
            "Resultado" => $data[0],
            "ElimDetalle"  => (count($dataElimDetalle) > 0) ?$dataElimDetalle : [],
            "n_motivos" => $dataMotivos[0]
        ]);
    }

    public function fecha_sql($fecha){
        date_default_timezone_set('America/Lima');
        $datetime_variable = new DateTime($fecha);
        $datetime_formatted = $datetime_variable->format('Y-m-d H:i:s');
       return  $datetime_formatted;  
    }

    public function ImprimirResumenEliminacion(Request $request){
        header('Content-type: application/pdf');
        $id = $request->cod_entidad;
        $NOM_ENTIDAD =  DB::select(
            "SELECT NOM_ENTIDAD FROM TBL_PADRON_ENTIDAD where COD_ENTIDAD = ?",[$id]
        );
        $pdf = new Pdf_Resumen;
        $pdf->AliasNbPages();
            $pdf->SetFont('Arial','',10);
            $pdf->AddPage();
            $pdf->Ln(4);
    
            //****************************************
            //****************************************
            
            $pdf->SetFillColor(205,205,205);//color de fondo tabla
            $pdf->SetTextColor(10);
            $pdf->SetDrawColor(153,153,153);
            $pdf->SetLineWidth(.3);
            $pdf->SetFont('Arial','B',9);
            
            $pdf->SetFillColor(205,205,205);
            $pdf->Cell(45,8,'ENTIDAD',1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(140,8,$NOM_ENTIDAD[0]->NOM_ENTIDAD,1,0,'L',true);
                    
            $pdf->Ln();
            $pdf->Ln(3);
            $w = array(10, 70, 105);
            // Línea de cierre
            $pdf->Cell(array_sum($w),0,'','T');
            
            $pdf->Ln();
    
            $pdf->Output();
            return 1;
    }
    
    public function ImprimirResumenEliminacionGet(Request $request, $id, $codigo_eliminacion, $oficio, $resolucion, $informe, $motivos, $fecha){
        header('Content-type: application/pdf');
        $NOM_ENTIDAD =  DB::select(
            "SELECT NOM_ENTIDAD FROM TBL_PADRON_ENTIDAD where COD_ENTIDAD = ?",[$id]
        );
        $dataElimDetalle = DB::select('exec PA_OBTENER_ELIMINACION_DETALLE ?', [
            $codigo_eliminacion
        ]);
        $n_motivos = DB::select(
            "select MOTIVO_ELIMINACION_BIEN[DESCRIPCION] from TBL_MUEBLES_UE_BIEN_ELIMINACION where COD_UE_BIEN_ELIMINACION = ?", [$codigo_eliminacion]
        );
        $pdf = new Pdf_Resumen;
        $pdf->AliasNbPages();
            $pdf->SetFont('Arial','',10);
            $pdf->AddPage();
            $pdf->Ln(4);
    
            //****************************************
            //****************************************
            
            $pdf->SetFillColor(205,205,205);//color de fondo tabla
            $pdf->SetTextColor(10);
            $pdf->SetDrawColor(153,153,153);
            $pdf->SetLineWidth(.3);
            $pdf->SetFont('Arial','B',9);
            
            $pdf->SetFillColor(205,205,205);
            $pdf->Cell(45,8,'ENTIDAD',1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(140,8,$NOM_ENTIDAD[0]->NOM_ENTIDAD,1,0,'L',true);
                    
            $pdf->Ln();
            $pdf->Ln(3);

            $pdf->SetFillColor(205,205,205);
            $pdf->Cell(45,8,'OFICIO',1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(140,8,$oficio,1,0,'L',true);
                    
            $pdf->Ln();
            $pdf->Ln(3);

            $pdf->SetFillColor(205,205,205);
            $pdf->Cell(45,8,'RESOLUCION',1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(140,8,$resolucion,1,0,'L',true);
                    
            $pdf->Ln();
            $pdf->Ln(3);

            $pdf->SetFillColor(205,205,205);
            $pdf->Cell(45,8,'INFORME',1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(140,8,$informe,1,0,'L',true);
                    
            $pdf->Ln();
            $pdf->Ln(3);

            $pdf->SetFillColor(205,205,205);
            $pdf->Cell(45,8,'MOTIVOS',1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(140,8,$n_motivos[0]->DESCRIPCION,1,0,'L',true);
                    
            $pdf->Ln();
            $pdf->Ln(3);

            $pdf->SetFillColor(205,205,205);
            $pdf->Cell(45,8,'FECHA',1,0,'L',true);		
            $pdf->SetFillColor(255,255,255);	
            $pdf->Cell(140,8,$fecha,1,0,'L',true);
                    
            $pdf->Ln();
            $pdf->Ln(3);
            $w = array(10, 70, 105);
            // Línea de cierre
            $pdf->Cell(array_sum($w),0,'','T');
            
            $pdf->Ln();
            $pdf->Ln(5);

            //DETALLE DEL BIEN
            $pdf->SetFont('Arial','B',9);
            $pdf->SetFillColor(205,205,205);	
            $pdf->Cell(185,7,utf8_decode('DETALLE DE LOS BIENES ELIMINADOS'),1,0,'L',true);
            
            $pdf->Ln();
            
            $pdf->SetFont('Arial','B',8);
            $w = array(10, 70, 105);
            $pdf->Cell($w[0],5,utf8_decode('Item'),1,0,'C',true);
            $pdf->Cell($w[1],5,utf8_decode('Nro bien'),1,0,'C',true);
            $pdf->Cell($w[2],5,utf8_decode('Denominación'),1,0,'C',true);
            $pdf->Ln();
            
            // Restauración de colores y fuentes
            $pdf->SetFillColor(224,235,255);
            $pdf->SetTextColor(0);
            $pdf->SetFont('Arial','',7);
            // Datos
            $fill = false;
            $contador = 0;
            foreach ($dataElimDetalle as $key => $value) {

                $CODIGO_PATRIMONIAL = $value->CODIGO_PATRI;
                $DENOM_BIEN = $value->DENOM_BIEN;
                
                $contador++;
                $linea = 0;
                
                if($contador == 40){
                    $pdf->Cell(array_sum($w),0,'','T');
                    $pdf->AddPage();
                    $linea = 0;
                }else{
                    $linea++;
                }
                            
                if($linea!= 0 && $linea%58==0 ){
                    $pdf->Cell(array_sum($w),0,'','T');
                    $pdf->AddPage();
                 }
        
                $pdf->Cell($w[0],4,$contador,'LR',0,'C');
                $pdf->Cell($w[1],4,$CODIGO_PATRIMONIAL,'LR',0,'C');
                $pdf->Cell($w[2],4,$DENOM_BIEN,'LR',0,'C');
                $pdf->Ln();
                $fill = !$fill;
            }
            $pdf->Cell(array_sum($w),0,'','T');
            
            $pdf->Ln();
            $pdf->Output();
            return 1;
    }


    public function Adjuntareliminartxt(Request $request, $data){
        //header("Access-Control-Allow-Origin: *");
        $data = explode('-',$data);
        $id_entidad	= $data[0];
        $usua_creacion = $data[1];
        $tipo_acto = 7;
        $nomb_archivo = '';
        
        //$cod_alta = $cod_alta[0]->CODIGO.'_'; 
        $id_adjuntado =  DB::select( "SELECT TOP 1 ID_ADJUNTADOS_ACTOS_TL FROM TBL_ADJUNTADOS_ACTOS_TL ORDER BY ID_ADJUNTADOS_ACTOS_TL DESC" );
        
        if( empty($id_adjuntado) ){
            $id_adjuntado = 1;
        }else{
            $id_adjuntado = $id_adjuntado[0]->ID_ADJUNTADOS_ACTOS_TL;
            $id_adjuntado++  ;
        }
 
        if ($request->hasFile('file0')) {  
            $image = $request->file('file0');
            $ARCHIVO_EXTENSION = 'txt'; 
            $NUEVA_CADENA   = substr( md5(microtime()), 1, 4);
            $FECHA_ARCHIVO  = date('Y').''.date('m').''.date('d'); 
            $name = "Eliminacion/";
            
            //dd($ARCHIVO_NOMBRE_GENERADO);
            $result = DB::select(
                "exec PA_ADJUNTADO_ACTOS_ADQUISICION_SW ?,?,?,?",[ 
                    $id_entidad,7,"",$usua_creacion
                    ]
            );
            
            $id_adjuntado = $result[0]->ID_ADJUNTADO; 
            $ARCHIVO_NOMBRE_GENERADO ='TXT_'. strval($id_adjuntado).'Eliminar_Entidad_'.$id_entidad.'_Serie_'.$FECHA_ARCHIVO.'_'.$NUEVA_CADENA.'.'.$ARCHIVO_EXTENSION;
         //    $url = Storage::disk('public')->putFileAs($name, $image,$ARCHIVO_NOMBRE_GENERADO); 
         //    file_put_contents("//SRV-PORTALSBN/Inventarios_zip$/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
            if(Config::get('app.APP_LINUX')){
             // $url = Storage::disk('public')->put(storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO, $image);
             \Storage::disk('local')->put("public/".$name.$ARCHIVO_NOMBRE_GENERADO, file_get_contents($image));
             // file_put_contents(storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
             $origen = storage_path('app/public')."/".$name.$ARCHIVO_NOMBRE_GENERADO;
             $final = '/mnt/Inventarios_zip/'.$name.$ARCHIVO_NOMBRE_GENERADO;
             shell_exec('cp '.$origen.' '.$final);
             }else{
                 file_put_contents("//SRV-PORTALSBN/Inventarios_zip$/".$name.$ARCHIVO_NOMBRE_GENERADO,file_get_contents($image));
             }
            $Peso_Arch = filesize($image);
 
            $result2 = DB::select(
                "exec PA_ADJUNTADO_ACTOS_ADQUISICION_UPD_NOMBRE_ARCHIVO_SW ?,?",[ 
                    $id_adjuntado,$ARCHIVO_NOMBRE_GENERADO
                    ]
            );
            //return response()->json($url);
            return response()->success($result2[0]);
        }
    }

    
   public function CargareliminarTXT(Request $request){

    $reglas = [
        'id_entidad'    => 'int',
        'id_adjuntado'  => 'int',
        'usua_creacion' => 'int'
    ];
    $validator = Validator::make($request->all(), $reglas);
    if ($validator->fails()){
        return response()->success([
            'error' => true,
            'reco' => $validator->errors()
            ]);
    }

    $id_entidad     = $request->id_entidad;
    $id_adjuntado   = $request->id_adjuntado;
    $nombreArchivo  = $request->nombreArchivo;
    $usua_creacion  = $request->usua_creacion; 

    $dataCarga = DB::select( "exec PA_CARGA_MASIVA_ELIMINAR_SW ?, ?, ?, ?",[
        $id_entidad,
        $id_adjuntado,
        $nombreArchivo,
        $usua_creacion
    ]);
    
    //return response()->success(true);
    return response()->success($dataCarga[0]);

    }

    public function ValidarEliminarTXT(Request $request){

        $reglas = [
            'id_entidad'    => 'int',
            'id_adjuntado'  => 'int',
            'usua_creacion' => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
                ]);
        }

        $id_entidad     = $request->id_entidad;
        $id_adjuntado   = $request->id_adjuntado;
        $nombreArchivo  = $request->nombreArchivo;
        $usua_creacion  = $request->usua_creacion;

        $dataValidacion = DB::select( "exec PA_VALIDACION_MASIVA_ELIMINAR_SW ?, ?, ?",[
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);
        
        //return response()->success(true);
        return response()->success($dataValidacion[0]);

    }


    public function ListadoErroresCargaMasivaEliminar(Request $request){

        
        $id_entidad     = $request->id_entidad;
        $id_adjuntado    = $request->id_adjuntado;
        $usua_creacion  = $request->usua_creacion;


        $accion         = 2;
        $dataObservaciones_2 = DB::select( "exec PA_OBSERVACIONES_VALIDACION_MASIVA_ELIMINACION_SW ?, ?, ?, ?",[
            $accion,
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);

        $accion         = 3;
        $dataObservaciones_3 = DB::select( "exec PA_OBSERVACIONES_VALIDACION_MASIVA_ELIMINACION_SW ?, ?, ?, ?",[
            $accion,
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);

        
        return response()->success([
            "errores_columnas"  => (count($dataObservaciones_2) > 0) ?$dataObservaciones_2 : [],
            "no_existe_codigos"  => (count($dataObservaciones_3) > 0) ?$dataObservaciones_3 : []
        ]);
    }
    
    public function FinalizarEliminacionTXT(Request $request){

        $reglas = [
            'id_entidad'    => 'int',
            'id_adjuntado'  => 'int',
            'usua_creacion' => 'int'
        ];
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()){
            return response()->success([
                'error' => true,
                'reco' => $validator->errors()
                ]);
        }

        $id_entidad     = $request->id_entidad;
        $id_adjuntado   = $request->id_adjuntado;
        $nombreArchivo  = $request->nombreArchivo;
        $usua_creacion  = $request->usua_creacion;

        $dataFinalizacion = DB::select( "exec PA_FINALIZACION_MASIVA_ELIMINACION_SW ?, ?, ?",[
            $id_entidad,
            $id_adjuntado,
            $usua_creacion
        ]);
        
        //return response()->success(true);
        return response()->success($dataFinalizacion[0]);

    }





}
