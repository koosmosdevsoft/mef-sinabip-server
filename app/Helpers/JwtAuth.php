<?php 
namespace App\Helpers;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\DB;


/**
 * Generador y validar token
 */
class JwtAuth
{
	public $Key;
	function __construct()
	{
		$this->key = 'sinabip_dsdvvvkvn';
	}

	public function checkToken($jwt, $getIndentity = false)
	{
		$auth = false;
		try {
			$decoded = JWT::decode($jwt, $this->key, array('HS256'));
		} catch (\UnexpectedValueException $e) {
            $auth = false;
		} catch (\DomainException $e) {
            $auth = false;
        }
        
		if (isset($decoded) && is_object($decoded) ) {
        // if (isset($decoded) && is_object($decoded) && isset($decoded->sud)) {
			$auth = true;
		}else{
			$auth = false;
		}
		if ($getIndentity) {
			return $decoded;
		}
		return $auth;
	}

	// public function validaToken()
}